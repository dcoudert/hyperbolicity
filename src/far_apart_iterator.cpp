/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "far_apart_iterator.h"

#include "algorithms.h"
#include "graph.h"

#include <numeric>
#include <algorithm>


NaiveFarApartIterator::NaiveFarApartIterator(Graph const& graph, Distance cut_off) :
    dist_matrix(alg::computeDistanceMatrix(graph)),
    far_matrix(graph.numberOfNodes(), graph.numberOfNodes(), 1),
    current_index(dist_matrix),
    cut_off(cut_off)
{
    current_distance = *std::max_element(dist_matrix.begin(), dist_matrix.end());

    for (NodeID u = 0; u < graph.numberOfNodes(); ++u) {
        for (NodeID v = 0; v < graph.numberOfNodes(); ++v) {
            if (u == v) {
                far_matrix(u, u) = 0;
                continue;
            }
            if (far_matrix(u, v) == 1) {
                for (auto w: graph.neighborsOf(v)) {
                    if (dist_matrix(u, v) + 1 == dist_matrix(u, w)) {
                        // v is not far from u
                        far_matrix(u, v) = 0;
                        far_matrix(v, u) = 0;
                        break;
                    }
                }
            }
        }
    }
}

bool NaiveFarApartIterator::hasNext()
{
    if (current_distance < cut_off) { return false; }

    while (current_distance != 0) {
        while (current_index.valid()) {
            if ((current_index.j() < current_index.i()) and
                (far_matrix[current_index] == 1) and
                (dist_matrix[current_index] == current_distance)) {
                return true;
                
            }
            ++current_index;
        }
        current_index.reset();
        --current_distance;
        if (current_distance < cut_off) { break; }
    }

    return false;
}

FarApartPair NaiveFarApartIterator::getNext()
{
    NodeID source = current_index.j();
    NodeID target = current_index.i();
    ++current_index;
    return {source, target, current_distance};
}

Distance NaiveFarApartIterator::getDistance() const
{
    return current_distance;
}


/* ==================================================================== */
/* Far Apart Iterator                                                   */
/* ==================================================================== */

std::size_t FarApartIterator::get_cpt_initial_bfs() {
    return cpt_initial_bfs;
}

FarApartIterator::FarApartIterator(Graph const& graph, BFSCache* cache, std::string const& algo, Distance cut_off) :
    graph(graph), algo(algo), cut_off(cut_off),
    diameter(0),
    ecc(graph.numberOfNodes()),
    cpt_initial_bfs(0),
    known_far_vertices(graph.numberOfNodes(), false),
    data_far(graph.numberOfNodes())
{
    if (cache == nullptr /* FIXME, do we really need this ? : or (cache->capacity == 0) */) { bfs_cache = nullptr; }
    else { bfs_cache = cache; }

    // 1. Compute all eccentricities using appropriate algorithm
    if (algo.compare("TK") == 0) {
        initialize_with_Takes_Kosters();
    } else { // default
        initialize_with_Dragan_Habib_Viennot();
    }
    diameter = *(std::max_element(ecc.begin(), ecc.end()));
    data_far.resize(diameter + 1);
    //std::cout << "diameter = " << diameter << "\t#BFS = " << cpt_initial_bfs << std::endl;

    // 2. Initialize the iterator
    current_dist = diameter;
    current_source_it = data_far[current_dist].begin();
    current_source_end = data_far[current_dist].end();
    current_source = current_source_it->first;
    if (not known_far_vertices[current_source]) {
        compute_far_vertices(current_source);
    }
    current_target_it = current_source_it->second.begin();
    current_target_end = current_source_it->second.end();
    _mates.clear();
    _temp_mates.clear();
}


FarApartIterator::~FarApartIterator()
{
    data_far.clear();
}


std::ostream& operator<<(std::ostream& os, FarApartIterator& F)
{
    std::size_t cpt = 0;
    for (auto b: F.known_far_vertices)
        cpt += b;
    os << "FarApartIterator (algo = " << F.algo << "): #BFS initial = " << F.cpt_initial_bfs << "\tfinal = " << cpt << "\n";
    return os;
}




bool FarApartIterator::hasNext()
{
    if (current_dist < cut_off) { return false; }

    while (true) {
        while (current_target_it == current_target_end) {
            // We go to next source
            swap_mates();
            ++current_source_it;
            while (current_source_it == current_source_end) {
                // We are done with current dist. We go to lower distance
                --current_dist;
                if (current_dist < cut_off) { return false; }
                current_source_it = data_far[current_dist].begin();
                current_source_end = data_far[current_dist].end();
                // We reset the temporary mates
                _temp_mates.clear();
            }
            current_source = current_source_it->first;
            // We check if we know far vertices from current source
            if (not known_far_vertices[current_source]) {
                compute_far_vertices(current_source);
            }
            current_target_it = current_source_it->second.begin();
            current_target_end = current_source_it->second.end();
        }
        if (is_far_apart(current_source, *current_target_it, current_dist)) {
            store_mate(*current_target_it);
            if (current_source < *current_target_it)
                return true;
        }
        // We go to the next target
        ++current_target_it;
    }
}

FarApartPair FarApartIterator::getNext()
{
    assert( hasNext() );
    NodeID target = *current_target_it;
    ++current_target_it;
    store_temp_mate(target);
    return {current_source, target, current_dist};
}

Distance FarApartIterator::getDistance() const
{
    return current_dist;
}

Distance FarApartIterator::getEccentricity(NodeID const u) const
{
    return ecc[u];
}

bool FarApartIterator::is_far_apart(NodeID const u, NodeID const v)
{
    if (not known_far_vertices[u]) { compute_far_vertices(u); }
    if (not known_far_vertices[v]) { compute_far_vertices(v); }

    // now check if far apart
    for (auto const& it: data_far) {
        auto it_u = it.find(u);
        auto it_v = it.find(v);
        if ((it_u != it.end()) and (it_v != it.end())
            and alg::contains(it_u->second, v) and alg::contains(it_v->second, u)) {
            return true;
		}
    }
    return false;
}


bool FarApartIterator::is_far_apart(NodeID const u, NodeID const v, Distance const dist)
{
    if (dist < cut_off) { return false; }
    if (dist <= current_dist) {
        if (not known_far_vertices[u]) { compute_far_vertices(u); }
        if (not known_far_vertices[v]) { compute_far_vertices(v); }
        
        // now check if far apart. The distance between u and v is known
        auto it_u = data_far[dist].find(u);
        auto it_v = data_far[dist].find(v);
        return ((it_u != data_far[dist].end()) and (it_v != data_far[dist].end())
		        and alg::contains(it_u->second, v) and alg::contains(it_v->second, u));
    }
    else {
        // data_far[dist] has already been consolidated and contains mates
        // So we check if u in data_far[dist] and v in data_far[dist][u]
        auto it_u = data_far[dist].find(u);
        return (it_u != data_far[dist].end()) and alg::contains(it_u->second, v);
    }
}



// TODO: if a cache is used, compute and store BFS in cache
void FarApartIterator::compute_far_vertices(NodeID source)
{
    if (bfs_cache == nullptr) {
        auto F = alg::getFarVertices(graph, source);
        store_far_vertices(source, F);
    } else {
		assert(!bfs_cache->contains(source));
        auto& bfs_info = bfs_cache->getNew(source);
        alg::runBFS(bfs_info, graph, source);
        auto FF = alg::getFarVertices(graph, bfs_info, true);
        store_far_vertices(source, FF);
    }

    known_far_vertices[source] = true;
}


void FarApartIterator::store_far_vertices(NodeID source, FarVertices const& far_vertices)
{
    if (not known_far_vertices[source]) {
        known_far_vertices[source] = true;

        // Feed data_far
        for (auto it = far_vertices.rbegin(); it != far_vertices.rend(); ++it) {
            auto dist = it->distance;
            if (dist < cut_off) { break; }
            if (data_far[dist].find(source) == data_far[dist].end()) {
                data_far[dist][source] = {};
            }

			data_far[dist][source].push_back(it->node_id);
        }
    }

	for (auto& map: data_far) {
		if (map.count(source)) {
			auto& vec = map[source];
			std::sort(vec.begin(), vec.end());
			auto new_end = std::unique(vec.begin(), vec.end());
			vec.erase(new_end, vec.end());
			vec.shrink_to_fit();
		}
	}
}

/*
 * Set cut_off distance to a larger value
 *
 * Data stored in dist_far for smaller distances are removed
 */
void FarApartIterator::increase_cut_off(Distance new_cut_off)
{
    assert((new_cut_off > cut_off) && (new_cut_off <= current_dist));
    
    for (Distance dist = cut_off; dist < new_cut_off; ++dist) {
        data_far[dist].clear();
    }
    cut_off = new_cut_off;
}




// Compute all eccentricities using Takes and Kosters
// On the way, store computed sets of u-Far vertices
void FarApartIterator::initialize_with_Takes_Kosters()
{
    NodeIDs todo(graph.numberOfNodes());
    std::iota(todo.begin(), todo.end(), 0);
    Distances eccs_ub(graph.numberOfNodes(), DIST_INF);
    Distances eccs_lb(graph.numberOfNodes(), 0);
    
    auto my_compare_ub = [&](const NodeID & v1, const NodeID & v2) {
        return eccs_ub[v1] < eccs_ub[v2];
    };
    auto my_compare_lb = [&](const NodeID & v1, const NodeID & v2) {
        return eccs_lb[v1] < eccs_lb[v2];
    };

    bool select_max = true;
    BFSInfo bfs_u_loc;
    while (not todo.empty()) {
        // We select alternatively a vertex with largest ecc upper bound and smallest lower bound
        std::size_t i;
        if (select_max) { i = std::max_element(todo.begin(), todo.end(), my_compare_ub) - todo.begin(); }
        else            { i = std::min_element(todo.begin(), todo.end(), my_compare_lb) - todo.begin(); }
        select_max = not select_max;
        NodeID u = todo[i];
        todo[i] = todo.back();
        todo.pop_back();

        // Compute distances and far vertices from u
        NodeID last;
        BFSInfo & bfs_u =
            (bfs_cache != nullptr) ? bfs_cache->getNew(u) : bfs_u_loc;
        alg::runBFS(bfs_u, graph, u, last); // rerun even if its in cache where it could contain a pruned BFS with non-zero condacc value
        auto far_vertices = alg::getFarVertices(graph, bfs_u);
        store_far_vertices(u, far_vertices);
        ecc[u] = bfs_u.distance(last);
        cpt_initial_bfs++;

        // update eccentricity bounds and remove vertices for which ecc is proved
        i = 0;
        while (i < todo.size()) {
            NodeID v = todo[i];
            eccs_lb[v] = std::max(eccs_lb[v], std::max(ecc[u] - bfs_u.distance(v), bfs_u.distance(v)));
            eccs_ub[v] = std::min(eccs_ub[v], ecc[u] + bfs_u.distance(v));
            if (eccs_lb[v] == eccs_ub[v]) {
                ecc[v] = eccs_lb[v];
                todo[i] = todo.back();
                todo.pop_back();
				// FIXME
                data_far[ecc[v]][v] = {};  // To indicate that we must compute v-far
            }
            else {
                ++i;
            }
        }
    }
}

void FarApartIterator::initialize_with_Dragan_Habib_Viennot()
{
	Distances eccs_ub(graph.numberOfNodes(), DIST_INF);
	Distances eccs_lb(graph.numberOfNodes(), 0);

	// add all nodes to queue
	NodeIDs todo(graph.numberOfNodes());
	std::iota(todo.begin(), todo.end(), 0);

	auto my_compare_lb = [&](const NodeID & v1, const NodeID & v2) {
		return eccs_lb[v1] < eccs_lb[v2];
	};


	// Select u with untight upper bound and minimal eccentricity
	// If eccs_lb[u] = ecc[u], we update upper bounds
	// Otherwise, we use distances from u and its antipode to update lower bounds
	BFSInfo bfs_u_dummy, bfs_a_dummy;
	while (not todo.empty()) {
		// select and remove the element with minimum eccentricity lower bound
		auto i = std::min_element(todo.begin(), todo.end(), my_compare_lb) - todo.begin();
		NodeID u = todo[i];
		todo[i] = todo.back();
		todo.pop_back();
		// Compute exact eccentricity of u

		NodeID antipode;
		assert(!bfs_cache->contains(u));
		BFSInfo& bfs_u = (bfs_cache != nullptr ? bfs_cache->getNew(u) : bfs_u_dummy);
		alg::runBFS(bfs_u, graph, u, antipode);

		cpt_initial_bfs++;
		auto u_far = alg::getFarVertices(graph, bfs_u);
		store_far_vertices(u, u_far);
		ecc[u] = bfs_u.distance(antipode);

		if (eccs_lb[u] == ecc[u]) {
			// We found the good vertex !
			// We update eccentricity upper bounds
			for (auto v: todo) {
				eccs_ub[v] = std::min(eccs_ub[v], bfs_u.distance(v) + ecc[u]);
			}
		}
		else {
			// u was not a good choice
			// We use its antipode to update lower bounds
			for (std::size_t j = 0; j < todo.size(); ++j) {
				if (todo[j] == antipode) {
					todo[j] = todo.back();
					todo.pop_back();
					break;
				}
			}

			NodeID b;
			assert(!bfs_cache->contains(antipode));
			BFSInfo& bfs_a = (bfs_cache != nullptr ? bfs_cache->getNew(antipode) : bfs_a_dummy);
			alg::runBFS(bfs_a, graph, antipode, b);

			cpt_initial_bfs++;
			auto a_far = alg::getFarVertices(graph, bfs_a);
			store_far_vertices(antipode, a_far);
			ecc[antipode] = bfs_a.distance(b);
			for (auto v: todo) {
                            eccs_lb[v] = std::max(eccs_lb[v], std::max(bfs_a.distance(v), bfs_u.distance(v)));
			}
		}
		std::size_t idx = 0;
		while (idx < todo.size()) {
			auto v = todo[idx];
			if (eccs_lb[v] == eccs_ub[v]) {
				ecc[v] = eccs_lb[v];
				todo[idx] = todo.back();
				todo.pop_back();
				data_far[ecc[v]][v] = {};  // To indicate that we must compute v-far
			} else {
				++idx;
			}
		}
	}
}



// Store mate v of current_source
void FarApartIterator::store_mate(NodeID const mate)
{
	_mates.push_back(mate);
}


// Replace data_far[current_dist][current_source] by _mates
// This way we keep only far vertices for which the pair is far apart
// This must but done only after all targets have been considered
void FarApartIterator::swap_mates(){
    assert( current_target_it == current_target_end );

	_mates.shrink_to_fit();
    data_far[current_dist][current_source].swap(_mates);
    _mates.clear();
}

// Store the mate of current_source
// Used to return correct list of mates when dist = current_dist
void FarApartIterator::store_temp_mate(NodeID const mate)
{
    auto it_u = _temp_mates.find(current_source);
    if (it_u == _temp_mates.end()) {
        _temp_mates[current_source] = {mate};
    } else {
        it_u->second.push_back(mate);
    }
    auto it_m = _temp_mates.find(mate);
    if (it_m == _temp_mates.end()) {
        _temp_mates[mate] = {current_source};
    } else {
        it_m->second.push_back(current_source);
    }
}




// Return u-Far vertices such that uv is far apart and has already been yielded
// Hence these pairs are such that distance >= current dist
FarVertices FarApartIterator::getMates(NodeID const u)
{
    FarVertices F;
    F.clear();

    if (known_far_vertices[u]) {
        // Feed F with far apart pairs containing u and with distance > current_dist
        // The sets of far vertices of u for each distance dist > current_dist have
        // already been consolidated, i.e. contains only mates
        for (Distance dist = diameter; dist > current_dist; --dist) {
            auto it_u = data_far[dist].find(u);
            if (it_u != data_far[dist].end()) {
                for (auto v: it_u->second) {
                    F.emplace_back(v, dist);
                }
            }
        }
        for (auto v: _mates) {
            F.emplace_back(v, current_dist);
        }
    }
    // Otherwise, u has not been involved in any yielded far apart pair, and so
    // has no mates.

    return F;
}

ConstNodeIDCRange FarApartIterator::getMates(NodeID const u, Distance const dist) const
{
    if (dist == current_dist) {
        if (u == current_source) {
            return ConstNodeIDCRange(_mates.begin(), _mates.end());
        } else {
            auto it_u = _temp_mates.find(u);
            if (it_u != _temp_mates.end()) {
                // auto it_d = data_far[dist].find(u);
                // std::cout << "mates(" << u << ", " << dist << ") :" ;
                // std::cout << it_d->second.size() << "\t" << _it_u->second.size() << std::endl;
                return ConstNodeIDCRange(it_u->second.begin(), it_u->second.end());
            }
        }
    } else if (dist >= current_dist) {
        auto it_u = data_far[dist].find(u);
        if (it_u != data_far[dist].end()) {
            return ConstNodeIDCRange(it_u->second.begin(), it_u->second.end());
        }
    }

	// FIXME: this is a bad hack that only works because we won't even access an element,
	// but because 's' is local it will be deleted and thus the iterators are invalid.

    // Otherwise, we return empty range
    NodeIDs s;
    return ConstNodeIDCRange(s.begin(), s.end());
}
