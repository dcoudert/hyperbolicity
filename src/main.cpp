/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "defs.h"
#include "graph.h"
#include "algorithms.h"
#include "hyperbolicity.h"
#include "hyperbolicity_dom.h"
#include "leanness.hpp"
#include "io.h"
#include "measurement_tool.h"

#include <cstring>

class ParserInput {
public:
	std::string filename;
	int alg_version;
	std::size_t capacity;
    int h_lb_guess;
    std::size_t k = 10;
    std::size_t ball = 1;
    bool prune = true;
    bool heur = true;
    Distance distance = 1;
    Distance compute_DOM_dist = DIST_INF;
    float compute_DOM_ratio = 2.0;
    uint compute_DOM_version = 52;
    uint compute_DOM_max_side = 10000;
    uint compute_DOM_max_cache_size = 1000;

	explicit ParserInput(int argc, char* argv[]):
            alg_version(3), capacity(1000), h_lb_guess(0), k(10), ball(1)
	{
		if (argc < 2)
		{
			printUsage();
			ERROR("You have to specify a graph filename.");
		}
		if (strcmp(argv[1], "-h") == 0) {
			printUsage();
			std::exit(0);
		}

		filename = argv[1];

		for (int i = 2; i < argc; ++i)
		{
			if (strcmp(argv[i], "-a") == 0)
			{
				if (i+1 >= argc) { ERROR("Missing parameter for \"-a\"."); }
				alg_version = std::stoi(argv[++i]);
			}
			else if (strcmp(argv[i], "-c") == 0)
			{
				if (i+1 >= argc) { ERROR("Missing parameter for \"-c\"."); }
				capacity = std::stoi(argv[++i]);
			}
            else if (strcmp(argv[i], "-noprune") == 0)
            {
                prune = false;
            }
            else if (strcmp(argv[i], "-noheur") == 0)
            {
                heur = false;
            }
			else if (strcmp(argv[i], "-l") == 0)
			{
				if (i+1 >= argc) { ERROR("Missing parameter for \"-l\"."); }
				h_lb_guess = std::stoi(argv[++i]);
			}
            else if (strcmp(argv[i], "-k") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-k\"."); }
                k = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-b") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-b\"."); }
                ball = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-domd") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-domd\"."); }
                compute_DOM_dist = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-domr") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-domr\"."); }
                compute_DOM_ratio = std::stof(argv[++i]);
            }
            else if (strcmp(argv[i], "-domv") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-domv\"."); }
                compute_DOM_version = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-doms") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-doms\"."); }
                compute_DOM_max_side = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-domc") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-domc\"."); }
                compute_DOM_max_cache_size = std::stoi(argv[++i]);
            }
			else if (strcmp(argv[i], "-h") == 0)
			{
				printUsage();
				std::exit(0);
			}
			else {
				ERROR("Unknown option " << argv[i]);
			}
		}
        
        // Turn off heuristic by setting parameter k to 0
        if (heur == false) {
            k = 0;
        }
	}

	void printUsage() {
		std::cout << "USAGE: ./main <graph_filename> [-a <algorithm_version>] [-c <cache_size>] [-noprune] [-noheur] [-l <lowerbound>] [-k <number_of_trials>] [-b <ball>] [-domd <distance>] [-domr <ratio>] [-domv <version>] [-doms <max_matrix_side>] [-domc <max_number_of_cached_matrices>] \n"
            "\n"
			"The <algorithm_version> should be the number:\n"
			"1) for Borassi et al.\n"
            "\n"
			"2) for the algorithm by Coudert, Nusser and Viennot, based on the enumeration of far-apart pairs. This version is without pruned BFS, initial heuristic and initial lower bound.\n"
			"3) for the algorithm by Coudert, Nusset and Viennot, based on the enumeration of far-apart pairs and using with pruned BFS and initial heuristic (default).\n"
            "\n"
            "   Parameters: [-c <cache_size>] [-noprune] [-noheur] [-l <lowerbound>] [-k <number_of_trials>]\n"
            "   -c <cache_size>: allow to specify the size of the cache of BFSs (at least 2)\n"
            "   -noprune : disable the use of pruned BFS in algorithm 3\n"
            "   -noheur : disable the heuristic in algorithm 3\n"
            "      The combination '-a 3 -noprune -noheur' is equivalant to '-a 2'\n"
            "   -l <lowerbound> : allow to specify an initial lower bound\n"
            "   -k <number_of_trials> : specifies the number of trials of the heuristic (default: 10).\n"
            "                          Using '-k 0' is equivalant to '-noheur'\n"
            "   -b <ball> : a parameter for the heuristic\n"
            "\n"
            "5) for a heuristic -- requires parameters k (10 by default) and b (1 by default)\n"
            "\n"
            "7) count number of far-apart pairs and output distribution according to distance\n"
            "\n"
            "20) exact hyperbolicity computation inspired from algorithm 1 : compute dominating sets for distances d, d/r, d/r^2, ..., 1 and check necessary quadruples in a depth first search manner. Distance d can be set with -domd d (computed depending on a lower-bound of hyperbolicity by default). Ratio r can be set with -domr r (3.0 by default). Dominating set heuristic can be set using -domv v (52 by default). Distances are computed using the pruned landmark labeling scheme of Akiba et al. The maximum side of cached distance matrices can be set using -doms s. The maximum number of cached matrices can be set using -domc c. Use -l h_lb if h_lb is a known lower-bound of hyperbolicity.\n"
            "21) Same as 20 but using the implementation of hub labeling by L. Viennot.\n"
            "\n"
            "   Parameters for algorithms 20 and 21: [-domd <distance>] [-domr <ratio>] [-domv <version>] [-doms <max_matrix_side>] [-domc <max_number_of_cached_matrices>] [-l <lowerbound>]\n"
            "   -domd <distance>: domination distance (>= 0).\n"
            "   -domr <ratio>: reduction ratio for the domination distances of the hierarchical dominating set (must be > 1).\n"
            "   -domv <version>: version of the greedy dominating set algorithm to use (52 by default). This is a number xy such that:\n"
            "       The lowest digit (y) indicates the order in which to consider vertices:\n"
            "           0: no specific order, so vertex labels\n"
            "           1: by increasing degree\n"
            "           2: by non-increasing degree\n"
            "           3: by increasing eccentricity\n"
            "           4: by non-increasing eccentricity\n"
            "           5: random order\n"
            "\n"
            "       The highest digit (x) indicates:\n"
            "           0: basic greedy dominating set; use recursion to build sub-dominating sets.\n"
            "           1: basic greedy dominating set; use iterative method to build sub-dominating sets.\n"
            "           2: connected dominating set; use iterative method to build sub-dominating sets.\n"
            "           3: same as 1\n"
            "           4: connected dominating set; each vertex attached to closest dominator; use iterative method to build sub-dominating sets.\n"
            "           5: basic greedy dominating set; each vertex attached to closest dominator; use iterative method to build sub-dominating sets.\n"
            "\n"
            "   -doms <max_matrix_side>: maximum side of stored distance matrices.\n"
            "   -domc <max_number_of_cached_matrices>]: maximum number of cached distance matrices (at least 7).\n"
            "\n"
            "30) Build hierarchical dominating set and exit.\n"
            "\n"
            "   Parameters: [-domd <distance>] [-domr <ratio>] [-domv <version>]\n"
            << std::endl;
	}
};


int main(int argc, char* argv[])
{
    ParserInput PI = ParserInput(argc, argv);

	try {
		auto parser_edges = io::readGraph(PI.filename);
		Graph g;
		g.init(parser_edges);

		std::cout << "Graph: " << g << std::endl;
		std::cout << "BFS cache capacity: " << PI.capacity << std::endl;
		std::cout << "BFSInfo type: " << BFS_INFO_TYPE << std::endl;
		std::cout << "Algorithm version: " << PI.alg_version << std::endl;

		/*
		auto ecc = alg::computeAllEccentricitiesDHV(g);
		auto diam_it = std::max_element(ecc.begin(), ecc.end());
		auto rad_it = std::min_element(ecc.begin(), ecc.end());
		std::cout << "radius = " << *rad_it << "\tdiameter = " << *diam_it << std::endl;
		return 0;
		*/

		Hyperbolicity h(g);
        HyperbolicityDom hd(g);
		float value = 0;
                long long int nb_pairs;
        switch(PI.alg_version) {
            case 1:
                value = h.computeBorassi();
                break;
            case 2:
                value = h.compute_v2(PI.capacity, false, 0, 0);
                break;
            case 3:
                value = h.compute_v2(PI.capacity, PI.prune, PI.h_lb_guess, PI.k);
                break;
            case 5:
                value = h.heuristicCCL(PI.k, PI.ball);
                break;
            case 7:
                value = 0;
                nb_pairs = h.compute_v2_nb_pairs(PI.capacity);
                std::cout <<"\nTotal nb of far appart pairs = "
                          << nb_pairs <<"\n";
                break;
            case 20:
                value = hd.compute_DOM_DFS(1, PI.compute_DOM_dist,
                                              PI.compute_DOM_ratio,
                                              PI.compute_DOM_version,
                                              PI.compute_DOM_max_side,
                                              PI.compute_DOM_max_cache_size,
                                              PI.h_lb_guess);
                break;
            case 21:
                value = hd.compute_DOM_DFS(2, PI.compute_DOM_dist,
                                              PI.compute_DOM_ratio,
                                              PI.compute_DOM_version,
                                              PI.compute_DOM_max_side,
                                              PI.compute_DOM_max_cache_size,
                                              PI.h_lb_guess);
                break;
            case 30:
                for (int i = 0; i < 1; i++) {
                    HierarchicalDominatingSet HDS(g, PI.compute_DOM_dist,
                                                  PI.compute_DOM_ratio,
                                                  PI.h_lb_guess,
                                                  PI.compute_DOM_version);
                    std::cout << HDS << std::endl;
                }
                break;
            case 42:
                value = h.testEccentricities();
                break;
            default:
                PI.printUsage();
                ERROR("Unknown algorithm version: " << PI.alg_version);
        }

		MEASUREMENT::print();

        std::cout << std::endl << "The hyperbolicity of the graph is " << value;
        std::cout << std::endl;
	}
	catch(const std::exception& e) {
		PI.printUsage();
		ERROR(e.what());
	}
}
