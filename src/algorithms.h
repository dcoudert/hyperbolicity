/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "basic_types.h"
#include "bfs_info.h"

#include "contraction.hh"

#include <algorithm>

class Graph;

namespace unit_tests
{
	void testAlgorithms();
}

namespace alg
{

// Return distances from source using BFS
void runBFS(Distances &distances, Graph const& graph, NodeID source);

// Return distances from source using BFS
void runBFS(BFSInfo &bfs_info, Graph const& graph, NodeID source);

// Run BFS, return distances and record last visited vertex in last
// The last visited vertex is at largest distance from source
void runBFS(BFSInfo &bfs_info, Graph const& graph, NodeID source, NodeID& last);

// Run BFS up to distance k
void runBFSk(BFSInfo &bfs_info, Graph const& graph, NodeID source, Distance k=1);

// Run a pruned BFS from source: we visit only nodes i s.t. 2*(eccs[i] - dist(source,i)) >= condacc) as in Hyperbolicity::computeAccVal.
// Return distances of visited nodes. Other nodes have distance DIST_INF.
void runBFSPrunedAccEcc(BFSInfo &bfs_info, Graph const& graph, Distances const &eccs, NodeID source, Distance condacc);

// Return shortest path distance from source to target, computed using BFS
Distance calcDist(Graph const& graph, NodeID source, NodeID target);

// Return distance matrix of the graph, computed using BFS
DistanceMatrix computeDistanceMatrix(Graph const& graph);

// Return the distance matrix between specified vertices in the graph
// Cell i,j of the matrix is the distance between vertices[i],vertices[j]
DistanceMatrix computeDistanceMatrix(Graph const& graph, NodeIDs const& vertices);

// Return a hub labeling of the graph, that is a compact representation of its distance matrix. If test > 0, checks that distances are right from test random nodes.
HubLabeling computeHubLabeling(Graph const& graph);
HubLabeling * computeHubLabeling_ptr(Graph const& graph);

// Return the eccentricity of source, computed using BFS
Distance computeEccentricity(Graph const& graph, NodeID source);

// Return the diameter of the graph, computed as the maximum over the eccentricity of all vertices
Distance computeDiameter(Graph const& graph);

// Return the diameter of the graph, using an algorithm by Dragan, Habib and Viennot
Distance computeDiameterDHV(Graph const& graph);

// Return the vector of eccentricities, computed using the algorithm by Takes and Kosters
Distances computeAllEccentricities(Graph const& graph);
Distances computeAllEccentricitiesDHV(Graph const& graph);

// Return the set of u-far vertices, computed from the distances from bfs_distances[0]
FarVertices getFarVertices(Graph const& graph, BFSInfo const& bfs_info, bool sorted=true);

// Return the set of u-far vertices, computed using BFS
FarVertices getFarVertices(Graph const& graph, NodeID u);

// Return distances as encoded in far_vertices
// far_vertices is passed by value because we modify it inside the function
Distances computeDistsFromFarVertices(Graph const& graph, FarVertices far_vertices, bool sorted=false, Distance cut_off=0);

// Return the list of far apart pairs computed from the distance matrix
FarApartPairs getFarApartPairs(Graph const& graph, DistanceMatrix const& dist, bool sorted=true);
FarApartPairs getFarApartPairs_v2(Graph const& graph, DistanceMatrix const& dist, bool sorted=true);

template <typename T>
bool contains(std::vector<T> const& container, T value)
{
	auto const lb = std::lower_bound(container.begin(), container.end(), value);
	return lb != container.end() && *lb == value;
}


} // end namespace alg
