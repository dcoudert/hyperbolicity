/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "defs.h"
#include "graph.h"
#include "hyperbolicity.h"
#include "io.h"
#include "measurement_tool.h"

void printUsage()
{
	std::cout << "USAGE: ./run_benchmarks [<cache_size>]" << std::endl;
	std::cout << "Default cache size is 1000." << std::endl;
}

int main(int argc, char* argv[])
{
	if (argc > 2) {
		printUsage();
		ERROR("Wrong number of parameters.");
	}
	std::size_t capacity = (argc < 2 ? 1000 : std::stoi(argv[1]));

	std::vector<std::string> edgelists = {
		"graphs/p2p-Gnutella09.bcc.edgelist",
		"graphs/ca-GrQc.bcc.edgelist",
		"graphs/CAIDA_as_20000102.bcc.edgelist"
	};

	std::vector<ParserData> parser_edges_vec;
	for (auto const& edgelist: edgelists) {
		parser_edges_vec.push_back(io::readGraph(edgelist));
	}

	for (std::size_t i = 0; i < edgelists.size(); ++i) {
		MEASUREMENT::reset();

		Graph g;
		g.init(parser_edges_vec[i]);
		std::cout << "Run algorithms for the graph:\n" << edgelists[i] << std::endl;
		std::cout << "=============================" << std::endl;

		Hyperbolicity h(g);
		std::cout << "Run Borassi et al.:" << std::endl;;
		auto const h1 = h.computeBorassi();
		std::cout << std::endl;

		std::cout << "Run new algorithm:" << std::endl;;
		auto const h2 = h.compute_v2(capacity);
		std::cout << std::endl;

		assert(h1 == h2);
		std::cout << "The hyperbolicity is " << h1 << std::endl;

		MEASUREMENT::print();
		std::cout << "\n" << std::endl;
	}
}
