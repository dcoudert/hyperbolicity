/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "basic_types.h"

#include <utility>

class IteratorInterface
{
public:
	virtual bool hasNext() = 0;
	virtual NodePair getNext() = 0;
	virtual Distance getDistance() const = 0;
};



class DistanceFromFarVerticesIteratorInterface
{
public:
    virtual bool hasNext() = 0;
    virtual FarVertex getNext();
    virtual Distance getDistance() const = 0;
};


class FarApartIteratorInterface
{
public:
    virtual bool hasNext() {return false;}
    virtual FarApartPair getNext() {return {0, 0, 0};}
    virtual Distance getDistance() const {return 0;}
    virtual Distance getEccentricity(NodeID const u) const {return 0;}
    virtual std::size_t get_cpt_initial_bfs() {return 0;}
    virtual void increase_cut_off(Distance new_cut_off) {}
    virtual ConstNodeIDCRange getMates(NodeID const u, Distance const dist) const {
        // return empty range
        NodeIDs s;
        return ConstNodeIDCRange(s.begin(), s.end());
    }
};

