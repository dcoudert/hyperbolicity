/*
 leanness.hpp
 Copyright (C) 2023  COUDERT David <david.coudert@inria.fr>
                     COULOMB Samuel <samuel.coulomb@ens.psl.eu>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef leanness_hpp
#define leanness_hpp

#include <vector>
#include "basic_types.h"
#include "graph.h"
#include "far_apart_iterator.h"
#include "cache.h"
#include "pruned_landmark_labeling.h"
#include "my_timer.h"


/*
 Class to implement exact and heuristic algorithms for the Leanness.
 It uses a matrix to store distances between vertices.
 */
class Leanness
{
private:
    Graph const& graph;
    Distance leanness_lb;
    DistanceMatrix distance;
    int _version_interval;
    int _version_slice;
    int _threshold;      // used when _version_slice=0. Threshold to use basic or IFUB

    void build_interval_and_update_leanness_MDG(NodeID x, NodeID y, Distance dxy, Matrix<char> & active_pairs);
    void build_interval_and_update_leanness_prune(NodeID x, NodeID y, Distance dxy);

    void update_leanness_MDG(Interval const& I);
    void update_leanness_range(Interval const& I, SliceIndex start, SliceIndex end);
    void update_leanness_range_v2(Interval const& I, SliceIndex start, SliceIndex end);

    // For statistics
    MyTimer mytimer;
    uint64_t considered_fap;
    uint64_t evaluated_intervals;
    uint64_t evaluated_slices;
    uint64_t sum_slices_size;
    uint64_t min_slices_size;
    uint64_t max_slices_size;

public:
    Leanness(Graph const& g);
    Distance compute_MDG();
    Distance compute(int version_interval=3, int version_slice=0, int threshold=20);
    Distance compute_fap_iter(int version_interval, int version_slice, int threshold=20, int capacity=2);
    Distance compute_heuristic(int t, int k);
    
    // Special initialization for running experiments on diameter methods
    void init_experiment_diameter(int version_slice=0, int threshold=20);
    // These methods a made public for experiments on diameter
    Distance slice_diameter(Slice const& s);
    Distance slice_diameter_basic(Slice const& s);
    Distance slice_diameter_IFUB(Slice const& s);
    Distance slice_diameter_DHV(Slice const& s);
};


/*
 Class to implement exact algorithms for the leanness of graphs by Coudert et al.
 */
class LeannessLowMemory
{
private:
    Graph const& graph;
    Distance leanness_lb;
    int _version_interval;
    int _version_slice;  // 0: auto, 1: naive, 2: IFUB, 3: DHV
    int _threshold;      // used when _version_slice=0. Threshold to use naive or IFUB
    int _version_hub;
    Distances ecc;
    
    // Hub labelings
    PrunedLandmarkLabeling<> pll;  // Akiba et al.
    HubLabeling * hublab;  // Viennot
    
    void update_leanness_range(Interval const& I, SliceIndex start, SliceIndex end);
    void update_leanness_range_v2(Interval const& I, SliceIndex start, SliceIndex end);
    
    // For statistics
    MyTimer mytimer;
    uint64_t considered_fap;
    uint64_t evaluated_intervals;
    uint64_t evaluated_slices;
    uint64_t sum_slices_size;
    uint64_t min_slices_size;
    uint64_t max_slices_size;
    
public:
    LeannessLowMemory(Graph const& g);
    Distance compute(std::size_t capacity=1000, int version_interval=3, int version_slice=0, int threshold=10, int version_hub=1);
    
    // Special initialization for running experiments on diameter methods
    void init_experiment_diameter(int version_slice=0, int threshold=10, int version_hub=1);
    // These methods a made public for experiments on diameter
    Distance slice_diameter(Slice const& s);
    Distance slice_diameter_basic(Slice const& s);
    Distance slice_diameter_IFUB(Slice const& s);
    Distance slice_diameter_DHV(Slice const& s);
};







#endif /* leanness_hpp */
