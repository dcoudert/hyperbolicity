/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef far_apart_iterator_h
#define far_apart_iterator_h

#include "basic_types.h"
#include "bfs_info.h"
#include "iterator_interface.h"

#include <vector>
#include <unordered_map>
#include <string>

class Graph;

namespace unit_tests {
    void testFarApartIterator();
    void testFarApartIteratorBFS();
}

/*
 * Naive Far Apart pairs iterator
 *
 * This iterator proceeds as follows:
 * 1) Initialization:
 *    - computes the distance matrix
 *    - determines far apart pairs, stored in a matrix
 * 2) Iteration:
 *    - yield far apart pairs by decreasing distance (iterates over the matrix)
 *
 * INPUT:
 * - a graph
 * - cut_off: a lower bound on the distance of far apart pairs to yield.
 *   This can help reducing memory consumption.
 */
class NaiveFarApartIterator : public FarApartIteratorInterface
{
private:
    using Index = DistanceMatrix::Index;

    DistanceMatrix dist_matrix;
    DistanceMatrix far_matrix;
    Index current_index;
    Distance current_distance;
    Distance cut_off;

public:
    NaiveFarApartIterator(Graph const& graph, Distance cut_off = 1);

    bool hasNext() override;
    FarApartPair getNext() override;
    Distance getDistance() const override;
    
};



/*
 * Improved Far Apart pairs iterator
 *
 * This iterator proceeds as follows:
 * 1) Initialization:
 *    - computes the eccentricities of all vertices using either the algorithm
 *      of Takes and Kosters or the algorithm of Dragan, Habib and Viennot
 *    - Each time a BFS is computed, the corresponding set of u-far vertices
 *      is determined and stored
 *    - For each vertex u from which no BFS has been computed yet, we store
 *      some data to recall to compute this BFS and corresponding set u-far
 *      the first time it is needed
 * 2) Iteration:
 *    - yield far apart pairs by decreasing distance
 *    - computes and stores the set u-far (if unknwon) the first time needed
 *    - each time a far apart pair uv is yielded, it records that v is a mate
 *      of u (i.e., are far apart)
 *    - when all far apart pairs at distance d involving u have been yielded,
 *      the data structure for u at distance d is replaced by the mates. This
 *      way, we know that if v is a mate of u, then uv is far apart, and we
 *      don't have to check both that v is u-far and u is v-far.
 *      Observe that the algorithm ensures that when all far apart pairs at
 *      distance d have been yielded, the sets of u-far vertices of each vertex
 *      u with eccentricity >= d have been computed.
 * 3) Extra operations:
 *    - At any time, it is possible to access the set of known mates of
 *      a vertex, either all known mates, or only those at distance d
 *    - TODO: it is possible to give a BFS or a set of u-far vertices to the
 *      iterator during iterations if such data structure has been computed
 *      elsewhere.
 *
 * INPUT:
 * - a graph
 * - algo: optional name of an algorithm for the initialization
 *   o "TK" for the algorithm by Takes and Kosters
 *   o "DHV" (default) for the algorithm by Dragan, Habib and Viennot
 * - cut_off: an optional lower bound on the distance of far apart pairs to yield.
 *   This can help reducing memory consumption.
 */
class FarApartIterator : public FarApartIteratorInterface
{
private:
    Graph const& graph;
    std::string algo;
    Distance cut_off;

    Distance diameter;
    Distances ecc;
    std::size_t cpt_initial_bfs; // count the number of BFS during initialization
    
    std::vector<bool> known_far_vertices;
    
    void compute_far_vertices(NodeID source);

    std::vector<std::unordered_map<NodeID, NodeIDs>> data_far;
    Distance current_dist;  // current distance in data_far
    // to iterate over the items of the map in data_far[current_dist]
    std::unordered_map<NodeID, NodeIDs>::iterator current_source_it, current_source_end;
    NodeID current_source;
    // to iterate over the targets in data_far[current_dist][current_source]
    NodeIDs::iterator current_target_it, current_target_end;
    
    void store_far_vertices(NodeID source, FarVertices const& far_vertices);
    
    // v is a mate of u if uv is far apart and has already been yielded
    NodeIDs _mates;
    void store_mate(NodeID const mate);
    // Replace data_far[current_dist][current_source] by _mates
    void swap_mates();
    // used to store correct mates when dist = current_dist
    std::unordered_map<NodeID, NodeIDs> _temp_mates;
    void store_temp_mate(NodeID const mate);
    
    void initialize_with_Takes_Kosters();
    void initialize_with_Dragan_Habib_Viennot();
    
    BFSCache *bfs_cache;

public:
    std::size_t get_cpt_initial_bfs() override; // count the number of BFS during initialization
    explicit FarApartIterator(Graph const& graph, BFSCache *cache, std::string const& algo = "DHV", Distance cut_off = 1);
    //FarApartIterator(Graph const& graph, std::string const& algo) : this(graph, algo, 1) {};
    //FarApartIterator(Graph const& graph, Distance cut_off) : this(graph, "DHV", cut_off) {};
    //FarApartIterator(Graph const& graph) : this(graph, "DHV", 1) {};

    ~FarApartIterator();
    
    bool hasNext() override;
    FarApartPair getNext() override;
    Distance getDistance() const override;
    
    bool is_far_apart(NodeID const u, NodeID const v);
    bool is_far_apart(NodeID const u, NodeID const v, Distance const dist);

    Distance getEccentricity(NodeID const u) const override; // Return the eccentricity of u
    // Return u-Far vertices such that uv is far apart and has already been yielded
    FarVertices getMates(NodeID const u);
    ConstNodeIDCRange getMates(NodeID const u, Distance const dist) const override;

    void increase_cut_off(Distance new_cut_off) override;
    
    // For pretty print of some information
    friend std::ostream& operator<<(std::ostream&, FarApartIterator&);
};

#endif /* far_apart_iterator_h */
