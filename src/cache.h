/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "defs.h"

#include <vector>
#include <unordered_map>

namespace unit_tests
{
    void testCache();
}

template<
  typename TKey,    // type keys
  typename TData    // type store data
  >
class Cache {
public:
    explicit Cache(std::size_t capacity = 10);
    ~Cache();

    std::size_t capacity;
    // universal time counter to maintain the age of information
    uint64_t tau;

    bool contains(TKey id);
    TData& get(TKey id);
    void add(TKey id, TData data); // FIXME: remove
    TData& getNew(TKey id);

private:
    std::unordered_map<TKey, std::size_t> key_to_index;
    std::vector<TData> data_vec;
    std::vector<uint64_t> age;
    
    void increase_ages();
};


/*
 * When I move this part to a .cpp file, I have a linker error...
 */

template<typename TKey, typename TData>
Cache<TKey, TData>::Cache(std::size_t capacity) :
    capacity(capacity), tau(0), data_vec(capacity), age(capacity, 0)
{
    key_to_index.clear();
}

template<typename TKey, typename TData>
Cache<TKey, TData>::~Cache()
{
}

template<typename TKey, typename TData>
bool Cache<TKey, TData>::contains(TKey id)
{
    return (key_to_index.find(id) != key_to_index.end());
}

template<typename TKey, typename TData>
TData& Cache<TKey, TData>::get(TKey id)
{
    assert( contains(id) );

    // first increase the age of all cached data
    increase_ages();
    // then set the age of data to return to 0
    std::size_t index = key_to_index[id];
    age[index] = tau;
    // finally return the data
    return data_vec[index];
}

template<typename TKey, typename TData>
void Cache<TKey, TData>::add(TKey id, TData data)
{
    if (capacity == 0) { return; }

    // We search for the index at which to store data
    std::size_t index;
    if (key_to_index.size() == capacity) {
        // The cache is full, we remove the oldest data
        uint64_t a = tau + 1;
        TKey key;
        for (auto item: key_to_index) {
            if (age[item.second] < a) {
                a = age[item.second];
                key = item.first;
            }
        }
        index = key_to_index[key];
        key_to_index.erase(key);
    }
    else {
        // We take the first free index
        index = key_to_index.size();
    }
    key_to_index[id] = index;
    data_vec[index] = data;
    increase_ages();
    age[index] = tau;
}

template<typename TKey, typename TData>
TData& Cache<TKey, TData>::getNew(TKey id)
{
	assert(capacity > 0);

	if (contains(id)) {
		return get(id);
	}

    // We search for the index at which to store data
    std::size_t index;
    if (key_to_index.size() == capacity) {
        // The cache is full, we remove the oldest data
        uint64_t a = tau + 1;
        TKey key;
        for (auto item: key_to_index) {
            if (age[item.second] < a) {
                a = age[item.second];
                key = item.first;
            }
        }
        index = key_to_index[key];
        key_to_index.erase(key);
    }
    else {
        // We take the first free index
        index = key_to_index.size();
    }
    key_to_index[id] = index;
    increase_ages();
    age[index] = tau;

    return data_vec[index];
}

template<typename TKey, typename TData>
void Cache<TKey, TData>::increase_ages()
{
    tau++;
}




// =======================================================================
// Cache based on a doubly linked list
// - The capacity is fixed (input parameter)
// - When an item is accessed, it is moved to the back of the list
// - When asking for an empty item, the first item of the list is used
// - It is possible to lock an item, in which case it can not be erased
//   until is is unlocked
//   - a locked item is moved to a separate list
//   - when an item is unlocked, it is moved to the back of the main list
// - Warning: if all items are locked, then an error is raised if asking
//   for an empty item.
// ======================================================================

template<
  typename TKey,    // type keys
  typename TData    // type store data
  >
class DLLCache {
public:
    explicit DLLCache(std::size_t capacity = 10);
    ~DLLCache();

    std::size_t capacity;

    bool contains(TKey id);
    TData& get(TKey id);
    TData& getNew(TKey id);

    void lock(TKey id);
    void unlock(TKey id);

private:
    
    struct DLLitem {
        int next;
        int pred;
        bool is_locked;
        TData data;

        explicit DLLitem() : next(-1), pred(-1), is_locked(false) {}
    };

    // mapping from the keys of the cached data to the internal int label
    std::unordered_map<TKey, int> key_to_index;
    // mapping from internal int labels to keys of cached data
    std::vector<TKey> index_to_key;
    std::vector<DLLitem> items;  // vector of all items
    int first;  // first item of the main list
    int last;   // last item of the main list
    int first_locked;  // first locked item
    
    void move_last(int index);
};

template<typename TKey, typename TData>
DLLCache<TKey, TData>::DLLCache(std::size_t capacity)
: capacity(capacity), index_to_key(capacity), items(capacity), first(0), last(capacity-1), first_locked(-1)
{
    assert(capacity >= 2);
    key_to_index.clear();
    items[0].next = 1;
    for (std::size_t i = 1; i < capacity - 1; i++) {
        items[i].next = i + 1;
        items[i].pred = i - 1;
    }
    items[last].pred = last - 1;
}

template<typename TKey, typename TData>
DLLCache<TKey, TData>::~DLLCache()
{
}

template<typename TKey, typename TData>
bool DLLCache<TKey, TData>::contains(TKey id)
{
    return (key_to_index.find(id) != key_to_index.end());
}

template<typename TKey, typename TData>
void DLLCache<TKey, TData>::move_last(int index)
{
    if ((index != last) and (first != last)) {
        // Move item to the last position of the list
        // We assume that the item is in the cache
        auto & it = items[index];
        if (not it.is_locked) {
            if (index == first) {
                first = it.next;
                items[first].pred = -1;
            } else {
                items[it.next].pred = it.pred;
                items[it.pred].next = it.next;
            }
            items[last].next = index;
            it.pred = last;
            it.next = -1;
            last = index;
        }
    }
}

template<typename TKey, typename TData>
TData& DLLCache<TKey, TData>::get(TKey id)
{
    assert( contains(id) );

    int index = key_to_index[id];
    move_last(index);
    
    // finally return the data
    return items[index].data;
}

template<typename TKey, typename TData>
TData& DLLCache<TKey, TData>::getNew(TKey id)
{
    assert(capacity > 0);

    if (contains(id)) {
        return get(id);
    }
    // Check if we have available items (i.e., not all locked)
    assert(first != -1);

    // The oldest item in the list is at first position
    if (key_to_index.size() == capacity) {
        // If the cache is full, so we must remove an entry
        TKey key = index_to_key[first];
        key_to_index.erase(key);
    }
    key_to_index[id] = first;
    index_to_key[first] = id;
    move_last(first);

    // The data to return is now at last position of the list
    return items[last].data;
}

template<typename TKey, typename TData>
void DLLCache<TKey, TData>::lock(TKey id)
{
    if (contains(id)) {
        int index = key_to_index[id];
        auto & it = items[index];
        if (not it.is_locked) {
            // remove for the normal list
            if (index == first) {
                first = it.next;
                if (first != -1) {
                    items[first].pred = -1;
                }
            } else if (index == last) {
                last = it.pred;
                items[last].next = -1;
            } else {
                items[it.next].pred = it.pred;
                items[it.pred].next = it.next;
            }
            // add to the locked list
            it.pred = -1;
            it.next = first_locked;
            if (first_locked != -1) {
                items[first_locked].pred = index;
            }
            first_locked = index;
            it.is_locked = true;
        }
    }
}

template<typename TKey, typename TData>
void DLLCache<TKey, TData>::unlock(TKey id)
{
    if (contains(id)) {
        int index = key_to_index[id];
        auto & it = items[index];
        if (it.is_locked) {
            // remove from the locked list
            if (index == first_locked) {
                first_locked = it.next;
                if (first_locked != -1) {
                    items[first_locked].pred = -1;
                }
            } else {
                items[it.pred].next = it.next;
                if (it.next != -1) {
                    items[it.next].pred = it.pred;
                }
            }
            // add at last position of normal list
            it.next = -1;
            it.pred = last;
            if (last == -1) {
                first = index;
            } else {
                items[last].next = index;
            }
            last = index;
            it.is_locked = false;
        } else {
            // move to last positio of main list
            move_last(index);
        }
    }
}

