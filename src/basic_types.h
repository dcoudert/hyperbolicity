/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "defs.h"
#include "id.h"
#include "matrix.h"
#include "range.h"
#include "cache.h"

// Hub labeling
#include <mgraph.hh>
#include <pruned_landmark_labeling.hh>

#include <iostream>
#include <vector>
#include <set>
#include <tuple>
#include <queue>

// general

using uint = unsigned int;

// Hub labeling
using WeightedGraph = mgraph<uint, uint>;
using HubLabeling = pruned_landmark_labeling<WeightedGraph>;

// nodes and edges

struct Node;
using NodeID = ID<Node>;
using NodeIDs = std::vector<NodeID>;
using NodeIDRange = ValueRange<NodeID>;
using ConstNodeIDCRange = ConstContainerRange<NodeIDs>;

using Neighbors = ContainerRange<NodeIDs>;
using ConstNeighbors = ConstContainerRange<NodeIDs>;

using NodePair = std::pair<NodeID, NodeID>;
using NodePairs = std::vector<NodePair>;

namespace std {
    template <>
    struct hash<NodePair>
    {
    std::size_t operator()(NodePair const& p) const noexcept
    {
            return hash<NodeID>()(p.first) + 3 * hash<NodeID>()(p.second);
    }
};
}

enum class EdgeType { Out, In };

// parser types

struct ParserEdge {
	NodeID source, target;

	bool operator<(ParserEdge const& other) const {
		return source < other.source || (source == other.source && target < other.target);
	}
	bool operator==(ParserEdge const& other) const {
		return source == other.source && target == other.target;
	}
};
using ParserEdges = std::vector<ParserEdge>;

struct ParserData {
	std::size_t number_of_nodes;
	std::size_t number_of_edges;
	ParserEdges edges;
};

// distance

using Distance = int_fast32_t;
using Distances = std::vector<Distance>;
using DistanceMatrix = Matrix<Distance>;

extern Distance DIST_INF;

// far vertices

struct FarVertex {
	NodeID node_id;
	Distance distance;

	FarVertex(NodeID node_id, Distance distance)
		: node_id(node_id), distance(distance) {}
    
    bool operator<(FarVertex const& other) const {
        return (distance < other.distance) ||
			(distance == other.distance &&  node_id < other.node_id);
    }
    bool operator==(FarVertex const& other) const {
        return node_id == other.node_id && distance == other.distance;
    }
};
using FarVertices = std::vector<FarVertex>;

struct FarApartPair {
    NodeID source, target;
    Distance distance;
    
    FarApartPair(NodeID source, NodeID target, Distance distance)
        : source(source), target(target), distance(distance) {}
    
    bool operator<(FarApartPair const& other) const {
		return (distance < other.distance)
			|| (distance == other.distance && source < other.source)
			|| (distance == other.distance && source == other.source && target < other.target);
    }
    bool operator==(FarApartPair const& other) const {
        return distance == other.distance &&
		       ((source == other.source && target == other.target) ||
		        (source == other.target && target == other.source));
    }

    friend std::ostream& operator<<(std::ostream& os, FarApartPair& F) {
        os << "{" << F.source << ", " << F.target << ", " << F.distance << "}";
        return os;
    }
};

namespace std {
    template <>
    struct hash<FarApartPair>
    {
	std::size_t operator()(FarApartPair const& p) const noexcept
	{
            return hash<NodeID>()(p.source) + 3 * hash<NodeID>()(p.target);
	}
};
}

using FarApartPairs = std::vector<FarApartPair>;


// Cache : see bfs_info.h

//using BFSCache = Cache<NodeID, BFSInfo>;


// Define basic types needed for the computation of the leanness
using Slice = NodeIDs;
using Interval = std::vector<Slice>;
using SliceIndex = std::size_t;
using SliceIndexPair = std::pair<SliceIndex, SliceIndex>;
using SliceIndexPairs = std::vector<SliceIndexPair>;

struct SliceIndexQElem {
    SliceIndex start, end;
    Distance length;
    Distance upper_bound;

    SliceIndexQElem(SliceIndex start, SliceIndex end, Distance length, Distance upper_bound)
    : start(start), end(end), length(length), upper_bound(upper_bound) {}

    bool operator<(SliceIndexQElem const& other) const {
        return ((upper_bound < other.upper_bound) or
                ((upper_bound == other.upper_bound) and
                 ((length < other.length) or
                  ((length == other.length) and (start < other.start)))));
    }

    bool operator==(SliceIndexQElem const& other) const {
        return ((upper_bound == other.upper_bound) and (length == other.length) and
                (start == other.start) and (end == other.end));
    }

    friend std::ostream& operator<<(std::ostream& os, SliceIndexQElem& E) {
        os << "{" << E.start << ", " << E.end << ", " << E.length << ", " << E.upper_bound << "}";
        return os;
    }
};

using SlideIndexQElements = std::vector<SliceIndexQElem>;
using SliceIndexPriorityQueue = std::priority_queue<SliceIndexQElem, SlideIndexQElements>;
