/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "io.h"

#include "defs.h"

#include <algorithm>
#include <fstream>
#include <numeric>
#include <sstream>

namespace
{

std::streamsize ignore_count = std::numeric_limits<std::streamsize>::max();

} // end anonymous namespace

ParserData io::readGraph(std::string const& filename, bool make_undirected)
{
	std::ifstream file(filename);
	if (!file.is_open()) {
		ERROR("Could not open file " << filename);
	}

	ParserData data;

	std::string source_string, target_string;
	NodeID max_id;

	while (file.peek() != std::ifstream::traits_type::eof()) {
		if (((file >> std::ws).peek() == '#') or
            ((file >> std::ws).peek() == 'c') or
            ((file >> std::ws).peek() == 'p')) {
			file.ignore(ignore_count, '\n');
			continue;
		}

		file >> source_string >> target_string;
		auto source_id = std::stoi(source_string);
		auto target_id = std::stoi(target_string);
		assert(source_id >= 0 && target_id >= 0);
		data.edges.push_back({source_id, target_id});

		if (!max_id.valid()) { max_id = source_id; }
		if ((NodeID)source_id > max_id) { max_id = source_id; }
		if ((NodeID)target_id > max_id) { max_id = target_id; }
	}

	data.number_of_nodes = max_id + 1;
	auto& edges = data.edges;

	if (make_undirected) {
		auto original_size = edges.size();
		for (std::size_t i = 0; i < original_size; ++i) {
			auto const& edge = edges[i];
			edges.push_back({edge.target, edge.source});
		}
	}

	// remove duplicates
	std::sort(edges.begin(), edges.end());
	auto new_end = std::unique(edges.begin(), edges.end());
	edges.erase(new_end, edges.end());

	data.number_of_edges = edges.size();

	return data;
}
