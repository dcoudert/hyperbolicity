#pragma once

#include "basic_types.h"
#include "bfs_info.h"
#include "pruned_landmark_labeling.h"
#include "cache.h"
#include "my_timer.h"

#include <vector>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <chrono>


class Graph;


namespace unit_tests
{
    void testHyperbolicityDom();
}

// data stored by a dominator of a dominating set
struct DominatorData {
    NodeID id;
    Distance radius;
    NodeIDs vertices;

    DominatorData() {}
    DominatorData(NodeID _id, Distance _radius, NodeIDs _vertices)
        : id(_id), radius(_radius), vertices(_vertices) {}
};

using DominatingSet = std::unordered_map<NodeID, DominatorData>;


class HierarchicalDominatingSet
{
private:
    Graph const& graph;
    uint version;
    bool version_use_rec;
    bool version_connected;
    uint version_priority;
    Distances domination_distances;
    uint nb_layers;
    std::vector<DominatingSet> dominating_sets_vec;
    std::vector<uint> values;
    uint max_value;

    NodeIDs compute_dominating_set_rec(uint layer, NodeIDs & vertices);
    void compute_dominating_set_iter(bool connected = true, bool closest = false);
    void prioritize_vertices(NodeIDs & vertices);
    
    Distances stat_radius_min;
    Distances stat_radius_max;
    std::vector<float> stat_radius_mean;
    std::vector<uint64_t> stat_ball_min;  // number of vertices dominated
    std::vector<uint64_t> stat_ball_max;
    std::vector<float> stat_ball_mean;
    std::vector<uint64_t> stat_dom_min;  // number of dominators at lower distance dominated
    std::vector<uint64_t> stat_dom_max;
    std::vector<float> stat_dom_mean;
    void stat_init();
    void stat_add_radius(uint layer, Distance radius);
    void stat_add_ball_size(uint layer, uint64_t ball_size);
    void stat_add_dom_size(uint layer, uint64_t dom_size);
    void stat_end();

public:
    HierarchicalDominatingSet(Graph const& graph, Distance d = DIST_INF, float ratio = 3.0,
                              Distance known_h_lb = 0, uint version = 0);
    HierarchicalDominatingSet(Graph const& graph, Distances const& eccentricities,
                              Distance d = DIST_INF, float ratio = 3.0,
                              Distance known_h_lb = 0, uint version = 0);

    uint number_of_layers() {return nb_layers;}
    uint top_layer_index() {return nb_layers - 1;}
    Distance domination_distance(uint layer) const;
    uint dominating_set_size(uint layer);
    NodeIDs dominating_set(uint layer);
    ConstNodeIDCRange dominated_vertices(uint layer, NodeID const u) const;
    std::size_t dominated_vertices_size(uint layer, NodeID const u) const;
    Distance domination_radius(uint layer, NodeID const u) const;

    // For pretty print of some information
    friend std::ostream& operator<<(std::ostream&, HierarchicalDominatingSet&);
};


/*
 This is a helper class to store distance matrix between pairs of nodes dominated
 by a pair ap,bp. So we store the distances of the complete bipartite graph.
 The matrix is rectangular.
 When parameter both is true, we store both the matrix ap,bp and the matrix bp,ap.
 This cache is for a single layer (that must be specified).
 */
class CacheOfDistanceMatrix
{
private:
    HierarchicalDominatingSet & HDS;
    PrunedLandmarkLabeling<> * pll;  // Akiba et al.
    HubLabeling * hublab;  // Viennot
    bool use_pll;
    uint layer;
    DLLCache<NodePair, DistanceMatrix> cache_of_matrices;

    DistanceMatrix& make_matrices(NodeID ap, NodeID bp, bool both=true);

public:
    CacheOfDistanceMatrix(HierarchicalDominatingSet & HDS,
                          PrunedLandmarkLabeling<> & pll,
                          uint layer, int cache_size=1000);
    CacheOfDistanceMatrix(HierarchicalDominatingSet & HDS,
                          HubLabeling * hublab,
                          uint layer, int cache_size=1000);
    DistanceMatrix& get(NodeID ap, NodeID bp, bool both=true);
    void lock(NodeID ap, NodeID bp, bool both=true);
    void unlock(NodeID ap, NodeID bp, bool both=true);
};



class HyperbolicityDom
{
private:
    Graph const& graph;
    Distance h_lb;
    Distance h_ub;
        
    Distances ecc;
    NodeID central_vertex;
    Distances dist_central;

    // for statistics.
    std::vector<uint64_t> evaluated_quadruples;
    std::vector<uint64_t> checked_quadruples;

    // Hub labelings
    PrunedLandmarkLabeling<> pll;  // Akiba et al.
    HubLabeling * hublab;  // Viennot
    
    // arrays used to store distances in compute_DOM_DFS_rec in order to save calls to hub labeling
    std::size_t storage_capacity;
    std::vector<Distances> storage_of_distances;

    // Caches of distance matrix between vertices dominated by a pair of nodes
    // We use 1 cache per layer
    std::vector<CacheOfDistanceMatrix> cache_of_matrices;
    uint max_matrix_side;
    
    MyTimer mytimer;

    void compute_DOM_DFS_rec(HierarchicalDominatingSet const& HDS, uint layer,
                             NodeID up, NodeID vp, NodeID xp, NodeID yp);


public:
    HyperbolicityDom(Graph const& graph);
    float compute_DOM_DFS(int version = 1, Distance d = DIST_INF, float ratio = 3.0, uint dom_version = 0,
                            uint max_side = 10000, uint max_cache_size = 1000, Distance known_h_lb = 0);
};





