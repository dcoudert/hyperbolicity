/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "algorithms.h"

#include "defs.h"
#include "graph.h"

#include <algorithm>
#include <numeric>
#include <queue>

namespace
{

// Note: using degree information makes things slower. Try to add back when
// testing on more instances.
NodeID selectAndDeleteNode(
	Graph const& graph, NodeIDs& node_ids, Distances const& lbs, Distances const& ubs)
{
	// 0 = node with small lower bound, 1 = node with large upper bound
	static int selection_mode = 0;

	if (node_ids.empty()) { ERROR("The node_ids vector is empty."); }

	NodeID best_id;
	if (selection_mode == 0) {
		Distance smallest_lb = DIST_INF;
		for (auto node_id: node_ids) {
			if (lbs[node_id] < smallest_lb/* ||
				(lbs[node_id] == smallest_lb && graph.outDegree(node_id) > graph.outDegree(best_id))*/) {
				smallest_lb = lbs[node_id];
				best_id = node_id;
			}
		}
	}
	else {
		best_id = 0;
		Distance largest_ub = 0;
		for (auto node_id: node_ids) {
			if (ubs[node_id] > largest_ub/* ||
				(ubs[node_id] == largest_ub && graph.outDegree(node_id) > graph.outDegree(best_id))*/) {
				largest_ub = ubs[node_id];
				best_id = node_id;
			}
		}
	}

	selection_mode = (selection_mode + 1)%2;

	assert(best_id.valid());
	return best_id;
}

} // end anonymous namespace

namespace alg
{

void runBFS(Distances &distances, Graph const& graph, NodeID source)
{
    std::iota(distances.begin(), distances.end(), DIST_INF);

    std::queue<NodeID> queue;
    queue.push(source);
    distances[source] = 0;

    while (!queue.empty()) {
        auto current = queue.front();
        queue.pop();

        for (auto neighbor_id: graph.neighborsOf(current)) {
            if (distances[neighbor_id] == DIST_INF) {
                distances[neighbor_id] = distances[current] + 1;
                queue.push(neighbor_id);
            }
        }
    }
}



void runBFS(BFSInfo &bfs_info, Graph const& graph, NodeID source)
{
    std::queue<NodeID> queue;
    queue.push(source);
    bfs_info.prepare(graph.numberOfNodes());

    bfs_info.visit(source, 0);

    while (!queue.empty()) {
        auto current = queue.front();
        queue.pop();
        
        for (auto neighbor_id: graph.neighborsOf(current)) {
            if ( ! bfs_info.visited(neighbor_id)) {
                bfs_info.visit(neighbor_id,
                               bfs_info.unsafe_distance(current) + 1);
                queue.push(neighbor_id);
            }
        }
    }    
}


void runBFSPrunedAccEcc(BFSInfo &bfs_info, Graph const& graph,
                        Distances const &eccs, NodeID source, Distance condacc)
{
    std::queue<NodeID> queue;
    queue.push(source);
    bfs_info.prepare(graph.numberOfNodes());

    bfs_info.visit(source, 0);

    while (!queue.empty()) {
        auto current = queue.front();
        queue.pop();

        for (auto neighbor_id: graph.neighborsOf(current)) {
            if ( ! bfs_info.visited(neighbor_id)) {
                Distance dist = bfs_info.unsafe_distance(current) + 1;
                if (2*(eccs[neighbor_id] - dist) >= condacc) {
                    bfs_info.visit(neighbor_id, dist);
                    queue.push(neighbor_id);
                }
            }
        }
    }
}

    

void runBFS(BFSInfo &bfs_info, Graph const& graph, NodeID source, NodeID& last)
{    
    std::queue<NodeID> queue;
    queue.push(source);
    bfs_info.prepare(graph.numberOfNodes());

    bfs_info.visit(source, 0);

    while (!queue.empty()) {
        auto current = queue.front();
        queue.pop();
        last = current;
        
        for (auto neighbor_id: graph.neighborsOf(current)) {
            if ( ! bfs_info.visited(neighbor_id)) {
                bfs_info.visit(neighbor_id,
                               bfs_info.unsafe_distance(current) + 1);
                queue.push(neighbor_id);
            }
        }
    }    
}

// Run BFS up to distance k
void runBFSk(BFSInfo &bfs_info, Graph const& graph, NodeID source, Distance k)
{
    std::queue<NodeID> queue;
    queue.push(source);
    bfs_info.prepare(graph.numberOfNodes());

    bfs_info.visit(source, 0);

    while (!queue.empty()) {
        auto current = queue.front();
        if (bfs_info.unsafe_distance(current) == k) {
            // We are done
            break;
        }
        queue.pop();
        
        for (auto neighbor_id: graph.neighborsOf(current)) {
            if ( ! bfs_info.visited(neighbor_id)) {
                bfs_info.visit(neighbor_id,
                               bfs_info.unsafe_distance(current) + 1);
                queue.push(neighbor_id);
            }
        }
    }
}



Distance calcDist(Graph const& graph, NodeID source, NodeID target)
{
	Distances distances(graph.numberOfNodes(), DIST_INF);

	std::queue<NodeID> queue;
	queue.push(source);
	distances[source] = 0;

	while (!queue.empty()) {
		auto current = queue.front();
		queue.pop();

		if (current == target) { break; }

		for (auto neighbor_id: graph.neighborsOf(current)) {
			if (distances[neighbor_id] == DIST_INF) {
				distances[neighbor_id] = distances[current] + 1;
				queue.push(neighbor_id);
			}
		}
	}

	return distances[target];
}

DistanceMatrix computeDistanceMatrix(Graph const& graph)
{
	auto n = graph.numberOfNodes();

	DistanceMatrix matrix(n, n);
        BFSInfo bfs_info;
        
        for (NodeID source_id : graph.vertices()) {
            runBFS(bfs_info, graph, source_id);

            for (NodeID target_id : graph.vertices()) {
                matrix(source_id, target_id) = bfs_info.distance(target_id);
            }
	}
        
	return matrix;
}

DistanceMatrix computeDistanceMatrix(Graph const& graph, NodeIDs const& vertices)
{
    auto nv = vertices.size();
    
    DistanceMatrix matrix(nv, nv);
    BFSInfo bfs_info;
        
    for (uint i = 0; i < nv; i++) {
        NodeID source_id = vertices[i];
        runBFS(bfs_info, graph, source_id);

        for (uint j = 0; j < nv; j++) {
            NodeID target_id = vertices[j];
            matrix(i, j) = bfs_info.distance(target_id);
        }
    }
    return matrix;
}


HubLabeling computeHubLabeling(Graph const& graph)
{
    std::vector<WeightedGraph::edge> edg;
    for (NodeID u : graph.vertices()) {
        for (NodeID v : graph.neighborsOf(u)) {
            edg.push_back(WeightedGraph::edge(u, v, 1));
        }
    }
    WeightedGraph g(edg);
    HubLabeling hl(g, {}, {}, 1, false /* unweighted: use BFS, not Dijkstra */);
    return hl;
}

HubLabeling * computeHubLabeling_ptr(Graph const& graph)
{
    std::vector<WeightedGraph::edge> edg;
    for (NodeID u : graph.vertices()) {
        for (NodeID v : graph.neighborsOf(u)) {
            edg.push_back(WeightedGraph::edge(u, v, 1));
        }
    }
    WeightedGraph g(edg);
    return new HubLabeling(g, {}, {}, 1, false /* unweighted: use BFS, not Dijkstra */);
}

Distance computeEccentricity(Graph const& graph, NodeID source)
{
    NodeID last;
    BFSInfo bfs_info;
    runBFS(bfs_info, graph, source, last);
    // return *std::max_element(distances.begin(), distances.end());
    return bfs_info.distance(last);
}

Distance computeDiameter(Graph const& graph)
{
	auto eccs = computeAllEccentricities(graph);
	return *std::max_element(eccs.begin(), eccs.end());
}


Distance computeDiameterDHV(Graph const& graph)
{
    Distances eccs_ub(graph.numberOfNodes(), DIST_INF);
    Distances eccs_lb(graph.numberOfNodes(), 0);
    
    auto my_compare_ub = [&](const NodeID & v1, const NodeID & v2) {
        return eccs_ub[v1] < eccs_ub[v2];
    };
    auto my_compare_lb = [&](const NodeID & v1, const NodeID & v2) {
        return eccs_lb[v1] < eccs_lb[v2];
    };

    // add all nodes to queue
    NodeIDs todo(graph.numberOfNodes());
    std::iota(todo.begin(), todo.end(), 0);

    Distance LB = 0;
    Distance UB = 0;

    BFSInfo bfs_u, bfs_x, bfs_a;
    
    do {
        // 1. Select u such that eccs_ub[u] is maximal
        auto i = std::max_element(todo.begin(), todo.end(), my_compare_ub) - todo.begin();
        auto u = todo[i];
        todo[i] = todo.back();
        todo.pop_back();
        // Compute exact eccentricity of u
        NodeID last;
        runBFS(bfs_u, graph, u, last);
        //const Distances &dist_u = bfs_u.distance;
        auto ecc_u = bfs_u.distance(last);
        // update eccentricity lower bounds
        // We also update upper bounds here instead of 3. to account for the default case x = u
        for (auto v: todo) {
            eccs_lb[v] = std::max(eccs_lb[v], bfs_u.distance(v));
        }
        // update diameter lower bound
        if (ecc_u > LB)
            LB = ecc_u;
        
        // 2. Select x such that dist(u, x) + e(x) = e(u) -- This is minES
        // Since we don't know e(x), we select x minimizing e_L(x) and such that d(u, x) + e_L(x) <= e(u)
        // We then compute e(x)
        // If e(x) = e_L(x), we are done. Otherwise, we update eccentricity bounds and repeat
        while (not todo.empty()) {
            // select the element with minimum eccentricity lower bound
            auto j = std::min_element(todo.begin(), todo.end(), my_compare_lb) - todo.begin();
            auto x = todo[j];
            todo[j] = todo.back();
            todo.pop_back();
            // Compute exact eccentricity of x
            NodeID antipode;
            runBFS(bfs_x, graph, x, antipode);
            auto ecc_x = bfs_x.distance(antipode);
            // Update eccentricity bounds
            LB = std::max(LB, ecc_x);
            if (eccs_lb[x] == ecc_x) {
                // We found the good vertex x !
                // We update eccentricity upper bounds and break
                UB = ecc_x;
                for (auto v: todo) {
                    eccs_ub[v] = std::min(eccs_ub[v], bfs_x.distance(v) + ecc_x);
                    if (UB < eccs_ub[v])
                        UB = eccs_ub[v];
                }
                break;
            }
            else {
                // x was not a good choice
                // We use its antipode to update lower bounds
                for (std::size_t l = 0; l < todo.size(); l++)
                    if (todo[l] == antipode) {
                        todo[l] = todo.back();
                        todo.pop_back();
                        break;
                    }
                NodeID b;
                runBFS(bfs_a, graph, antipode, b);
                auto ecc_a = bfs_a.distance(b);
                for (auto v: todo) {
                    eccs_lb[v] = std::max(eccs_lb[v], bfs_a.distance(v));
                }
                LB = std::max(LB, ecc_a);
            }
        }
        
        // 3. update eccentricity upper bounds
        // already done above in 1. and 2.

    } while (LB < UB);
    
    return LB;
}



/*
* Implement the algorithm by Takes and Kosters for computing
* all eccentricities.
*/
Distances computeAllEccentricities(Graph const& graph)
{
	Distances eccs_ub(graph.numberOfNodes(), DIST_INF);
	Distances eccs_lb(graph.numberOfNodes(), 0);
        
	// add all nodes to queue
	NodeIDs todo(graph.numberOfNodes());
	std::iota(todo.begin(), todo.end(), 0);

	// as long as we still have to compute an eccentricity...
        std::size_t cpt_bfs = 0;
        BFSInfo bfs_info;
	while (!todo.empty()) {
		auto current = selectAndDeleteNode(graph, todo, eccs_lb, eccs_ub);

        NodeID last;
	runBFS(bfs_info, graph, current, last);
        cpt_bfs++;
        auto ecc = bfs_info.distance(last);
		eccs_lb[current] = ecc;

		std::size_t i = 0;
		while (i < todo.size()) {
			auto node_id = todo[i];
                        
			auto new_lb = std::max(ecc - bfs_info.distance(node_id), bfs_info.distance(node_id));
			eccs_lb[node_id] = std::max(eccs_lb[node_id], new_lb);
			auto new_ub = ecc + bfs_info.distance(node_id);
			eccs_ub[node_id] = std::min(eccs_ub[node_id], new_ub);

			if (eccs_lb[node_id] == eccs_ub[node_id]) {
				// delete node_id from todo
				std::swap(todo[i], todo.back());
				todo.pop_back();
			}
			else {
				++i;
			}
		}
	}
    //std::cout << "#BFS (TK) = " << cpt_bfs << std::endl;

	return eccs_lb;
}


/*
 * Implement the algorithm by Dragan, Habib en Viennot (2018) for computing
 * all eccentricities.
 */
Distances computeAllEccentricitiesDHV(Graph const& graph)
{
    Distances eccs_ub(graph.numberOfNodes(), DIST_INF);
    Distances eccs_lb(graph.numberOfNodes(), 0);

    // add all nodes to queue
    NodeIDs todo(graph.numberOfNodes());
    std::iota(todo.begin(), todo.end(), 0);

    auto my_compare_lb = [&](const NodeID & v1, const NodeID & v2) {
        return eccs_lb[v1] < eccs_lb[v2];
    };

    BFSInfo bfs_u, bfs_a;

    // Select u with untight upper bound and minimal eccentricity
    // If eccs_lb[u] = ecc[u], we update upper bounds
    // Otherwise, we use distances from u and its antipode to update lower bounds
    while (not todo.empty()) {
        // select and remove the element with minimum eccentricity lower bound
        auto i = std::min_element(todo.begin(), todo.end(), my_compare_lb) - todo.begin();
        NodeID u = todo[i];
        todo[i] = todo.back();
        todo.pop_back();
        // Compute exact eccentricity of u
        NodeID antipode;
        alg::runBFS(bfs_u, graph, u, antipode);
        auto ecc_u = bfs_u.distance(antipode);
        
        if (eccs_lb[u] == ecc_u) {
            // We found the good vertex !
            // We update eccentricity upper bounds
            for (auto v: todo) {
                eccs_ub[v] = std::min(eccs_ub[v], bfs_u.distance(v) + ecc_u);
            }
        }
        else {
            // u was not a good choice
            eccs_lb[u] = ecc_u;
            // We use its antipode to update lower bounds
            for (std::size_t j = 0; j < todo.size(); ++j) {
                if (todo[j] == antipode) {
                    todo[j] = todo.back();
                    todo.pop_back();
                    break;
                }
            }
            NodeID b;
            alg::runBFS(bfs_a, graph, antipode, b);
            eccs_lb[antipode] = bfs_a.distance(b);
            for (auto v: todo) {
                eccs_lb[v] = std::max(eccs_lb[v], std::max(bfs_a.distance(v), bfs_u.distance(v)));
            }
        }
        // We remove vertices for which the gap is closed
        std::size_t idx = 0;
        while (idx < todo.size()) {
            auto v = todo[idx];
            if (eccs_lb[v] == eccs_ub[v]) {
                todo[idx] = todo.back();
                todo.pop_back();
            } else {
                ++idx;
            }
        }
    }
    return eccs_lb;
 }











FarVertices getFarVertices(Graph const& graph, BFSInfo const& bfs_info, bool sorted)
{
	assert(graph.numberOfNodes() == bfs_info.size());

	FarVertices far_vertices;

        if (sorted) {
            for (NodeID node_id : bfs_info.visitSet) {
                auto distance = bfs_info.distance(node_id);
		bool found_further_vertex = false;
		for (auto neighbor_id: graph.neighborsOf(node_id)) {
                    if (bfs_info.distance(neighbor_id) == distance + 1) {
				found_further_vertex = true;
				break;
			}
		}

		if (not found_further_vertex) {
			far_vertices.emplace_back(node_id, distance);
		}
            }
        } else {
            // same but iterate in natural vertex order
            for (NodeID node_id : graph.vertices()) {
                auto distance = bfs_info.distance(node_id);
		bool found_further_vertex = false;
		for (auto neighbor_id: graph.neighborsOf(node_id)) {
                    if (bfs_info.distance(neighbor_id) == distance + 1) {
				found_further_vertex = true;
				break;
			}
		}

		if (not found_further_vertex) {
			far_vertices.emplace_back(node_id, distance);
		}
            }
        }
	return far_vertices;
}


/*
 * This method computes the set of u-Far vertices while running BFS.
 *
 * Recall that a vertex v is u-far if it is a leaf of each shortest path tree
 * rooted at u. Hence, all neighbors of v are at distance at most d(u, v)
 * from u. This can be checked while processing vertex v during BFS.
 *
 * By construction, this method ensures that far vertices are inserted by
 * increasing distance from u in the returned vector
 */
FarVertices getFarVertices(Graph const& graph, NodeID u)
{
    Distances distances(graph.numberOfNodes(), DIST_INF);
    FarVertices far_vertices;
    far_vertices.clear();
    
    std::queue<NodeID> queue;
    queue.push(u);
    distances[u] = 0;

    while (!queue.empty()) {
        auto current = queue.front();
        queue.pop();
        auto current_dist_plus_one = distances[current] + 1;
        bool is_far = true;

        for (auto neighbor_id: graph.neighborsOf(current)) {
            if (distances[neighbor_id] == DIST_INF) {
                distances[neighbor_id] = current_dist_plus_one;
                queue.push(neighbor_id);
                is_far = false;
            }
            else if (distances[neighbor_id] == current_dist_plus_one) {
                is_far = false;
            }
        }
        if (is_far)
            far_vertices.emplace_back(current, distances[current]);
    }
    return far_vertices;
}



/*
 * This method computes distances starting from far vertices
 *
 * Starting from the far vertices at largest distance k, it determines the vertices
 * at distance k-1 which are the neighbors of the far vertices at distance k that
 * have not been seen yet. It then proceeds with vertices at distances k-2, etc.
 *
 * Input:
 * - graph
 * - far_vertices: set of u-far vertices
 * - sorted (default: false): indicates if far_vertices is already sorted by increasing distance
 * - cut_off (default: 0): stop computations when all vertices with distance >= cut_off
 *  have been determined. Other distances are set to 0.
 */
Distances computeDistsFromFarVertices(Graph const& graph, FarVertices far_vertices, bool sorted, Distance cut_off)
{
	Distances distances(graph.numberOfNodes(), 0);

    if (not sorted) {
        auto dist_inc = [](FarVertex const& v1, FarVertex const& v2) {
            return v1.distance < v2.distance;
        };
        std::sort(far_vertices.begin(), far_vertices.end(), dist_inc);
    }

	NodeIDs last, current, next;
	Distance current_dist = far_vertices.back().distance;

    std::vector<bool> to_consider(graph.numberOfNodes(), true);
	do {
		last.swap(current);
		current.swap(next);
        next.clear();

		while (!far_vertices.empty() && far_vertices.back().distance == current_dist) {
			current.push_back(far_vertices.back().node_id);
			far_vertices.pop_back();
		}

		for (auto node_id: current) { to_consider[node_id] = false; }

		for (auto node_id: current) {
			distances[node_id] = current_dist;
			for (auto neighbor_id: graph.neighborsOf(node_id)) {
				if (to_consider[neighbor_id]) {
					next.push_back(neighbor_id);
				}
			}
		}
	}
	while (current_dist-- != cut_off);

	return distances;
}

/*
 * This method takes as input the distance matrix of the graph and returns the list
 * of far apart pairs of vertices.
 * A pair (i, j) apears only once.
 * When sorted is true, the list is sorted by decreasing distances.
 */
FarApartPairs getFarApartPairs(Graph const& graph, DistanceMatrix const& dist, bool sorted)
{
    FarApartPairs F;

    for (std::size_t i = 0; i < graph.numberOfNodes(); ++i) {
        NodeIDs candidates;
        for (std::size_t j = i+1; j < graph.numberOfNodes(); ++j) {
            auto dij = dist(i, j);
            bool found_further_vertex = false;
            for (auto k: graph.neighborsOf(j)) {
                if (dist(i, k) > dij) {
                    found_further_vertex = true;
                    break;
                }
            }

            if (not found_further_vertex) {
                candidates.push_back(j);
            }
        }

        for (auto j: candidates) {
            auto dij = dist(i, j);
            bool found_further_vertex = false;
            for (auto k: graph.neighborsOf(i)) {
                if (dist(j, k) > dij) {
                    found_further_vertex = true;
                    break;
                }
            }

            if (not found_further_vertex) {
                F.emplace_back(i, j, dij);
            }
        }
    }

    if (sorted) {
        // sort in decreasing order of distances
        std::sort(F.rbegin(), F.rend());
    }
    return F;
}


/*
 * This method takes as input the distance matrix of the graph and returns the list
 * of far apart pairs of vertices.
 * A pair (i, j) apears only once.
 * When sorted is true, the list is sorted by decreasing distances.
 * This version avoids a call to "sort" using buckets
 */
FarApartPairs getFarApartPairs_v2(Graph const& graph, DistanceMatrix const& dist, bool sorted)
{
    if (not sorted) {
        return getFarApartPairs(graph, dist, false);
    }
    
    std::size_t n = graph.numberOfNodes();
    std::vector<NodePairs> Q(n);
    
    for (std::size_t i = 0; i < n; ++i) {
        NodeIDs candidates;
        for (std::size_t j = i+1; j < n; ++j) {
            auto dij = dist(i, j);
            bool found_further_vertex = false;
            for (auto k: graph.neighborsOf(j)) {
                if (dist(i, k) > dij) {
                    found_further_vertex = true;
                    break;
                }
            }

            if (not found_further_vertex) {
                candidates.push_back(j);
            }
        }

        for (auto j: candidates) {
            auto dij = dist(i, j);
            bool found_further_vertex = false;
            for (auto k: graph.neighborsOf(i)) {
                if (dist(j, k) > dij) {
                    found_further_vertex = true;
                    break;
                }
            }

            if (not found_further_vertex) {
                Q[dij].emplace_back(i, j);
            }
        }
    }

    FarApartPairs F;
    for (auto dij = n - 1; dij > 0; dij--)
        for (auto [i, j] : Q[dij])
            F.emplace_back(i, j, dij);

    return F;
}




} // end namespace alg
