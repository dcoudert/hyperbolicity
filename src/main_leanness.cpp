/*
 leanness
 Copyright (C) 2023  COUDERT David <david.coudert@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "defs.h"
#include "graph.h"
#include "algorithms.h"
#include "leanness.hpp"
#include "io.h"
#include "measurement_tool.h"
#include "my_timer.h"

#include <cstring>
// to use shuffle
#include <algorithm>
#include <random>


class ParserInput {
public:
    std::string filename;
    int alg_version = 2;
    std::size_t capacity = 20;
    std::size_t t = 10;
    std::size_t k = 10;
    uint interval_version = 3;
    uint slice_version = 0;
    std::size_t threshold = 30;
    uint hub_version = 1;
    bool experiment_diameter = false;

    explicit ParserInput(int argc, char* argv[])
    {
        if (argc < 2)
        {
            printUsage();
            ERROR("You have to specify a graph filename.");
        }
        if (strcmp(argv[1], "-h") == 0) {
            printUsage();
            std::exit(0);
        }

        filename = argv[1];

        for (int i = 2; i < argc; ++i)
        {
            if (strcmp(argv[i], "-a") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-a\"."); }
                alg_version = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-c") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-c\"."); }
                capacity = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-i") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-i\"."); }
                interval_version = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-s") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-s\"."); }
                slice_version = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-th") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-th\"."); }
                threshold = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-t") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-t\"."); }
                t = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-k") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-k\"."); }
                k = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-hub") == 0)
            {
                if (i+1 >= argc) { ERROR("Missing parameter for \"-hub\"."); }
                hub_version = std::stoi(argv[++i]);
            }
            else if (strcmp(argv[i], "-h") == 0)
            {
                printUsage();
                std::exit(0);
            }
            else if ((strcmp(argv[i], "-d") == 0) or (strcmp(argv[i], "-diameter") == 0))
            {
                experiment_diameter = true;
            }
            else {
                ERROR("Unknown option " << argv[i]);
            }
        }
    }

    void printUsage() {
        std::cout << "USAGE: leanness <graph_filename> [-a <algorithm_version>] [-i <interval_version>] [-s <slice_version>] [-th <threshold>] [-hub <hub_labeling_version>] [-c <cache_size>] [-t <number_of_trials>] [-k <considered_vertices>] [-d|-diameter]\n"
            "\n"
            "The <algorithm_version> should be the number:\n"
            "1) exact algorithm using distance matrix\n"
            "2) exact algorithm proposed based on the enumeration of far-apart pairs and the use of hub labeling to store distances (default)\n"
            "3) heuristic algorithm using distance matrix\n"
            "\n"
            "Parameters for exact algorithms 1 and 2:\n"
            "   -i <interval_version>: specify the method to explore an interval.\n"
            "              1: consider all slices of the interval expect those at distance less than lb/2 from ends\n"
            "              2: use dichotomy to prune ranges of slices\n"
            "              3: use improved dichotomy to prune ranges of slices (default)\n"
            "              Used only wen algorithm is 1.\n"
            "   -s <slice_version>: specify the algorithm to find the largest distance in a slice.\n"
            "              1: consider all pairs\n"
            "              2: use iFUB to find the diameter\n"
            "              3: use DHV to find the diameter\n"
            "              0: auto (default), use 1 if the slice has size less than threshold, and 2 otherwise\n"
            "   -th <threshold>: specify the threshold used when parameter -s is 0 (default: 10)\n"
            "\n"
            "Extra parameters for exact algorithm 2:\n"
            "   -c <cache_size>: allow to specify the size of the cache of BFSs (at least 2).\n"
            "              Used only when algorithm is 2.\n"
            "   -hub <hub_labeling_version>: specify the hub labeling implementation\n"
            "              1: .... (default)\n"
            "              2: .... \n"
            "\n"
            "Parameters for heuristic algorithm 3:\n"
            "   -t <number_of_trials>: number of trials for the heuristic\n"
            "   -k <considered_vertices>: number of considered vertices per trial of the heuristic\n"
            "\n"
            "Special parameter for experiments on diameter\n"
            "   -d, -diameter: run experiments on diameter on specified graph file\n"
            "\n"
            "   -h: print this help\n"
            << std::endl;
    }
};


void run_experiment_on_diameter(ParserInput & PI, int max_run=1000)
{
    try {
        // Read the graph
        auto parser_edges = io::readGraph(PI.filename);
        Graph g;
        g.init(parser_edges);
        
        // Initialize distance matrix and hub labeling
        Leanness LM(g);
        LM.init_experiment_diameter();
        LeannessLowMemory LH(g);
        LH.init_experiment_diameter();

        NodeIDs V(g.numberOfNodes());
        std::iota(V.begin(), V.end(), 0);

        // Initialize random engine
        auto rd = std::random_device {};
        auto rng = std::default_random_engine { rd() };

        using duration_type = std::chrono::duration<int, std::nano>;

        std::cout << "k\tLM_basic\tLM_IFUB\tM_DHV\tLH_basic\tLH_IFUB\tH_DHV" << std::endl;

        // Run experiment for slices of size 5, 10,..., 100
        for (int size=5; size <= 500; size += 5) {
            Slice my_slice(size);
            
            std::chrono::high_resolution_clock::time_point start_time, end_time;
            
            for (int t=0; t < max_run; t++) {
                // Choose a random slice
                std::shuffle(V.begin(), V.end(), rng);
                for (int i=0; i < size; i++) {
                    my_slice[i] = V[i];
                }

                std::cout << size << "\t" << t << "\t";

                start_time = std::chrono::high_resolution_clock::now();
                auto D1 = LM.slice_diameter_basic(my_slice);
                end_time = std::chrono::high_resolution_clock::now();
                std::cout << std::chrono::duration_cast<duration_type>(end_time - start_time).count() << "\t";

                start_time = std::chrono::high_resolution_clock::now();
                auto D2 = LM.slice_diameter_IFUB(my_slice);
                end_time = std::chrono::high_resolution_clock::now();
                std::cout << std::chrono::duration_cast<duration_type>(end_time - start_time).count() << "\t";

                start_time = std::chrono::high_resolution_clock::now();
                auto D3 = LM.slice_diameter_DHV(my_slice);
                end_time = std::chrono::high_resolution_clock::now();
                std::cout << std::chrono::duration_cast<duration_type>(end_time - start_time).count() << "\t";

                start_time = std::chrono::high_resolution_clock::now();
                auto D4 = LH.slice_diameter_basic(my_slice);
                end_time = std::chrono::high_resolution_clock::now();
                std::cout << std::chrono::duration_cast<duration_type>(end_time - start_time).count() << "\t";

                start_time = std::chrono::high_resolution_clock::now();
                auto D5 = LH.slice_diameter_IFUB(my_slice);
                end_time = std::chrono::high_resolution_clock::now();
                std::cout << std::chrono::duration_cast<duration_type>(end_time - start_time).count() << "\t";;

                start_time = std::chrono::high_resolution_clock::now();
                auto D6 = LH.slice_diameter_DHV(my_slice);
                end_time = std::chrono::high_resolution_clock::now();
                std::cout << std::chrono::duration_cast<duration_type>(end_time - start_time).count() << std::endl;

                if ((D1 != D2) or (D2 != D3) or (D3 != D4) or (D4 != D5) or (D5 != D6)) {
                    std::cerr << "Wrong result: " << D1 << " " << D2 << " " << D3 << " " << D4 << " " << D5 << " " << D6 << "\n";
                }
            }
        }

    }
    catch(const std::exception& e) {
        PI.printUsage();
        ERROR(e.what());
    }
}


int main(int argc, char* argv[])
{
    ParserInput PI = ParserInput(argc, argv);
    
    if (PI.experiment_diameter) {
        run_experiment_on_diameter(PI);
        exit(0);
    }

    try {
        auto parser_edges = io::readGraph(PI.filename);
        Graph g;
        g.init(parser_edges);

        std::cout << "Graph: " << g << std::endl;
        std::cout << "BFS cache capacity: " << PI.capacity << std::endl;
        std::cout << "BFSInfo type: " << BFS_INFO_TYPE << std::endl;
        std::cout << "Algorithm version: " << PI.alg_version << std::endl;

        Leanness LM(g);
        LeannessLowMemory LH(g);
        Distance value;

        switch(PI.alg_version) {
            case 1:
                value = LM.compute(PI.interval_version, PI.slice_version, PI.threshold);
                break;
            case 2:
                value = LH.compute(PI.capacity, PI.interval_version, PI.slice_version, PI.threshold, PI.hub_version);
                break;
            case 3:
                value = LM.compute_heuristic(PI.t, PI.k);
                break;
            case 4:
                value = LM.compute_fap_iter(PI.interval_version, PI.slice_version, PI.threshold);
                break;
            default:
                PI.printUsage();
                ERROR("Unknown algorithm version: " << PI.alg_version);
        }

        MEASUREMENT::print();

        std::cout << std::endl << "The leanness of the graph is " << value;
        std::cout << std::endl;
    }
    catch(const std::exception& e) {
        PI.printUsage();
        ERROR(e.what());
    }
}
