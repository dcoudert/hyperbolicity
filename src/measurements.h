/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

// To add a new measurement, add a new enum value to MEASUREMENT::EXP. This will
// be the handle to the measurement. Then you also have to add a tuple of the
// form (<name>, <handle>, <type>). The values mean the following:
//
// <name>: This is just a string that will be used when printing.
//
// <handle>: The handle which is used to reference the measurment in the code. This
//           has to be the same that was also added to the enum.
//
// <type>: There are serveral types of measurements.
//     * TIMER: Measures time using a start and stop function. Afterwards the sum
//              and mean can be calculated.
//     * INT: Accumulating int values. The sum and mean can be recalled from that.
//     * FLOAT: Same as INT just for float values.
//     * TIMER_DATA: Like TIMER but all the measurements are collected and thus
//                   more complex stats can be created like standard deviation.
//     * INT_DATA: Like TIMER_DATA but for INT.
//     * FLOAT_DATA: Like TIMER_DATA but for FLOAT.
//     * COUNTER: A simple counter which can just be increased.


// Add new measurements here ...
enum class MEASUREMENT::EXP {
	// DummyTimer1,
	// DummyTimer2,
	// DummyTimer3,
	// DummyTimer4,
	// DummyTimer5,
	// DummyCounter1,
	// DummyCounter2,
	// DummyCounter3,
	// DummyCounter4,
	// DummyCounter5,
	// INNER_LOOP,
	// INNER_LOOP_ALL,
	// SAMPLE_VALUE,
	// SAMPLE_VALUES,
	INIT_V2,
	HAS_NEXT_V2,
	LOOP_INIT_V2,
	ACC_VAL_V2,
	HLB_UPDATE_V2,
	BFS_COUNTER_V2,
    BORASSI_TIMER_FA,
    BORASSI_TIMER_COMPUTE,
	COMPLETE_BORASSI,
    BORASSI_HUBLAB_TIMER_FA,
    BORASSI_HUBLAB_TIMER_COMPUTE,
	COMPLETE_BORASSI_HUBLAB,
	COMPLETE_V2,
};

inline auto MEASUREMENT::getMeasurements() -> Measurements {
// ... and here
	return {
		// {"dummy timer 1", EXP::DummyTimer1, TIMER},
		// {"dummy timer 2", EXP::DummyTimer2, TIMER},
		// {"dummy timer 3", EXP::DummyTimer3, TIMER},
		// {"dummy timer 4", EXP::DummyTimer4, TIMER},
		// {"dummy timer 5", EXP::DummyTimer5, TIMER},
		// {"dummy counter 1", EXP::DummyCounter1, COUNTER},
		// {"dummy counter 2", EXP::DummyCounter2, COUNTER},
		// {"dummy counter 3", EXP::DummyCounter3, COUNTER},
		// {"dummy counter 4", EXP::DummyCounter4, COUNTER},
		// {"dummy counter 5", EXP::DummyCounter5, COUNTER},
		// {"inner loop timer", EXP::INNER_LOOP, TIMER},
		// {"inner loop all", EXP::INNER_LOOP_ALL, TIMER_DATA},
		// {"sample value", EXP::SAMPLE_VALUE, INT},
		// {"sample values", EXP::SAMPLE_VALUES, INT_DATA},
        {"Time for computation of far apart pairs in computeBorassi", EXP::BORASSI_TIMER_FA, TIMER},
        {"Time for computation of hyperbolicity in computeBorassi", EXP::BORASSI_TIMER_COMPUTE, TIMER},
        {"Time for computation of far apart pairs in computeBorassiHubLabeling", EXP::BORASSI_HUBLAB_TIMER_FA, TIMER},
        {"Time for computation of hyperbolicity in computeBorassiHubLabeling", EXP::BORASSI_HUBLAB_TIMER_COMPUTE, TIMER},
		{"Initialization of v2 algorithm", EXP::INIT_V2, TIMER},
		{"HasNext call of v2 loop to get next far pair", EXP::HAS_NEXT_V2, TIMER},
		{"Initial part of v2 main loop", EXP::LOOP_INIT_V2, TIMER},
		{"Time for acceptable and valuable computation in v2 main loop", EXP::ACC_VAL_V2, TIMER},
		{"Lower bound update of v2 main loop", EXP::HLB_UPDATE_V2, TIMER},
		{"Counts BFS calls in compute_v2", EXP::BFS_COUNTER_V2, COUNTER},
        {"COMPLETE TIME FOR BORASSI_HUBLAB: ", EXP::COMPLETE_BORASSI_HUBLAB, TIMER},
        {"COMPLETE TIME FOR BORASSI: ", EXP::COMPLETE_BORASSI, TIMER},
        {"COMPLETE TIME FOR OUR APPROACH: ", EXP::COMPLETE_V2, TIMER},
	};
}
