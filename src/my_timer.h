//
//  my_timer.h
//
//
//  Created by David Coudert on 18/03/2023.
//

#pragma once

class MyTimer
{
public:
    std::chrono::high_resolution_clock::time_point start_time;
    std::chrono::high_resolution_clock::time_point last_time;
    MyTimer() { reset(); };
    void reset() { start_time = std::chrono::high_resolution_clock::now(); last_time = start_time; };
    friend std::ostream& operator<<(std::ostream&, MyTimer&);
};
