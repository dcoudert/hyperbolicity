/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "basic_types.h"

#include <vector>

// struct for type 3
struct DistanceCount {
    Distance dist;
    uint count;
    DistanceCount() : dist(DIST_INF), count(0) {}
};

// Structure for recording distance information coming from a BFS
class BFSInfo {
private:
	Distances distances;
#if BFS_INFO_TYPE == 2
	uint bfs_count;// incremented each time a bfs is performed with this data
	std::vector<uint> visitCount; // visited nodes have value equal to bfs_count
#endif
#if BFS_INFO_TYPE == 3
	struct DistanceCount distInfDistCount;
	uint bfs_count;// incremented each time a bfs is performed with this data
	std::vector<DistanceCount> distCount; // visited nodes have count equal to bfs_count
#endif

public:
	std::vector<NodeID> visitSet;

	//
	// basic functions
	//

#if BFS_INFO_TYPE == 0 || BFS_INFO_TYPE == 1
	BFSInfo()
	{
	}

	void visit(NodeID i, Distance d) {
		distances[i] = d;
		visitSet.push_back(i);
	}

	bool visited(NodeID i) const {
		return distances[i] != DIST_INF;
	}

	Distance distance(NodeID i) const {
		return distances[i];
	}
#endif

#if BFS_INFO_TYPE == 0 || BFS_INFO_TYPE == 1 || BFS_INFO_TYPE == 2
	Distance unsafe_distance(NodeID i) const {
		return distances[i];
	}

	std::size_t size() const {
		return distances.size();
	}
#endif

	//
	// prepare
	//

#if BFS_INFO_TYPE == 0
	void prepare(uint nb_nodes) {
		if (distances.size() != nb_nodes) {
			distances.assign(nb_nodes, DIST_INF);
			visitSet.reserve(nb_nodes);
		} else {
			std::fill(distances.begin(), distances.end(), DIST_INF);
		}
		visitSet.clear();
	}
#endif

#if BFS_INFO_TYPE == 1
	void prepare(uint nb_nodes) {
		if (distances.size() < nb_nodes) {
			distances.assign(nb_nodes, DIST_INF);
			visitSet.reserve(nb_nodes);
		} else {
			for (NodeID i : visitSet) {
				distances[i] = DIST_INF;
			}
		}
		visitSet.clear();
	}
#endif

#if BFS_INFO_TYPE == 2
	void prepare(uint nb_nodes) {
		if (distances.size() < nb_nodes
			|| bfs_count == std::numeric_limits<uint>::max()) {
			bfs_count = 0;
			distances.assign(nb_nodes, DIST_INF);
			visitCount.assign(nb_nodes, 0);
			visitSet.reserve(nb_nodes);
		}
		visitSet.clear();
		++bfs_count;
	}
#endif

#if BFS_INFO_TYPE == 2
	BFSInfo() : bfs_count(0) {}

	void visit(NodeID i, Distance d) {
		distances[i] = d;
		visitCount[i] = bfs_count;
		visitSet.push_back(i);
	}

	bool visited(NodeID i) const {
		return visitCount[i] == bfs_count;
	}

	Distance distance(NodeID i) const {
		return visited(i) ? distances[i] : DIST_INF;
	}
#endif

#if BFS_INFO_TYPE == 3
	BFSInfo() : bfs_count(0) {}

	void prepare(uint nb_nodes) {
		if (distCount.size() < nb_nodes
			|| bfs_count == std::numeric_limits<uint>::max()) {
			bfs_count = 0;
			distCount.assign(nb_nodes, distInfDistCount);
			visitSet.reserve(nb_nodes);
		}
		visitSet.clear();
		++bfs_count;
	}

	void visit(NodeID i, Distance d) {
		distCount[i].dist = d;
		distCount[i].count = bfs_count;
		visitSet.push_back(i);
	}

	bool visited(NodeID i) const {
		return distCount[i].count == bfs_count;
	}

	Distance distance(NodeID i) const {
		return visited(i) ? distCount[i].dist : DIST_INF;
	}
		
	Distance unsafe_distance(NodeID i) const {
		return distCount[i].dist;
	}

	std::size_t size() const {
		return distCount.size();
	}
#endif
};

using BFSCache = Cache<NodeID, BFSInfo>;
