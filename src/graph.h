/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "defs.h"
#include "basic_types.h"

#include <vector>

namespace unit_tests
{
	void testGraph();
}

class Graph
{
private:
	uint number_of_nodes;
	uint number_of_edges;

	std::vector<uint> offsets;
	NodeIDs neighbors;

	void initOffsets(ParserData& edges);

public:
	Graph() = default;

	void init(ParserData data);
	void printInfo() const;

	uint numberOfNodes() const { return number_of_nodes; }
	uint numberOfEdges() const { return number_of_edges; }
	uint degree(NodeID node_id) const;

    // To iterate over all vertices, e.g., for (auto v: graph.vertices()) {...}
    NodeIDRange vertices() const;

	ConstNeighbors neighborsOf(NodeID node_id) const;

    // make the output function a friend so it can access its private data members
    // The goal is to be able to write: cout << my_node << endl;
    friend std::ostream& operator<<(std::ostream&, Graph&);

	friend void unit_tests::testGraph();
};
