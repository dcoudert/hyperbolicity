/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#define MEASURE
#include <iostream>

// helper macro

#define GRAPH_HYPERBOLICITY_NOP do { } while (0)

// printing macros

#ifdef NDEBUG
#define DEBUG(x) GRAPH_HYPERBOLICITY_NOP
#else
#define DEBUG(x) do { std::cout << x << std::endl; } while (0)
#endif

#if defined(NVERBOSE)
#define PRINT(x) GRAPH_HYPERBOLICITY_NOP
#else
#define PRINT(x) do { std::cout << x << std::endl; } while (0)
#endif

#define ERROR(x) do { std::cerr << "Error: " << x << std::endl;\
                      std::exit(EXIT_FAILURE); } while (0)

// Hard enforce of asserts
// This is bad style, but at least asserts always work.
#undef NDEBUG
#include <cassert>
