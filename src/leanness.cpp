/*
 leanness.cpp
 Copyright (C) 2023  COUDERT David <david.coudert@inria.fr>
                     COULOMB Samuel <samuel.coulomb@ens.psl.eu>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "leanness.hpp"
#include "algorithms.h"
#include "measurement_tool.h"
#include "my_timer.h"

#include <random>
#include <queue>  // TODO: supprimer

// TODO:
// 1) distinguer Mohamed et al. de notre algo => 2 méthodes
// 2) vérifier que les calculs de Mohammed et al. sont bien comme indiquer dans l'article (calcul interval)
// 3) faire une méthodes avec pré-calcul des FAP et une autre avec itérateur
// 4) utiliser la méthode de Samuel pour les tranches de slices
//    - vector<vector<Tranches>> de longueur au plus diam(G)
//    - initialiser avec première tranche (tout) dans la case d(x,y) (borne sup diam tranches)
//    - retirer une tranche de borne sur d (initialement d(x,y)), vérifier bornes
//      Si on peut découper on le fait et on range aux bons endroits
//    - quand il n'y a plus de tranches pour borne d, on va a d-1
// 5) limiter les temps de calculs à 3600 secondes. Ceux qui demandent plus ne terminent pas (trop de mémoire)


// Return a slice with vertices sorted by increasing distance
// We assume that dc[i] is the distance associated to node s[i]
inline Slice sort_slice(Slice const& s, Distances const& dc)
{
    std::size_t n = s.size();
    std::vector<std::size_t> index(n);
    std::iota(index.begin(), index.end(), 0);
    std::sort(index.begin(), index.end(),
              [&,dc](const NodeID u, const NodeID v){ return dc[u] < dc[v]; });
    Slice sorted_slice(n) ;
    for (std::size_t i = 0; i < n; i++)
        sorted_slice[i] = s[index[i]];
    return sorted_slice;
}



/*
 Class Leanness
 Implement the exact and the heuristic algorithms proposed
 by A. O. Mohammed, F. F. Dragan, and H. M. Guarnera.
 */

Leanness::Leanness(Graph const& g) : graph(g)
{
}

/*
 Special initialization for running experiments on diameter methods.
*/
void Leanness::init_experiment_diameter(int version_slice, int threshold)
{
    std::size_t n = graph.numberOfNodes();
    _version_slice = version_slice;
    _threshold = threshold;
    evaluated_slices = 0;
    sum_slices_size = 0;
    min_slices_size = n;
    max_slices_size = 0;

    mytimer.reset();
    std::cout << "Compute distance matrix... " << std::flush;
    distance = alg::computeDistanceMatrix(graph);
    std::cout << "done\t" << mytimer << std::endl;
}


/*
 Compute the exact leanness of the graph using the algorithm proposed by Mohammed et al.
 */
Distance Leanness::compute_MDG()
{
    std::size_t n = graph.numberOfNodes();
    _version_interval = 1;
    _version_slice = 1;
    _threshold = graph.numberOfNodes();
    std::cout << "Interval version: " << _version_interval << std::endl;
    std::cout << "Slice version: " << _version_slice << std::endl;
    std::cout << "Threshold: " << _threshold << std::endl;

    mytimer.reset();
    considered_fap = 0;
    evaluated_intervals = 0;
    evaluated_slices = 0;
    sum_slices_size = 0;
    min_slices_size = n;
    max_slices_size = 0;

    std::cout << "Compute distance matrix... " << std::flush;
    distance = alg::computeDistanceMatrix(graph);
    std::cout << "done\t" << mytimer << std::endl;

    // Build list Q of pairs sorted by non-increasing distance.
    // To avoid sorting, we use buckets to arrange pairs by distances
    std::cout << "Build list of active pairs sorted by distances... " << std::flush;
    std::vector<NodePairs> Q(n);
    Matrix<char> active_pairs(n ,n);  // to identify active pairs
    for (auto y=1; y < n; y++) {
        for (auto x=0; x < y; x++) {
            Q[distance(x, y)].emplace_back(x, y);
            active_pairs(x, y) = true;
        }
    }
    std::cout << "done\t" << mytimer << std::endl;

    // Main loop
    std::cout << "Start search... " << std::endl;
    leanness_lb = 0;
    Distance leanness_ub = n;
    for (auto dxy = n - 1; dxy > leanness_lb; dxy--) {
        if ((Q[dxy].size() > 0) and (dxy < leanness_ub)) {
            leanness_ub = dxy;
            std::cout << "new upper bound: " << leanness_ub << "\t" << mytimer;
            std::cout << "\t(" << considered_fap << ", " << evaluated_intervals << ", " << evaluated_slices << ")" << std::endl;
        }
        for (auto [x, y]: Q[dxy]) {
            if (active_pairs(x, y)) {
                // Pair (x, y) has not been considered or discared yet.
                // We build the interval I(x, y) and find the diameter (i.e., maximum distance)
                // of its slices
                considered_fap++;
                build_interval_and_update_leanness_MDG(x, y, dxy, active_pairs);
            }
        } // goto next pair
    } // goto to next distance
    std::cout << "end of search\t" << mytimer << std::endl;

    std::cout << "Considered FAP:\t" << considered_fap << std::endl;
    std::cout << "Evaluated Intervals:\t" << evaluated_intervals << std::endl;
    std::cout << "Evaluated slices:\t" << evaluated_slices << std::endl;
    std::cout << "Min / Avg / Max size of slices:\t" << min_slices_size;
    std::cout << " / " << (1.0*sum_slices_size)/evaluated_slices;
    std::cout << " / " << max_slices_size << std::endl;

    // Finally return the best found value
    return leanness_lb;
}



/*
 Build interval I(x, y) and compute the maximum distance in the slices
 lambda/2... dist(x, y) - lambda/2. The maximum distance in a slice is
 computed as the maximum overall pairs.
 */
void Leanness::build_interval_and_update_leanness_MDG(NodeID x, NodeID y, Distance dxy, Matrix<char> & active_pairs)
{
    // Build the interval I(x, y)
    Interval I(dxy + 1);
    for (auto w: graph.vertices()) {
        if ((x == w) or (y == w)) { continue; }
        auto dxw = distance(x, w);
        if (dxy == dxw + distance(y, w)) {
            // insert w into the correct slice of the interval
            I[dxw].push_back(w);
            // remove pairs (x, w) and (y, w) from Q, i.e., remove from active_pairs.
            // In other words, these pairs are not far-apart
            active_pairs(x, w) = false;
            active_pairs(w, x) = false;
            active_pairs(y, w) = false;
            active_pairs(w, y) = false;
        }
    }

    // Compute the maximum leanness of the interval
    update_leanness_MDG(I);
}


/*
 Check if interval I(x, y) helps improving the leanness.
 Consider all slices, except slices too close to x and y.
 */
inline void Leanness::update_leanness_MDG(Interval const& I)
{
    evaluated_intervals++;
    // Compute the maximum leanness of the interval
    std::size_t start = leanness_lb / 2;
    std::size_t end = I.size() - 1 - start;
    for (std::size_t i=start + 1; i < end; i++) {
        Distance best = slice_diameter(I[i]);
        if (best > leanness_lb) {
            leanness_lb = best;
            std::cout << "new lower bound: " << leanness_lb << "\t" << mytimer;
            std::cout << "\t(" << considered_fap << ", " << evaluated_intervals << ", " << evaluated_slices << ")" << std::endl;
        }
    }
}






/*
 Compute the exact leanness of the graph.
 Far-apart pairs are pre-computed
 */
Distance Leanness::compute(int version_interval, int version_slice, int threshold)
{
    if (version_interval == 1) {
        // This is a call to MDG
        return compute_MDG();
    }
    std::size_t n = graph.numberOfNodes();
    _version_interval = version_interval;
    _version_slice = version_slice;
    _threshold = threshold;
    std::cout << "Interval version: " << version_interval << std::endl;
    std::cout << "Slice version: " << version_slice << std::endl;
    std::cout << "Threshold: " << threshold << std::endl;

    mytimer.reset();
    considered_fap = 0;
    evaluated_intervals = 0;
    evaluated_slices = 0;
    sum_slices_size = 0;
    min_slices_size = n;
    max_slices_size = 0;

    std::cout << "Compute distance matrix... " << std::flush;
    distance = alg::computeDistanceMatrix(graph);
    std::cout << "done\t" << mytimer << std::endl;

    // Build list Q of pairs sorted by non-increasing distance.
    // To avoid sorting, we use buckets to arrange pairs by distances
    std::cout << "Build list of active pairs sorted by distances... " << std::flush;
    auto Q = alg::getFarApartPairs_v2(graph, distance, true);
    std::cout << "done\t" << mytimer << std::endl;

    // Main loop
    std::cout << "Start search... " << std::endl;
    leanness_lb = 0;
    Distance leanness_ub = n;
    for (auto [x, y, dxy]: Q) {
        if (dxy <= leanness_lb) {
            // We can not improve further
            break;
        }
        if (dxy < leanness_ub) {
            leanness_ub = dxy;
            std::cout << "new upper bound: " << leanness_ub << "\t" << mytimer;
            std::cout << "\t(" << considered_fap << ", " << evaluated_intervals << ", " << evaluated_slices << ")" << std::endl;
        }
        considered_fap++;
        build_interval_and_update_leanness_prune(x, y, dxy);
    } // goto to next far-apart pair
    std::cout << "end of search\t" << mytimer << std::endl;

    std::cout << "Considered FAP:\t" << considered_fap << std::endl;
    std::cout << "Evaluated Intervals:\t" << evaluated_intervals << std::endl;
    std::cout << "Evaluated slices:\t" << evaluated_slices << std::endl;
    std::cout << "Min / Avg / Max size of slices:\t" << min_slices_size;
    std::cout << " / " << (1.0*sum_slices_size)/evaluated_slices;
    std::cout << " / " << max_slices_size << std::endl;

    // Finally return the best found value
    return leanness_lb;
}


/*
 Compute the exact leanness of the graph.
 Far-apart pairs are computed using the far-apart pairs iterator
 */
Distance Leanness::compute_fap_iter(int version_interval, int version_slice, int threshold, int capacity)
{
    if (version_interval == 1) {
        // This is a call to MDG
        return compute_MDG();
    }
    std::size_t n = graph.numberOfNodes();
    _version_interval = version_interval;
    _version_slice = version_slice;
    _threshold = threshold;
    std::cout << "Interval version: " << version_interval << std::endl;
    std::cout << "Slice version: " << version_slice << std::endl;
    std::cout << "Threshold: " << threshold << std::endl;

    mytimer.reset();
    considered_fap = 0;
    evaluated_intervals = 0;
    evaluated_slices = 0;
    sum_slices_size = 0;
    min_slices_size = n;
    max_slices_size = 0;

    std::cout << "Compute distance matrix... " << std::flush;
    distance = alg::computeDistanceMatrix(graph);
    std::cout << "done\t" << mytimer << std::endl;

    std::cout << "Initialize far apart pairs... " << std::flush;
    // setup iterator of far apart pairs
    assert(capacity >= 2);
    BFSCache bfs_cache(capacity);
    BFSInfo* bfs_x = nullptr;

    auto bfs_cache2 = &bfs_cache;
    FarApartIteratorInterface *FA(0);
    FarApartIterator *_FAI = nullptr;
    _FAI = new FarApartIterator(graph, bfs_cache2, "DHV");
    FA = _FAI;
    std::cout << "done\t" << mytimer << std::endl;

    // Main loop
    std::cout << "Start search... " << std::endl;
    leanness_lb = 0;
    Distance leanness_ub = n;
    while (true) { // it's this way to enable time measurements of hasNext()
        auto const has_next = FA->hasNext();
        if (not has_next) { break; }
        
        auto [x, y, dxy] = FA->getNext();
        if (dxy <= leanness_lb) {
            // We can not improve
            break;
        }
        if (dxy < leanness_ub) {
            leanness_ub = dxy;
            std::cout << "new upper bound: " << leanness_ub << "\t" << mytimer;
            std::cout << "\t(" << considered_fap << ", " << evaluated_intervals << ", " << evaluated_slices << ")" << std::endl;
        }
        considered_fap++;
        build_interval_and_update_leanness_prune(x, y, dxy);
    } // goto to next far-apart pair
    std::cout << "end of search\t" << mytimer << std::endl;

    std::cout << "Considered FAP:\t" << considered_fap << std::endl;
    std::cout << "Evaluated Intervals:\t" << evaluated_intervals << std::endl;
    std::cout << "Evaluated slices:\t" << evaluated_slices << std::endl;
    std::cout << "Min / Avg / Max size of slices:\t" << min_slices_size;
    std::cout << " / " << (1.0*sum_slices_size)/evaluated_slices;
    std::cout << " / " << max_slices_size << std::endl;

    // Finally return the best found value
    return leanness_lb;
}


/*
 Build interval I(x,y) and compute it's leanness.
 During the construction, if a slice has size 1, we end the current interval,
 compute its leanness, and start a new one.
 An interval with less than leanness_lb slices can be pruned.
 */
void Leanness::build_interval_and_update_leanness_prune(NodeID x, NodeID y, Distance dxy)
{
    std::vector<bool> visited(graph.numberOfNodes(), false);
    Distance dx_current = distance(x, y);
    Interval I(dx_current + 1);
    I[0].push_back(y);
    SliceIndex start = 0;          // first slice of current interval
    SliceIndex end = 1;            // last slice of current interval
    SliceIndex current_slice = 0;  // index of current slice

    while (dx_current > 0) {
        // Build next slice
        for (NodeID current: I[current_slice]) {
            for (auto neighbor_id: graph.neighborsOf(current)) {
                if ((not visited[neighbor_id]) and (distance(x, neighbor_id) + 1 == dx_current)) {
                    visited[neighbor_id] = true;
                    I[end].push_back(neighbor_id);
                }
            }
        }
        if (I[end].size() == 1) {
            // We end the current interval and compute its leanness if not pruned
            if ((end - start) > leanness_lb) {
                // We use this interval to update the lower bound on the leanness
                update_leanness_range_v2(I, start, end);
                //if (_version_interval == 2) {
                //    update_leanness_range(I, start, end);
                //} else  { // _version_interval == 3
                //    update_leanness_range_v2(I, start, end);
                //}
            }
            // We start a new interval
            start = end;
        }
        current_slice = end;
        end++;
        dx_current--;
    }
}


/*
 Check if interval I(x, y) helps improving the leanness.
 Ranges of slices are pruned if not promising.
 */
void Leanness::update_leanness_range(Interval const& I, SliceIndex start, SliceIndex end)
{
    evaluated_intervals++;
    Distances diam(end + 1, 0);   // vector recording the largest distance in each slice
    SliceIndexPairs Q;            // stack (or queue or priority queue) of sub-intervals to explore
    Q.push_back({start, end});
    /*
    SliceIndex good_start = start + leanness_lb / 2;
    diam[good_start] = slice_diameter_v1(I[good_start]);
    SliceIndex good_end = end - leanness_lb / 2;
    diam[good_end] = slice_diameter_v1(I[good_end]);
    Q.push_back({good_start, good_end});
    */
    
    while (Q.size() > 0) {
        auto [left, right] = Q.back();
        Q.pop_back();
        
        // Check if this sub-interval is still promizing
        if ((diam[left] + diam[right])/2 + right - left <= leanness_lb) {
            continue;
        }

        // Compute the largest distance in the middle slice
        SliceIndex middle = (left + right) / 2;
        auto d_middle = slice_diameter(I[middle]);
        diam[middle] = d_middle;

        // Check if it improves the lower bound on the leanness
        if (d_middle > leanness_lb) {
            leanness_lb = d_middle;
            std::cout << "new lower bound: " << leanness_lb << "\t" << mytimer;
            std::cout << "\t(" << considered_fap << ", " << evaluated_intervals << ", " << evaluated_slices << ")" << std::endl;
        }

        // Prepare sub-intervals
        auto dist = middle - left;
        if ((dist > 1) and ((diam[left] + d_middle)/2 + dist > leanness_lb)) {
            Q.push_back({left, middle});
        }
        dist = right - middle;
        if ((dist > 1) and ((d_middle + diam[right])/2 + dist > leanness_lb)) {
            Q.push_back({middle, right});
        }
    } // goto next sub-interval
}


/*
 Check if interval I(x, y) helps improving the leanness.
 Ranges of slices are pruned if not promising.
 
 WARNING: We assume that the starting and ending slices have a single vertex
 */
void Leanness::update_leanness_range_v2(Interval const& I, SliceIndex start, SliceIndex end)
{
    evaluated_intervals++;
    Distances diam(end + 1, 0);   // vector recording the largest distance in each slice
    Distance d = end - start;
    std::vector<std::vector<SliceIndexQElem>> Q(d + 1);
    //SliceIndexPriorityQueue Q;
    //Q.push({start, end, d, d});
    Q[d].emplace_back(start, end, d, d);

    while (d > leanness_lb) {
        while (not Q[d].empty()) {
            auto [left, right, length, upper_bound] = Q[d].back();
            Q[d].pop_back();

            // Check if this sub-interval is still promizing
            // i.e., if upper_bound = (diam[left] + diam[right])/2 + right - left <= leanness_lb
            if (upper_bound <= leanness_lb) {
                continue;
            }

            // Find the most promizing slice in the interval
            SliceIndex middle = (diam[right] - diam[left] + 2 * (left + right)) / 4; // TODO: round...
            Distance d_middle;
            if (middle == left) {
                middle++;
            } else if (middle == right) {
                middle--;
            }
            // Compute the largest distance in the middle slice
            d_middle = slice_diameter(I[middle]);
            diam[middle] = d_middle;

            // Check if it improves the lower bound on the leanness
            if (d_middle > leanness_lb) {
                leanness_lb = d_middle;
                std::cout << "new lower bound: " << leanness_lb << "\t" << mytimer;
                std::cout << "\t(" << considered_fap << ", " << evaluated_intervals << ", " << evaluated_slices << ")" << std::endl;
            }

            // Prepare sub-intervals
            Distance dist = middle - left;
            if (dist > 1) {
                Distance ub = (diam[left] + d_middle)/2 + dist;
                if (ub > leanness_lb) {
                    Q[ub].emplace_back(left, middle, dist, ub);
                }
            }
            dist = right - middle;
            if (dist > 1) {
                Distance ub = (d_middle + diam[right])/2 + dist;
                if (ub > leanness_lb) {
                    Q[ub].emplace_back(middle, right, dist, ub);
                }
            }
        }  // go to next sub interval with same upper bound
        d--;
    }  // go to sub intervals with a lower upper bound
}



// Compute the diameter of a slice using the right algorithm
inline Distance Leanness::slice_diameter(Slice const& s)
{
    evaluated_slices++;
    std::size_t slice_size = s.size();
    sum_slices_size += slice_size;
    if (slice_size < min_slices_size) {
        min_slices_size = slice_size;
    } else if (slice_size > max_slices_size) {
        max_slices_size = slice_size;
    }

    switch (_version_slice) {
        case 1: return slice_diameter_basic(s);
        case 2: return slice_diameter_IFUB(s);
        case 3: return slice_diameter_DHV(s);
        default:
            if (s.size() < _threshold) {
                return slice_diameter_basic(s);
            } else {
                return slice_diameter_IFUB(s);
            }
    }
}


// Return the maximum graph distance between the vertices in the slice s
Distance Leanness::slice_diameter_basic(Slice const& s)
{
    if (s.size() < 2) {
        return 0;
    }
    Distance d = 0;
    for (std::size_t y=1; y < s.size(); y++) {
        for (std::size_t x=0; x < y; x++) {
            Distance dxy = distance(s[x], s[y]);
            if (d < dxy) {
                d = dxy;
            }
        }
    }
    return d;
}


// Compute the diameter of a slice using IFUB on the complete graph
// of the vertices of the slice
Distance Leanness::slice_diameter_IFUB(Slice const& s)
{
    // Sort vertices by increasing distance to s[0]
    std::size_t n = s.size();
    NodeID c = s[0];
    Distances dc(n);
    for (std::size_t i = 0; i < n; i++) {
        dc[i] = distance(c, s[i]);
    }
    auto sorted_slice = sort_slice(s, dc);

    // Find the largest distance
    Distance lb = distance(c, sorted_slice.back());
    for (std::size_t i = n - 1; i > 0; i--) {
        NodeID u = sorted_slice[i];
        if (2*distance(c, u) <= lb) { break; }
        for (std::size_t j = i - 1; j > 0; j--) {
            Distance d = distance(u, sorted_slice[j]);
            if (d > lb) {
                lb = d;
            }
        }
    }
    return lb;
}


// Compute the diameter of a slice using DHV on the complete graph
// of the vertices of the slice
Distance Leanness::slice_diameter_DHV(Slice const& s)
{
    if (s.size() < 1) { return 0; }
    if (s.size() == 2) { return distance(s[0], s[1]); }

    Distances eccs_ub(graph.numberOfNodes(), DIST_INF); // TODO : 
    Distances eccs_lb(graph.numberOfNodes(), 0);

    auto my_compare_ub = [&](const NodeID & v1, const NodeID & v2) {
        return eccs_ub[v1] < eccs_ub[v2];
    };
    auto my_compare_lb = [&](const NodeID & v1, const NodeID & v2) {
        return eccs_lb[v1] < eccs_lb[v2];
    };

    // add all nodes to queue
    NodeIDs todo;
    for (auto u: s) {
        todo.push_back(u);
    }

    Distance LB = 0;
    Distance UB = 0;
    Distances dist_x;  // used as cache to reduce the number of calls to distance
    dist_x.resize(graph.numberOfNodes());

    do {
        // 1. Select u such that eccs_ub[u] is maximal
        auto i = std::max_element(todo.begin(), todo.end(), my_compare_ub) - todo.begin();
        auto u = todo[i];
        todo[i] = todo.back();
        todo.pop_back();
        // Compute exact eccentricity of u
        Distance ecc_u = eccs_lb[u];
        for (auto v: todo) {
            auto duv = distance(u, v);
            if (duv > ecc_u) {
                ecc_u = duv;
            }
            // update eccentricity lower bounds
            eccs_lb[v] = std::max(eccs_lb[v], duv);
        }
        // update diameter lower bound
        if (ecc_u > LB)
            LB = ecc_u;

        // 2. Select x such that dist(u, x) + e(x) = e(u) -- This is minES
        // Since we don't know e(x), we select x minimizing e_L(x) and such that d(u, x) + e_L(x) <= e(u)
        // We then compute e(x)
        // If e(x) = e_L(x), we are done. Otherwise, we update eccentricity bounds and repeat
        while (not todo.empty()) {
            // select the element with minimum eccentricity lower bound
            auto j = std::min_element(todo.begin(), todo.end(), my_compare_lb) - todo.begin();
            auto x = todo[j];
            todo[j] = todo.back();
            todo.pop_back();
            // Compute exact eccentricity of x
            Distance ecc_x = eccs_lb[x];
            NodeID antipode;
            for (auto v: todo) {
                Distance dxv = distance(x, v);
                dist_x[v] = dxv;
                if (dxv > ecc_x) {
                    ecc_x = dxv;
                    antipode = v;
                }
            }
            // Update eccentricity bounds
            LB = std::max(LB, ecc_x);
            if (eccs_lb[x] == ecc_x) {
                // We found the good vertex x !
                // We update eccentricity upper bounds and break
                UB = ecc_x;
                for (auto v: todo) {
                    // dist_x[v] is the distance from x to v
                    eccs_ub[v] = std::min(eccs_ub[v], dist_x[v] + ecc_x);
                    if (UB < eccs_ub[v])
                        UB = eccs_ub[v];
                }
                break;
            }
            else {
                // x was not a good choice
                // We use its antipode to update lower bounds
                for (std::size_t l = 0; l < todo.size(); l++)
                    if (todo[l] == antipode) {
                        todo[l] = todo.back();
                        todo.pop_back();
                        break;
                    }
                Distance ecc_a = eccs_lb[antipode];
                for (auto v: todo) {
                    Distance dav = distance(antipode, v);
                    if (dav > ecc_a) {
                        ecc_a = dav;
                    }
                    if (eccs_lb[v] < dav) {
                        eccs_lb[v] = dav;
                    }
                }
                LB = std::max(LB, ecc_a);
            }
        }

        // 3. update eccentricity upper bounds
        // already done above in 1. and 2.

    } while ((LB < UB) and (todo.size() > 1));

    return LB;
}







/*
 Return a lower bound on the leanness of the graph computed using
 the algorithm proposed in:
   Abdulhakeem Othman Mohammed, Feodor F. Dragan, Heather M. Guarnera.
   Fellow Travelers Phenomenon Present in Real-World Networks.
   COMPLEX NETWORKS 2021: 194-206
 */
Distance Leanness::compute_heuristic(int t, int k)
{
    mytimer.reset();
    Distance leanness_lb = 0;
    std::cout << "Start search... " << std::endl;

    // for random vertex selection
    std::default_random_engine dre(std::chrono::system_clock::now().time_since_epoch().count());
    std::uniform_int_distribution<> uid(0, graph.numberOfNodes()-1);

    BFSInfo bfs_z;
    BFSInfo bfs_x;
    BFSInfo bfs_y;
    BFSInfo bfs_u;

    for (int i=0; i < t; i++) {
        // Choose an arbitrary vertex z
        NodeID z = uid(dre); // get random vertex

        // Let x be the last vertex visited by a BFS from z
        NodeID x;
        alg::runBFS(bfs_z, graph, z, x);  // BFS from z
        // Let y be the last vertex visited by a BFS from x
        NodeID y;
        alg::runBFS(bfs_x, graph, x, y);  // BFS from x
        // Compute distances from y
        alg::runBFS(bfs_y, graph, y);  // BFS from y

        // We build the interval I(x, y)
        Distance dxy = bfs_x.distance(y);
        Interval I(dxy);
        for (auto w:graph.vertices()) {
            if ((x != w) and (y != w) and (dxy == bfs_x.distance(w) + bfs_y.distance(w))) {
                // insert w in the correct slice of the interval
                I[bfs_x.distance(w)].push_back(w);
            }
        }
        
        // Compute the maximum leanness of the interval
        std::size_t start = std::floor(leanness_lb/2);
        std::size_t end = dxy - start;
        for (std::size_t i=start + 1; i < end; i++) {
            auto S = I[i];

            // Pick k random vertices from slice S
            std::size_t k_max = S.size();
            if (k < S.size()) {
                std::shuffle(S.begin(), S.end(), dre);
                k_max = k;
            }

            // Compute BFS from each of these vertices
            // and check distance to the other vertices of the slice
            // TODO: prune BFS ?
            for (std::size_t j=0; j < k_max; j++) {
                NodeID u = S[j];
                alg::runBFS(bfs_u, graph, u);  // BFS from u
                for (auto v:S) {
                    if (leanness_lb < bfs_u.distance(v)) {
                        leanness_lb = bfs_u.distance(v);
                        std::cout << "new lower bound: " << leanness_lb << "\t" << mytimer;
                        std::cout << "\t(" << considered_fap << ", " << evaluated_intervals << ", " << evaluated_slices << ")" << std::endl;
                    }
                }
            }
        }
    }
    std::cout << "end of search\t" << mytimer << std::endl;

    return leanness_lb;
}





/*
 Class to implement exact algorithms for the leanness of graphs by Coudert et al.
 */
LeannessLowMemory::LeannessLowMemory(Graph const& g) : graph(g)
{
}


/*
 Special initialization for running experiments on diameter methods.
*/
void LeannessLowMemory::init_experiment_diameter(int version_slice, int threshold, int version_hub)
{
    std::size_t n = graph.numberOfNodes();
    _version_slice = version_slice;
    _version_hub = version_hub;
    _threshold = threshold;
    evaluated_slices = 0;
    sum_slices_size = 0;
    min_slices_size = n;
    max_slices_size = 0;

    mytimer.reset();
    std::cout << "Compute hub labeling... " << std::flush;
    if (_version_hub == 1) {
        // std::cout << "Compute hub labeling using Akiba... " << std::endl;
        pll.ConstructIndex(graph);
        pll.PrintStatistics();
        hublab = nullptr;
    } else {
        // std::cout << "Compute hub labeling using Viennot... " << std::endl;
        hublab = alg::computeHubLabeling_ptr(graph);
    }
    //MEASUREMENT::stop(EXP::LEANNESS_TIMER_HUB);
    std::cout << "done\t" << mytimer << std::endl;
}


/*
 Compute the exact leanness of the graph without distance matrix
 Use:
 - incremental far-apart pairs iterator
 - hub labeling to get distances
 */
Distance LeannessLowMemory::compute(std::size_t capacity, int version_interval, int version_slice, int threshold, int version_hub)
{
    std::size_t n = graph.numberOfNodes();
    //MEASUREMENT::start(EXP::COMPLETE_LEANNESS);
    mytimer.reset();
    considered_fap = 0;
    evaluated_intervals = 0;
    evaluated_slices = 0;
    sum_slices_size = 0;
    min_slices_size = n;
    max_slices_size = 0;
    std::cout << "Hub version: " << version_hub << std::endl;
    std::cout << "Interval version: " << version_interval << std::endl;
    std::cout << "Slice version: " << version_slice << std::endl;
    std::cout << "Threshold: " << threshold << std::endl;

    _version_interval = version_interval;
    _version_slice = version_slice;
    _version_hub = version_hub;
    _threshold = threshold;
    leanness_lb = 0;
    
    //MEASUREMENT::start(EXP::LEANNESS_TIMER_HUB);
    std::cout << "Compute hub labeling... " << std::flush;
    if (_version_hub == 1) {
        // std::cout << "Compute hub labeling using Akiba... " << std::endl;
        pll.ConstructIndex(graph);
        pll.PrintStatistics();
        hublab = nullptr;
    } else {
        // std::cout << "Compute hub labeling using Viennot... " << std::endl;
        hublab = alg::computeHubLabeling_ptr(graph);
    }
    //MEASUREMENT::stop(EXP::LEANNESS_TIMER_HUB);
    std::cout << "done\t" << mytimer << std::endl;

    //MEASUREMENT::start(EXP::LEANNESS_TIMER_FA);
    std::cout << "Initialize far apart pairs... " << std::flush;
    // setup iterator of far apart pairs
    assert(capacity >= 2);
    BFSCache bfs_cache(capacity);
    BFSInfo* bfs_x = nullptr;

    auto bfs_cache2 = &bfs_cache;
    FarApartIteratorInterface *FA(0);
    FarApartIterator *_FAI = nullptr;
    _FAI = new FarApartIterator(graph, bfs_cache2, "DHV");
    FA = _FAI;
    
    // Since the far apart iterator has computed eccentricities
    // at initialization, we get these values
    ecc.resize(graph.numberOfNodes());
    for (NodeID id : graph.vertices()) {
        ecc[id] = FA->getEccentricity(id);
    }
    //MEASUREMENT::stop(EXP::LEANNESS_TIMER_FA);
    std::cout << "done\t" << mytimer << std::endl;


    //MEASUREMENT::start(EXP::LEANNESS_TIMER_COMPUTE);
    std::cout << "Start search... " << std::endl;
    Distance leanness_ub = n;
    while (true) { // it's this way to enable time measurements of hasNext()
        auto const has_next = FA->hasNext();
        if (not has_next) { break; }
        considered_fap++;
        
        FarApartPair fap = FA->getNext();
        if (fap.distance <= leanness_lb) {
            // We can not improve
            break;
        }
        NodeID x = fap.source;
        NodeID y = fap.target;
        if (fap.distance < leanness_ub) {
            leanness_ub = fap.distance;
            std::cout << "new upper bound: " << leanness_ub << "\t" << mytimer;
            std::cout << "\t(" << considered_fap << ", " << evaluated_intervals << ", " << evaluated_slices << ")" << std::endl;
        }

        // We compute distances from x
        if (bfs_cache.contains(x)) {
            bfs_x = &bfs_cache.get(x);
        } else {
            bfs_x = &bfs_cache.getNew(x);
            //alg::runBFS(*bfs_x, graph, x);
            alg::runBFSk(*bfs_x, graph, x, fap.distance + 1);
        }

        // We now build the interval I(x,y) and compute it's leanness.
        // During the construction, if a slice has size 1, we end the current interval,
        // compute its leanness, and start a new one.
        // An interval with less than leanness_lb slices can be pruned.
        std::vector<bool> visited(graph.numberOfNodes(), false);
        Distance dx_current = bfs_x->distance(y);
        Interval I(dx_current + 1);
        I[0].push_back(y);
        SliceIndex start = 0;          // first slice of current interval
        SliceIndex end = 1;            // last slice of current interval
        SliceIndex current_slice = 0;  // index of current slice

        while (dx_current > 0) {
            for (NodeID current: I[current_slice]) {
                for (auto neighbor_id: graph.neighborsOf(current)) {
                    if ((not visited[neighbor_id]) and (bfs_x->distance(neighbor_id) + 1 == dx_current)) {
                        visited[neighbor_id] = true;
                        I[end].push_back(neighbor_id);
                    }
                }
            }
            if (I[end].size() == 1) {
                // We end the current interval and compute its leanness if not pruned
                if ((end - start) > leanness_lb) {
                    // We use this interval to update the lower bound on the leanness
                    //MEASUREMENT::start(EXP::LEANNESS_TIMER_INTERVAL);
                    if (_version_interval == 2) {
                        update_leanness_range(I, start, end);
                    } else  { // _version_interval == 3
                        update_leanness_range_v2(I, start, end);
                    }
                    //MEASUREMENT::stop(EXP::LEANNESS_TIMER_INTERVAL);
                }
                // We start a new interval
                start = end;
            }
            current_slice = end;
            end++;
            dx_current--;
        }
    } // Go to the next far apart pair
    //MEASUREMENT::stop(EXP::LEANNESS_TIMER_COMPUTE);
    std::cout << "end of search\t" << mytimer << std::endl;

    std::cout << "Considered FAP:\t" << considered_fap << std::endl;
    std::cout << "Evaluated Intervals:\t" << evaluated_intervals << std::endl;
    std::cout << "Evaluated slices:\t" << evaluated_slices << std::endl;
    std::cout << "Min/Avg/Max size of slices:\t" << min_slices_size;
    std::cout << " / " << (1.0*sum_slices_size)/evaluated_slices;
    std::cout << " / " << max_slices_size << std::endl;
    // Finally return the result
    return leanness_lb;
}


// Update the lower bound on the leanness
void LeannessLowMemory::update_leanness_range(Interval const& I, SliceIndex start, SliceIndex end)
{
    evaluated_intervals++;
    Distances diam(end + 1, 0);   // vector recording the largest distance in each slice
    SliceIndexPairs Q;            // stack or queue of sub-intervals to explore
    Q.push_back({start, end});

    while (Q.size() > 0) {
        auto [left, right] = Q.back();
        Q.pop_back();
        
        // Check if this sub-interval is still promizing
        if ((diam[left] + diam[right])/2 + right - left <= leanness_lb) {
            continue;
        }

        // Compute the largest distance in the middle slice
        SliceIndex middle = (left + right) / 2;
        auto d_middle = slice_diameter(I[middle]);
        diam[middle] = d_middle;

        // Check if it improves the lower bound on the leanness
        if (d_middle > leanness_lb) {
            leanness_lb = d_middle;
            std::cout << "new lower bound: " << leanness_lb << "\t" << mytimer;
            std::cout << "\t(" << considered_fap << ", " << evaluated_intervals << ", " << evaluated_slices << ")" << std::endl;
       }

        // Prepare sub-intervals
        auto dist = middle - left;
        if ((dist > 1) and ((diam[left] + d_middle)/2 + dist > leanness_lb)) {
            Q.push_back({left, middle});
        }
        dist = right - middle;
        if ((dist > 1) and ((d_middle + diam[right])/2 + dist > leanness_lb)) {
            Q.push_back({middle, right});
        }
    }
}

/*
 Check if interval I(x, y) helps improving the leanness.
 Ranges of slices are pruned if not promising.
 */
void LeannessLowMemory::update_leanness_range_v2(Interval const& I, SliceIndex start, SliceIndex end)
{
    evaluated_intervals++;
    Distances diam(end + 1, 0);   // vector recording the largest distance in each slice
    Distance d = end - start;
    std::vector<std::vector<SliceIndexQElem>> Q(d + 1);
    //SliceIndexPriorityQueue Q;
    //Q.push({start, end, d, d});
    Q[d].emplace_back(start, end, d, d);

    while (d > leanness_lb) {
        while (not Q[d].empty()) {
            auto [left, right, length, upper_bound] = Q[d].back();
            Q[d].pop_back();
            
            // Check if this sub-interval is still promizing
            // i.e., if upper_bound = (diam[left] + diam[right])/2 + right - left <= leanness_lb
            if (upper_bound <= leanness_lb) {
                continue;
            }
            
            // Find the most promizing slice in the interval
            SliceIndex middle = (diam[right] - diam[left] + 2 * (left + right)) / 4; // TODO: round...
            Distance d_middle;
            if (middle == left) {
                middle++;
            } else if (middle == right) {
                middle--;
            }
            // Compute the largest distance in the middle slice
            d_middle = slice_diameter(I[middle]);
            diam[middle] = d_middle;
            
            // Check if it improves the lower bound on the leanness
            if (d_middle > leanness_lb) {
                leanness_lb = d_middle;
                std::cout << "new lower bound: " << leanness_lb << "\t" << mytimer;
                std::cout << "\t(" << considered_fap << ", " << evaluated_intervals << ", " << evaluated_slices << ")" << std::endl;
            }
            
            // Prepare sub-intervals
            Distance dist = middle - left;
            if (dist > 1) {
                Distance ub = (diam[left] + d_middle)/2 + dist;
                if (ub > leanness_lb) {
                    Q[ub].emplace_back(left, middle, dist, ub);
                }
            }
            dist = right - middle;
            if (dist > 1) {
                Distance ub = (d_middle + diam[right])/2 + dist;
                if (ub > leanness_lb) {
                    Q[ub].emplace_back(middle, right, dist, ub);
                }
            }
        } // goto next sub-interval with same upper bound
        d--;
    } // goto next sub-interval
}



// Compute the diameter of a slice using the right algorithm
Distance LeannessLowMemory::slice_diameter(Slice const& s)
{
    evaluated_slices++;
    std::size_t slice_size = s.size();
    sum_slices_size += slice_size;
    if (slice_size < min_slices_size) {
        min_slices_size = slice_size;
    } else if (slice_size > max_slices_size) {
        max_slices_size = slice_size;
    }

    switch (_version_slice) {
        case 1: return slice_diameter_basic(s);
        case 2: return slice_diameter_IFUB(s);
        case 3: return slice_diameter_DHV(s);
        default:
            if (s.size() < _threshold) {
                return slice_diameter_basic(s);
            } else {
                return slice_diameter_IFUB(s);
            }
    }
}

// Compute the diameter of a slice using the naive approach
// of evaluating each distance
Distance LeannessLowMemory::slice_diameter_basic(Slice const& s)
{
    Distance best = 0;
    if (_version_hub == 1) {
        for (std::size_t i=0; i < s.size(); i++) {
            NodeID u = s[i];
            for (std::size_t j=0; j < i; j++) {
                Distance d = pll.QueryDistance(u, s[j]);
                if (d > best) {
                    best = d;
                }
            }
        }
    } else {
        for (std::size_t i=0; i < s.size(); i++) {
            NodeID u = s[i];
            for (std::size_t j=0; j < i; j++) {
                Distance d = hublab->distance(u, s[j]);
                if (d > best) {
                    best = d;
                }
            }
        }
    }
    return best;
}

// Compute the diameter of a slice using IFUB on the complete graph
// of the vertices of the slice
Distance LeannessLowMemory::slice_diameter_IFUB(Slice const& s)
{
    // Sort vertices by increasing distance to s[0]
    std::size_t n = s.size();
    NodeID c = s[0];
    Distances dc(n);
    if (_version_hub == 1) {
        for (std::size_t i = 0; i < n; i++) {
            dc[i] = pll.QueryDistance(c, s[i]);
        }
    } else {
        for (std::size_t i = 0; i < n; i++) {
            dc[i] = hublab->distance(c, s[i]);
        }
    }
    auto sorted_slice = sort_slice(s, dc);

    // Find the largest distance
    if (_version_hub == 1) {
        Distance lb = pll.QueryDistance(c, sorted_slice.back());
        for (std::size_t i = n - 1; i > 0; i--) {
            NodeID u = sorted_slice[i];
            if (2*pll.QueryDistance(c, u) <= lb) { break; }
            for (std::size_t j = i - 1; j > 0; j--) {
                Distance d = pll.QueryDistance(u, sorted_slice[j]);
                if (d > lb) {
                    lb = d;
                }
            }
        }
        return lb;
    } else {
        Distance lb = hublab->distance(c, sorted_slice.back());
        for (std::size_t i = n - 1; i > 0; i--) {
            NodeID u = sorted_slice[i];
            if (2*hublab->distance(c, u) <= lb) { break; }
            for (std::size_t j = i - 1; j > 0; j--) {
                Distance d = hublab->distance(u, sorted_slice[j]);
                if (d > lb) {
                    lb = d;
                }
            }
        }
        return lb;
    }
}

// Compute the diameter of a slice using DHV on the complete graph
// of the vertices of the slice
Distance LeannessLowMemory::slice_diameter_DHV(Slice const& s)
{
    if (s.size() < 1) { return 0; }
    if (s.size() == 2) { return pll.QueryDistance(s[0], s[1]); }

    Distances eccs_ub(graph.numberOfNodes(), DIST_INF); // TODO :
    Distances eccs_lb(graph.numberOfNodes(), 0);

    auto my_compare_ub = [&](const NodeID & v1, const NodeID & v2) {
        return eccs_ub[v1] < eccs_ub[v2];
    };
    auto my_compare_lb = [&](const NodeID & v1, const NodeID & v2) {
        return eccs_lb[v1] < eccs_lb[v2];
    };

    // add all nodes to queue
    NodeIDs todo;
    for (auto u: s) {
        todo.push_back(u);
    }

    Distance LB = 0;
    Distance UB = 0;
    Distances dist_x;  // used as cache to reduce the number of calls to pll
    dist_x.resize(graph.numberOfNodes());

    do {
        // 1. Select u such that eccs_ub[u] is maximal
        auto i = std::max_element(todo.begin(), todo.end(), my_compare_ub) - todo.begin();
        auto u = todo[i];
        todo[i] = todo.back();
        todo.pop_back();
        // Compute exact eccentricity of u
        Distance ecc_u = eccs_lb[u];
        for (auto v: todo) {
            Distance duv = pll.QueryDistance(u, v);
            if (duv > ecc_u) {
                ecc_u = duv;
            }
            // update eccentricity lower bounds
            if (duv > eccs_lb[v]) {
                eccs_lb[v] = duv;
            }
        }
        // update diameter lower bound
        if (ecc_u > LB)
            LB = ecc_u;

        // 2. Select x such that dist(u, x) + e(x) = e(u) -- This is minES
        // Since we don't know e(x), we select x minimizing e_L(x) and such that d(u, x) + e_L(x) <= e(u)
        // We then compute e(x)
        // If e(x) = e_L(x), we are done. Otherwise, we update eccentricity bounds and repeat
        while (not todo.empty()) {
            // select the element with minimum eccentricity lower bound
            auto j = std::min_element(todo.begin(), todo.end(), my_compare_lb) - todo.begin();
            auto x = todo[j];
            todo[j] = todo.back();
            todo.pop_back();
            // Compute exact eccentricity of x
            Distance ecc_x = eccs_lb[x];
            NodeID antipode;
            for (auto v: todo) {
                Distance dxv = pll.QueryDistance(x, v);
                dist_x[v] = dxv;  // record to use in next for loop
                if (dxv > ecc_x) {
                    ecc_x = dxv;
                    antipode = v;
                }
            }
            // Update eccentricity bounds
            LB = std::max(LB, ecc_x);
            if (eccs_lb[x] == ecc_x) {
                // We found the good vertex x !
                // We update eccentricity upper bounds and break
                UB = ecc_x;
                for (auto v: todo) {
                    // dist_x[v] is the distance from x to v
                    eccs_ub[v] = std::min(eccs_ub[v], dist_x[v] + ecc_x);
                    if (UB < eccs_ub[v])
                        UB = eccs_ub[v];
                }
                break;
            }
            else {
                // x was not a good choice
                // We use its antipode to update lower bounds
                for (std::size_t l = 0; l < todo.size(); l++)
                    if (todo[l] == antipode) {
                        todo[l] = todo.back();
                        todo.pop_back();
                        break;
                    }
                Distance ecc_a = eccs_lb[antipode];
                for (auto v: todo) {
                    Distance dav = pll.QueryDistance(antipode, v);
                    if (dav > ecc_a) {
                        ecc_a = dav;
                    }
                    if (dav > eccs_lb[v]) {
                        eccs_lb[v] = dav;
                    }
                }
                if (ecc_a > LB) {
                    LB = ecc_a;
                }
            }
        }

        // 3. update eccentricity upper bounds
        // already done above in 1. and 2.

    } while ((LB < UB) and (todo.size() > 1));

    return LB;
}


