/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "basic_types.h"
#include "iterator_interface.h"

namespace unit_tests { void testNaiveIterator(); }

class NaiveIterator : public IteratorInterface
{
private:
	using Index = DistanceMatrix::Index;

	DistanceMatrix const& dist_matrix;
	Index current_index;
	Distance current_distance;

public:
	NaiveIterator(DistanceMatrix const& dist_matrix);

	bool hasNext() override;
	NodePair getNext() override;
	Distance getDistance() const override;
};
