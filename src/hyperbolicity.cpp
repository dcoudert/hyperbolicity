/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "hyperbolicity.h"
#include "algorithms.h"
#include "graph.h"
#include "far_apart_iterator.h"
#include "measurement_tool.h"

#include <random>
#include<cmath> // std::floor
#include <unordered_map>
#include <unordered_set>

Hyperbolicity::Hyperbolicity(Graph const& graph)
	: graph(graph), dist_central(graph.numberOfNodes()) {}


/*
 * This method is the central part of the BCCM (aka Borassi) algorithm for hyperbolicity.
 * Computations are done assuming that vertices are named 0..n-1
 *
 * It takes as input:
 * - number of nodes on which to compute
 * - distance matrix
 * - far apaprt pairs
 *
 * It assumes that the following data structures are initialized
 * - eccs: eccentricities of the vertices
 * - dist_central: distances from a central vertex
 */
void Hyperbolicity::computeBorassi_main(uint n,
                                         DistanceMatrix const& dists,
                                         FarApartPairs const& far_apart_pairs
                                         )
{
    h_lb = 0;
    h_ub = DIST_INF;

    mates.clear();
    mates.resize(n);

    MEASUREMENT::start(EXP::BORASSI_TIMER_COMPUTE);
    for (auto const& far_apart_pair: far_apart_pairs) {
        auto x = far_apart_pair.source;
        auto y = far_apart_pair.target;
        auto const dist = far_apart_pair.distance;

        if (dist < h_ub) {
            if (h_ub <= h_lb) { break; }
            h_ub = dist;
            std::cout<< "h_lb = " << h_lb << "\th_ub = " << h_ub << std::endl;
        }
        if (h_ub <= h_lb) { break; }

        computeAccValBorassi(n, x, y, dists);

        for (auto v: valuable) {
            for (auto w: mates[v]) {
                if (is_acceptable[w]) {
                    // compute delta from already computed values
                    Distance S1 = h_ub + dists(v,w);
                    Distance S2 = dists(x,v) + dists(y,w);
                    Distance S3 = dists(x,w) + dists(y,v);
                    Distance delta = S1 - std::max(S2, S3);

                    if (delta > h_lb) {
                        h_lb = delta;
                        std::cout<< "h_lb = " << h_lb << "\th_ub = " << h_ub << std::endl;
                        if (h_ub == h_lb) {
                            MEASUREMENT::stop(EXP::BORASSI_TIMER_COMPUTE);
                            return;
                        }
                    }
                }
            }
        }

        mates[x].push_back(y);
        mates[y].push_back(x);
    }
    MEASUREMENT::stop(EXP::BORASSI_TIMER_COMPUTE);
}

float Hyperbolicity::computeBorassi()
{
    MEASUREMENT::start(EXP::COMPLETE_BORASSI);
    MEASUREMENT::start(EXP::BORASSI_TIMER_FA);
	preprocessing();

	auto dists = alg::computeDistanceMatrix(graph);
    auto far_apart_pairs = alg::getFarApartPairs(graph, dists, true);
    MEASUREMENT::stop(EXP::BORASSI_TIMER_FA);

    computeBorassi_main(graph.numberOfNodes(), dists, far_apart_pairs);

    MEASUREMENT::stop(EXP::COMPLETE_BORASSI);
    return h_lb/2.;
}


void Hyperbolicity::preprocessing()
{
	eccs = alg::computeAllEccentricities(graph);

	auto min_it = std::min_element(eccs.begin(), eccs.end()) - eccs.begin();
    radius = eccs[min_it];
    alg::runBFS(dist_central, graph, min_it);
    auto max_it = std::max_element(eccs.begin(), eccs.end()) - eccs.begin();
    diameter = eccs[max_it];
    has_mates.assign(graph.numberOfNodes(), false);

    auto mean_eccs = std::accumulate(eccs.begin(), eccs.end(), 0.0)/graph.numberOfNodes();
    std::cout << "radius = " << radius << "\nmean eccentricity = " << mean_eccs << "\ndiameter = " << diameter << std::endl;
}


float Hyperbolicity::compute_v2(std::size_t capacity, bool prune, Distance h_lb_guess, int k_heuristic)
{
	MEASUREMENT::start(EXP::COMPLETE_V2);

	MEASUREMENT::start(EXP::INIT_V2);
        Distance best_h_lb_found = 0;
        
    // setup iterator of far apart pairs
    assert(capacity >= 2);
	BFSCache bfs_cache(capacity);
    
    //auto bfs_cache2 = bfs_info::create_bfs_cache(capacity);
    //auto bfs_cache2 = nullptr;
    auto bfs_cache2 = &bfs_cache;
    FarApartIteratorInterface *FA(0);
    FarApartIterator *_FAI = nullptr;
    _FAI = new FarApartIterator(graph, bfs_cache2, "DHV");
    FA = _FAI;

    // Since the far apart iterator has computed eccentricities
    // at initialization, we get these values
    eccs.resize(graph.numberOfNodes());
    for (NodeID id : graph.vertices()) {
        eccs[id] = FA->getEccentricity(id);
    }
    auto min_it = std::min_element(eccs.begin(), eccs.end()) - eccs.begin();
    radius = eccs[min_it];
    alg::runBFS(dist_central, graph, min_it);
        MEASUREMENT::inc(EXP::BFS_COUNTER_V2);
    auto max_it = std::max_element(eccs.begin(), eccs.end()) - eccs.begin();
    diameter = eccs[max_it];

    auto mean_eccs = std::accumulate(eccs.begin(), eccs.end(), 0.0)/graph.numberOfNodes();
    std::cout << "radius = " << radius << "\nmean eccentricity = " << mean_eccs << "\ndiameter = " << diameter << std::endl;

    has_mates.assign(graph.numberOfNodes(), false);
    
    accval_count = std::numeric_limits<uint>::max();
    is_valuable_int.assign(graph.numberOfNodes(), 0);

    NodeID x, y;
    assert(h_lb_guess < FA->getDistance()); // otherwise increase_cutt_off fails
    h_lb = h_lb_guess;
    h_lb = std::max(h_lb, (Distance)(2.0 * heuristicCCL(k_heuristic, 1, FA->get_cpt_initial_bfs())));
    if (h_lb > 0) { FA->increase_cut_off(h_lb + 1); }
    h_ub = DIST_INF;
	MEASUREMENT::stop(EXP::INIT_V2);
    
    while (true) { // it's this way to enable time measurements of hasNext()
		MEASUREMENT::start(EXP::HAS_NEXT_V2);
		auto const has_next = FA->hasNext();
		MEASUREMENT::stop(EXP::HAS_NEXT_V2);
		if (!has_next) { break; }

		MEASUREMENT::start(EXP::LOOP_INIT_V2);
        FarApartPair p = FA->getNext();
        NodeID x = p.source;
        NodeID y = p.target;
        if (p.distance < h_ub) {
            if (h_ub <= h_lb) { MEASUREMENT::stop(EXP::LOOP_INIT_V2); break; }
            h_ub = p.distance;
            std::cout<< "h_lb = " << h_lb << "\th_ub = " << h_ub << std::endl;
        }
        if (h_ub <= h_lb) { MEASUREMENT::stop(EXP::LOOP_INIT_V2); break; }

        // Use cache of BFS
        auto condacc = 3*(h_lb+1) - 2*h_ub;
        bool fresh_x = false, fresh_y = false;
        if (bfs_cache.contains(x)) {
            bfs_x = &bfs_cache.get(x);
        } else {
            bfs_x = &bfs_cache.getNew(x);
            if (prune) {
                alg::runBFSPrunedAccEcc(*bfs_x, graph, eccs, x, condacc);
            } else {
                alg::runBFS(*bfs_x, graph, x);
            }
			MEASUREMENT::inc(EXP::BFS_COUNTER_V2);
            fresh_x = true;
        }

        if (bfs_cache.contains(y)) {
            bfs_y = &bfs_cache.get(y);
        } else {
			bfs_y = &bfs_cache.getNew(y);
            if (prune) {
                alg::runBFSPrunedAccEcc(*bfs_y, graph, eccs, y, condacc);
            } else {
                alg::runBFS(*bfs_y, graph, y);
            }
			MEASUREMENT::inc(EXP::BFS_COUNTER_V2);
            fresh_y = true;
        }
		MEASUREMENT::stop(EXP::LOOP_INIT_V2);

		MEASUREMENT::start(EXP::ACC_VAL_V2);
        if (prune) { computeAccValPrune(x, y, fresh_x, fresh_y); }
        else { computeAccVal(x, y); }
        has_mates[x] = true;
        has_mates[y] = true;
		MEASUREMENT::stop(EXP::ACC_VAL_V2);

		MEASUREMENT::start(EXP::HLB_UPDATE_V2);
        for (auto v: valuable) {
            is_valuable_int[v] = accval_count;
            // We iterate over all mates of v, starting with largest distance
            for (Distance dist_vw = eccs[v]; dist_vw >= h_ub; --dist_vw) {
                for (auto w: FA->getMates(v, dist_vw)) {
                    if ((is_acceptable_int[w] == accval_count) and (is_valuable_int[w] != accval_count)) {
                        // compute delta from already computed values
                        Distance S1 = h_ub + dist_vw;
                        // We know that v and w were visited in both x,y BFSs
                        Distance S2 = bfs_x->unsafe_distance(v)
                            + bfs_y->unsafe_distance(w);
                        Distance S3 = bfs_x->unsafe_distance(w)
                            + bfs_y->unsafe_distance(v);
                        Distance delta = S1 - std::max(S2, S3);
                        // if S1 < std::max(S2, S3), the next test will ignore
                        // the quadruple (Distance is a signed int) which
                        // is safe because some other pair of far apart pairs
                        // covers that case.

                        if (delta > best_h_lb_found) {
                            best_h_lb_found = delta;
                            if (delta > h_lb) {
                                // could break also here if just want to know that h_lb_guess was ok
                                h_lb = delta;
								if (h_lb < h_ub) {
                                    FA->increase_cut_off(h_lb + 1); // We search for a larger value, so +1
								}
                            } 
                            std::cout<< "best_h_lb_found = "<< best_h_lb_found <<" h_lb = " << h_lb << "\th_ub = " << h_ub  <<" for "<< x <<" "<< y <<" "<< v <<" "<< w << std::endl;
                            if (h_ub == h_lb) {
								MEASUREMENT::stop(EXP::HLB_UPDATE_V2);
								goto end_compute_v2;
                            }
                        }
                    }
                }
            }
        }
		MEASUREMENT::stop(EXP::HLB_UPDATE_V2);

		assert(bfs_cache.contains(x));
		assert(bfs_cache.contains(y));
    }

end_compute_v2:
	MEASUREMENT::stop(EXP::COMPLETE_V2);
    std::cout << (*_FAI) << std::endl;

    return best_h_lb_found/2.;
}

long long int Hyperbolicity::compute_v2_nb_pairs(std::size_t capacity)
{
	MEASUREMENT::start(EXP::COMPLETE_V2);

	MEASUREMENT::start(EXP::INIT_V2);
        
    // setup iterator of far apart pairs
	BFSCache bfs_cache(capacity);
    
    //auto bfs_cache2 = bfs_info::create_bfs_cache(capacity);
    //auto bfs_cache2 = nullptr;
    auto bfs_cache2 = &bfs_cache;
    auto FA = FarApartIterator(graph, bfs_cache2, "DHV");
    
    // Since the far apart iterator has computed eccentricities
    // at initialization, we get these values
    eccs.resize(graph.numberOfNodes());
    for (NodeID id : graph.vertices()) {
        eccs[id] = FA.getEccentricity(id);
    }
    auto min_it = std::min_element(eccs.begin(), eccs.end()) - eccs.begin();
    radius = eccs[min_it];
    alg::runBFS(dist_central, graph, min_it);
        MEASUREMENT::inc(EXP::BFS_COUNTER_V2);
    auto max_it = std::max_element(eccs.begin(), eccs.end()) - eccs.begin();
    diameter = eccs[max_it];

    auto mean_eccs = std::accumulate(eccs.begin(), eccs.end(), 0.0)/graph.numberOfNodes();
    std::cout << "radius = " << radius << "\nmean eccentricity = " << mean_eccs << "\ndiameter = " << diameter << std::endl;

    has_mates.assign(graph.numberOfNodes(), false);
    
    accval_count = std::numeric_limits<uint>::max();

    NodeID x, y;
    h_ub = DIST_INF;
    std::vector<long long int> nb_pairs(diameter+1, 0);
    long long int nb_tot = 0;
	MEASUREMENT::stop(EXP::INIT_V2);
    
    while (true) { // it's this way to enable time measurements of hasNext()
		MEASUREMENT::start(EXP::HAS_NEXT_V2);
		auto const has_next = FA.hasNext();
		MEASUREMENT::stop(EXP::HAS_NEXT_V2);
		if (!has_next) { break; }

		MEASUREMENT::start(EXP::LOOP_INIT_V2);
        FarApartPair p = FA.getNext();
        if (p.distance < h_ub) {
            if (h_ub <= diameter) {
                std::cout<<"nb_pairs = "<< nb_pairs[h_ub]
                         <<" at dist = "<< h_ub <<"\n";
            }
            h_ub = p.distance;
        }
        ++(nb_pairs[h_ub]);
        ++nb_tot;
        MEASUREMENT::stop(EXP::LOOP_INIT_V2);
    }
    std::cout<<"nb_pairs ="<< nb_pairs[h_ub]
             <<" at dist = "<< h_ub <<"\n";
    return nb_tot;
}
        

void Hyperbolicity::computeAccVal(NodeID x, NodeID y)
{
    //is_acceptable.assign(graph.numberOfNodes(), false);
    if (accval_count == std::numeric_limits<uint>::max()) {
        is_acceptable_int.assign(graph.numberOfNodes(), 0);
        accval_count = 1;
    } else {
        ++accval_count;
    }
    valuable.clear();
    
    auto condacc = 3*(h_lb+1) - 2*h_ub;
    
    for (NodeID i: graph.vertices()) {
        if (has_mates[i]) {
            if (bfs_x->visited(i) && bfs_y->visited(i)) {
                Distance xi = bfs_x->unsafe_distance(i);
                Distance yi = bfs_y->unsafe_distance(i);
                if (2*(eccs[i] - xi) >= condacc && 2*(eccs[i] - yi) >= condacc) {
                    if (2*xi >= h_lb+1 && 2*yi >= h_lb+1) {
                        if (2*eccs[i] >= 2*(h_lb+1) - h_ub + xi + yi) {
                            is_acceptable_int[i] = accval_count;
                            // h_ub = xy distance  - Lemma 13 and Definition 14
                            if (2*dist_central[i] + h_ub - h_lb > xi + yi) {
                                valuable.push_back(i);
                            }
                        }
                    }
                }
            }
        }
    }
}

// Same as computeAccVal but avoid unecessary tests after pruning is used in BFS
// fresh_x and fresh_y indicate wether pruned BFS was performed on x and y resp.
void Hyperbolicity::computeAccValPrune(NodeID x, NodeID y, bool fresh_x, bool fresh_y)
{
    //is_acceptable.assign(graph.numberOfNodes(), false);
    if (accval_count == std::numeric_limits<uint>::max()) {
        is_acceptable_int.assign(graph.numberOfNodes(), 0);
        accval_count = 1;
    } else {
        ++accval_count;
    }
    valuable.clear();
    
    auto condacc = 3*(h_lb+1) - 2*h_ub;
    
    const std::vector<NodeID> &visitSet = bfs_y->visitSet.size() < bfs_x->visitSet.size() ?
                                                            bfs_x->visitSet : bfs_y->visitSet;
    
    for (NodeID i: visitSet) {
        if (has_mates[i]) {
            if (bfs_x->visited(i) && bfs_y->visited(i)) {
                Distance xi = bfs_x->unsafe_distance(i);
                Distance yi = bfs_y->unsafe_distance(i);
                if (2*(eccs[i] - xi) >= condacc && 2*(eccs[i] - yi) >= condacc) {
                    if (2*xi >= h_lb+1 && 2*yi >= h_lb+1) {
                        if (2*eccs[i] >= 2*(h_lb+1) - h_ub + xi + yi) {
                            is_acceptable_int[i] = accval_count;
                            // h_ub = xy distance  - Lemma 13 and Definition 14
                            if (2*dist_central[i] + h_ub - h_lb > xi + yi) {
                                valuable.push_back(i);
                            }
                        }
                    }
                }
            }
        }
    }
}

void Hyperbolicity::computeAccValBorassi(uint n, NodeID x, NodeID y, DistanceMatrix const& dists)
{
	is_acceptable.assign(n, false);
	valuable.clear();

	auto condacc = 3*(h_lb+1) - 2*h_ub;

    // We assume that vertices are numbered from 0 to n-1
    for (uint i = 0; i < n; i++) {
		if (!mates[i].empty()) {
			if (2*(eccs[i] - dists(x, i)) >= condacc && 2*(eccs[i] - dists(y,i)) >= condacc) {
				if (2*dists(x, i) >= h_lb+1 && 2*dists(y,i) >= h_lb+1) {
					if (2*eccs[i] >= 2*(h_lb+1) - h_ub + dists(x, i) + dists(y,i)) {
						is_acceptable[i] = true;
						// h_ub = xy distance  - Lemma 13 and Definition 14
						if (2*dist_central[i] + h_ub - h_lb > dists(x, i) + dists(y,i)) {
							valuable.push_back(i);
						}
					}
				}
			}
		}
	}
}


// TODO: make sure that we just need one of the pairs (x,y) and (y,x) and not both
NodePairs Hyperbolicity::getSortedNodePairs(DistanceMatrix const& dists)
{
	NodePairs node_pairs;
	for (std::size_t i = 0; i < graph.numberOfNodes(); ++i) {
		for (std::size_t j = i+1; j < graph.numberOfNodes(); ++j) {
			node_pairs.push_back({i, j});
		}
	}

	auto larger_dist = [&](NodePair const& pair1, NodePair const& pair2) {
		return dists(pair1.first, pair1.second) > dists(pair2.first, pair2.second);
	};
	std::sort(node_pairs.begin(), node_pairs.end(), larger_dist);

	return node_pairs;
}


/*
 * Heuristic proposed in Cohen, Coudert, Lancin 2015 http://dx.doi.org/10.1145/2780652
 *
 * Repeat:
 *  Starting from a random vertex, finds a pair (a,b) of distant vertices using
 *  k-sweep like approach (at least one BFS). Then selects a subset of vertices
 *  at same distance from a and b (within a ball), and compute delta(a,b,c,d) for
 *  all c,d in this subset. Repeat trials time and return best result. The search
 *  space is pruned whenever possible.
 */
float Hyperbolicity::heuristicCCL(std::size_t k, Distance epsilon, int max_bfs)
{
    h_lb = 0;
    if (k == 0) {
        return 0;
    }
    int n_bfs = 0;

    BFSInfo bfs_x;
    BFSInfo bfs_y;
    BFSInfo bfs_u;
    bfs_x.prepare(graph.numberOfNodes());
    bfs_y.prepare(graph.numberOfNodes());
    bfs_u.prepare(graph.numberOfNodes());

	// for random vertex selection
	std::default_random_engine dre(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<> uid(0, graph.numberOfNodes()-1);

    for (std::size_t cpt = 0; cpt < k; cpt++) {
        /*
        NodeID x;
        NodeID y;
        do {
            NodeID r = selector(vertices); // get random vertex
            alg::runBFS(bfs_u, graph, r, x);  // BFS from random vertex
            alg::runBFS(bfs_x, graph, x, y);  // BFS from x
        }
        while (bfs_x.distances[y] <= h_lb);
        */

        if (n_bfs >= max_bfs) break;

        NodeID x = uid(dre); // get random vertex
        NodeID y;
        alg::runBFS(bfs_x, graph, x, y); // BFS from x
        n_bfs++;
        Distance dist_xy;
        do {
            x = y;
            dist_xy = bfs_x.distance(y);
            alg::runBFS(bfs_x, graph, x, y);
            n_bfs++;
            if (n_bfs >= max_bfs) break;
        }
        while (dist_xy < bfs_x.distance(y));

        dist_xy = bfs_x.distance(y);
        alg::runBFS(bfs_y, graph, y); // BFS from y
        n_bfs++;
        if (n_bfs >= max_bfs) break;
        
        // Find vertices at equal distance from x and y (+/- epsilon)
        std::vector<NodeID> middle;
        for (NodeID i: graph.vertices()) {
            if ((2 * bfs_x.distance(i) > h_lb) and
                (2 * bfs_y.distance(i) > h_lb) and
                (std::abs(bfs_x.distance(i) - bfs_y.distance(i)) <= epsilon)) {
                middle.push_back(i);
            }
        }

        // Compute delta(x, y, u, v) for all pairs (u, v) in middle
        for (NodeID u: middle) {
            if ((2 * bfs_x.distance(u) > h_lb) and
                (2 * bfs_y.distance(u) > h_lb)) {
                alg::runBFS(bfs_u, graph, u);
                n_bfs++;
                if (n_bfs >= max_bfs) break;
                
                for (NodeID v: middle) {
                    if (bfs_u.distance(v) > h_lb) {
                        auto S1 = dist_xy + bfs_u.distance(v);
                        auto S2 = bfs_x.distance(u) + bfs_y.distance(v);
                        auto S3 = bfs_x.distance(v) + bfs_y.distance(u);
                        Distance tmp;
                        if (S1 >= S2) {
                            if (S2 >= S3) { tmp = S1 - S2; }
                            else if (S1 > S3) { tmp = S1 - S3; }
                            else { tmp = S3 - S1; }
                        }
                        else if (S1 >= S3) { tmp = S2 - S1; }
                        else if (S2 > S3) { tmp = S2 - S3; }
                        else {tmp = S3 - S2; }

                        if (tmp > h_lb) {
                            h_lb = tmp;
                            std::cout<< "h_lb = " << h_lb << " (" << x << ", " << y << ", " << u << ", " << v << ")" << std::endl;
                        }
                    }
                }
            }
        }
    }
    return h_lb/2.;
}



float Hyperbolicity::testEccentricities() {
    Distances ecc = alg::computeAllEccentricitiesDHV(graph);

    for (auto v : graph.vertices()) {
        std::cout <<"ecc "<< v <<" "<< ecc[v] <<"\n";
    }

    return 0.;
}
