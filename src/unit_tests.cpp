/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "unit_tests.h"

#include "algorithms.h"
#include "cache.h"
#include "defs.h"
#include "io.h"
#include "graph.h"
#include "naive_iterator.h"
#include "far_apart_iterator.h"
#include "hyperbolicity.h"

#include <chrono>

//
// Define some helpers
//

#define TEST(x)                                                                \
	do {                                                                       \
		if (!(x)) {                                                            \
			std::cout << "\n";                                                 \
			std::cout << "TEST_FAILED!\n";                                     \
			std::cout << "File: " << __FILE__ << "\n";                         \
			std::cout << "Line: " << __LINE__ << "\n";                         \
			std::cout << "Function: " << __func__ << "\n";                     \
			std::cout << "Test: " << #x << "\n";                               \
			std::cout << "\n";                                                 \
			std::cout << std::flush;                                           \
			std::abort();                                                      \
		}                                                                      \
	} while (0)

void unit_tests::testAll()
{
	//testGraph();
    //testCache();
	// testAlgorithms();
	// testNaiveIterator();
    //testImprovedIterator1();
    //testDistanceFromFarVerticesIterator2();
    // testImprovedIterator2();
    //testFarApartIterator();
    testFarApartIteratorBFS();
    //testDavid();
    //testHyperbolicity();
}

// TODO: extend tests
void unit_tests::testGraph()
{
	auto parser_edges = io::readGraph("./graphs/ca-GrQc.bcc.edgelist");

	Graph g;
	g.init(parser_edges);

	TEST(g.numberOfNodes() == 2651);
	TEST(g.numberOfEdges() == 10480);
    
    std::size_t cpt = 0;
    for (auto v: g.vertices()) { (void) v; cpt++; }
    TEST( cpt == g.numberOfNodes() );
}


void unit_tests::testAlgorithms()
{
    auto parser_edges = io::readGraph("./graphs/ca-GrQc.bcc.edgelist");
    
    Graph g;
    g.init(parser_edges);
    
    // test BFS
    BFSInfo bfs_0, bfs_42;
    alg::runBFS(bfs_0, g, 0);
    alg::runBFS(bfs_42, g, 42);
    TEST(bfs_0.distance(42) == bfs_42.distance(0));
    
    // compare eccentricities with naive computation
    Distances eccs_naive(g.numberOfNodes());
    for (NodeID node_id = 0; node_id < g.numberOfNodes(); ++node_id) {
        eccs_naive[node_id] = alg::computeEccentricity(g, node_id);
    }
    
    auto eccs_alg = alg::computeAllEccentricities(g);
    TEST(std::equal(eccs_naive.begin(), eccs_naive.end(), eccs_alg.begin()));
    
    auto eccs_DHV = alg::computeAllEccentricitiesDHV(g);
    TEST(std::equal(eccs_naive.begin(), eccs_naive.end(), eccs_DHV.begin()));
    
    std::cout << "=== Test Diameter ========" << std::endl;
    // test diameter
    {
        auto start = std::chrono::high_resolution_clock::now();
        auto diameter = alg::computeDiameter(g);
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        TEST(diameter == 11);
        start = std::chrono::high_resolution_clock::now();
        diameter = alg::computeDiameterDHV(g);
        stop = std::chrono::high_resolution_clock::now();
        auto duration2 = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        TEST(diameter == 11);
        std::cout << "Compute diameter in time (ms): naive = " << duration.count() << " and DVH = " << duration2.count() << std::endl;
    }
    std::cout << "==========================" << std::endl;
    
    // test far vertices
    {
        auto parser_edges_7 = io::readGraph("./unit_test_data/7_circle.edgelist");
        auto parser_edges_8 = io::readGraph("./unit_test_data/8_circle.edgelist");
        
        Graph g_7;
        Graph g_8;
        g_7.init(parser_edges_7);
        g_8.init(parser_edges_8);
        
        auto start_7 = std::chrono::high_resolution_clock::now();
        BFSInfo bfs_7;
        alg::runBFS(bfs_7, g_7, 0);
        auto stop_7 = std::chrono::high_resolution_clock::now();
        auto duration_7 = std::chrono::duration_cast<std::chrono::nanoseconds>(stop_7 - start_7); // can also be cast to micro/milliseconds
		(void) duration_7;
        auto far_vertices_7 = alg::getFarVertices(g_7, bfs_7);
        BFSInfo bfs_8;
        alg::runBFS(bfs_8, g_8, 0);
        auto far_vertices_8 = alg::getFarVertices(g_8, bfs_8);
        TEST(far_vertices_7.size() == 2);
        TEST(far_vertices_8.size() == 1);
        
        //auto start_7_b = std::chrono::high_resolution_clock::now();
        auto far_vertices_7_b = alg::getFarVertices(g_7, 0);
        //auto stop_7_b = std::chrono::high_resolution_clock::now();
        //auto duration_7_b = std::chrono::duration_cast<std::chrono::nanoseconds>(stop_7_b - start_7_b);
        auto far_vertices_8_b = alg::getFarVertices(g_8, 0);
        TEST(far_vertices_7_b.size() == 2);
        TEST(far_vertices_8_b.size() == 1);
        //std::cout << "Compute 0-Far vertices in time (ns) " << duration_7.count() << " and " << duration_7_b.count() << std::endl;
    }
    
    // distance matrix
    auto dist_matrix = alg::computeDistanceMatrix(g);
    BFSInfo bfs;
    alg::runBFS(bfs, g, 23);
    TEST(bfs.distance(42) == dist_matrix(23, 42));
    alg::runBFS(bfs, g, 75);
    TEST(bfs.distance(96) == dist_matrix(75, 96));
    alg::runBFS(bfs, g, 275);
    TEST(bfs.distance(196) == dist_matrix(275, 196));
    
    
    {
        // recreate distances from far vertices
        alg::runBFS(bfs, g, 483);
        auto far_vertices = alg::getFarVertices(g, bfs);
        auto recovered_distances = alg::computeDistsFromFarVertices(g, far_vertices);
        for (NodeID u : g.vertices()) {
            TEST(bfs.distance(u) == recovered_distances[u]);
        }
        
        far_vertices = alg::getFarVertices(g, 483);
        recovered_distances = alg::computeDistsFromFarVertices(g, far_vertices, true);
        for (NodeID u : g.vertices()) {
            TEST(bfs.distance(u) == recovered_distances[u]);
        }
    }
}

void unit_tests::testNaiveIterator()
{
	auto parser_edges = io::readGraph("./graphs/ca-GrQc.bcc.edgelist");

	Graph g;
	g.init(parser_edges);

	auto dist_matrix = alg::computeDistanceMatrix(g);
	auto it = NaiveIterator(dist_matrix);

	std::size_t count = 0;
	Distance last_distance = DIST_INF;

	while (it.hasNext()) {
		it.getNext();
		auto distance = it.getDistance();
		TEST(distance <= last_distance);
		last_distance = distance;
		++count;
	}
	TEST(count == g.numberOfNodes()*(g.numberOfNodes()-1));
}



void unit_tests::testFarApartIterator()
{
    auto parser_edges = io::readGraph("./graphs/ca-GrQc.bcc.edgelist");

    Graph g;
    g.init(parser_edges);
    auto it_naive = NaiveFarApartIterator(g);
    auto it_improved = FarApartIterator(g, nullptr);
    std::size_t i = 0;
    /*
    while (it_naive.hasNext()) {
        auto pair_naive = it_naive.getNext();
        ++i;
    }
    std::cout << i;
    std::size_t j = 0;
    while (it_improved.hasNext()) {
        auto pair_improved = it_improved.getNext();
        ++j;
    }
    std::cout << " " << j << std::endl;
    TEST(i == j);
    */
    // We check that iterators report far apart pairs in the right order of distances
    while (it_naive.hasNext() && it_improved.hasNext()) {
        auto pair_naive = it_naive.getNext();
        auto pair_improved = it_improved.getNext();
        //std::cout << pair_naive << " / " << pair_improved << std::endl;
        TEST(pair_naive.distance == pair_improved.distance);
        ++i;
    }
    TEST(i == 1379633);
    //std::cout << it_improved << std::endl;
}

void unit_tests::testFarApartIteratorBFS()
{
    auto parser_edges = io::readGraph("./unit_test_data/grid100.txt");

    Graph g;
    g.init(parser_edges);
    BFSCache bfs_cache(1000);
    auto it_improved = FarApartIterator(g, &bfs_cache);
    std::cout << it_improved << std::endl;
    std::size_t j = 0;
    while (it_improved.hasNext()) {
        it_improved.getNext();
        ++j;
    }
    std::cout << it_improved << std::endl;
}


// TODO: extend tests
void unit_tests::testDavid()
{
	auto parser_edges = io::readGraph("./unit_test_data/toy.edgelist");

	Graph g;
	g.init(parser_edges);
    std::cout << "=== Test pretty print ====" << std::endl;
    std::cout << g << std::endl;
    std::cout << "==========================" << std::endl;

}

// TODO: This is now not possible anymore as we set the BFSInfo type at compile time
// void unit_tests::testCache()
// {
//     auto C = Cache<int, int>(4);
//     for (int i = 0; i < 4; i++)
//         C.add(i, i + 10);
//     TEST(C.contains(2) == true);
//     TEST(C.get(2) == 12);
//     C.add(4, 14);
//     C.add(5, 15);
//     C.add(6, 16);
//     TEST(C.contains(2) == true);
//     TEST(C.contains(1) == false);
//     
//     auto parser_edges = io::readGraph("./graphs/ca-GrQc.bcc.edgelist");
//     Graph g;
//     g.init(parser_edges);
//     for (int bfs_info_type = 0; bfs_info_type < 4; ++bfs_info_type) {
//         BFSCache* BFS = bfs_info::create_bfs_cache(4, bfs_info_type);
//         for (NodeID id = 0; id < 5; ++id) {
//             BFSInfo &bfs = BFS->getNew(id);
//             alg::runBFS(bfs, g, id);
//         }
//         TEST(BFS->contains(0) == false);
//         TEST(BFS->contains(1) == true);
//         auto bfs_1 = BFS->get(1);
//         BFSInfo bfs;
//         alg::runBFS(bfs, g, 1);
//         for (NodeID u : g.vertices()) {
//             TEST(bfs_1.distance(u) == bfs.distance(u));
//         }
//     }
// }

void unit_tests::testHyperbolicity()
{
    auto parser_edges = io::readGraph("./graphs/ca-GrQc.bcc.edgelist");
    Graph g;
    g.init(parser_edges);
    std::cout << "=== Test Hyperbolicity ===" << std::endl;
    std::cout << g << std::endl;

	std::cout << "Borassi:" << std::endl;
    auto start1 = std::chrono::high_resolution_clock::now();
    auto H = Hyperbolicity(g);
    auto hyp1 = H.computeBorassi();
    auto stop1 = std::chrono::high_resolution_clock::now();
    auto duration1 = std::chrono::duration_cast<std::chrono::milliseconds>(stop1 - start1);
    TEST(hyp1 == 3.5);
    std::cout << "Compute hyperbolicity in time (ms) " << duration1.count() << std::endl;

	std::cout << "New Approach:" << std::endl;
    auto start2 = std::chrono::high_resolution_clock::now();
    auto H2 = Hyperbolicity(g);
    auto hyp2 = H2.compute_v2(1000);
    auto stop2 = std::chrono::high_resolution_clock::now();
    auto duration2 = std::chrono::duration_cast<std::chrono::milliseconds>(stop2 - start2);
    TEST(hyp2 == 3.5);
    std::cout << "Compute hyperbolicity in time (ms) " << duration2.count() << std::endl;

    std::cout << "==========================" << std::endl;
}


// just in case anyone does anything stupid with this file...
#undef TEST
