/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "basic_types.h"
#include "bfs_info.h"

#include <vector>
#include <set>

class Graph;

namespace unit_tests
{
    void testHyperbolicity();
}

class Hyperbolicity
{
private:
	Graph const& graph;

	void preprocessing();
	void computeAccVal(NodeID x, NodeID y);
	void computeAccValPrune(NodeID x, NodeID y, bool fresh_x, bool fresh_y);
	void computeAccValBorassi(uint n, NodeID x, NodeID y, DistanceMatrix const& dists);
	NodePairs getSortedNodePairs(DistanceMatrix const& dists);

	Distance h_lb;
	Distance h_ub;

	BFSInfo* bfs_x = nullptr;
	BFSInfo* bfs_y = nullptr;
	Distances dist_central;
	Distances eccs;
    Distance radius;
    Distance diameter;

	std::vector<bool> is_acceptable;
    std::vector<uint> is_acceptable_int;
    uint accval_count;
    std::vector<uint> is_valuable_int;
	NodeIDs valuable;

    std::vector<bool> has_mates;
	// should only be used in Borassi
	std::vector<NodeIDs> mates;

    void computeBorassi_main(uint n, DistanceMatrix const& dists,
                             FarApartPairs const& far_apart_pairs);

public:
	Hyperbolicity(Graph const& graph);

	float computeBorassi();
	float compute();
	float compute_v2(std::size_t capacity = 2, bool prune = false, Distance h_lb_guess = 0, int k_heuristic = 10);
	long long int compute_v2_nb_pairs(std::size_t capacity = 1);
        float heuristicCCL(std::size_t k, Distance epsilon = 1, int max_bfs = 500);

    float testEccentricities();
};
