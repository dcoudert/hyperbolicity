/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>

template <typename T>
class Matrix
{
private:
	using Container = typename std::vector<T>;
	Container elements;

public:
    using Size = unsigned int;
    //Size const n, m;
    Size n, m;

    struct Index {
    private:
        Size _n, _m;
        Size _i = 0;
        Size _j = 0;

    public:
        Index(Matrix const& matrix) : _n(matrix.n), _m(matrix.m) {}

        Size i() const { return _i; }
        Size j() const { return _j; }
        bool valid() const { return _i < _n && _j < _m; }
        Index const& operator++() {
            if (_j == _m-1) { _j = 0; ++_i; }
            else { ++_j; }
            return *this;
        }
        void reset() { _i = 0; _j = 0; }
    };

    Matrix(Size n = 0, Size m = 0, T default_value = T())
        : elements(n*m, default_value), n(n), m(m) {}

    T& operator()(Size i, Size j) { return elements[i*m + j]; }
    T& operator[](Index index) { return elements[index.i()*m + index.j()]; }
    T const& operator()(Size i, Size j) const { return elements[i*m + j]; }
    T const& operator[](Index index) const { return elements[index.i()*m + index.j()]; }
    typename Container::iterator begin() { return elements.begin(); }
    typename Container::iterator end() { return elements.end(); }
    typename Container::const_iterator begin() const { return elements.cbegin(); }
    typename Container::const_iterator end() const { return elements.cend(); }
    
    void resize(Size new_n, Size new_m) {
        n = new_n;
        m = new_m;
        elements.resize(n*m);
    }
    void resize(Size new_n, Size new_m, T default_value) {
        n = new_n;
        m = new_m;
        elements.assign(n*m, default_value);
    }
};
