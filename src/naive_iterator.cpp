/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "naive_iterator.h"

#include <algorithm>

NaiveIterator::NaiveIterator(DistanceMatrix const& dist_matrix)
	: dist_matrix(dist_matrix), current_index(dist_matrix)
{
	current_distance = *std::max_element(dist_matrix.begin(), dist_matrix.end());
}

bool NaiveIterator::hasNext()
{
	while (current_distance != 0) {
		while (current_index.valid()) {
			if (dist_matrix[current_index] == current_distance) { return true; }
			++current_index;
		}
		current_index.reset();
		--current_distance;
	}

	return false;
}

NodePair NaiveIterator::getNext()
{
	NodePair pair = {current_index.j(), current_index.i()};
	++current_index;
	return pair;
}

Distance NaiveIterator::getDistance() const
{
	return current_distance;
}
