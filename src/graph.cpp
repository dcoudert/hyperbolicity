/*
 hyperbolicity
 Copyright (C) 2021  COUDERT David <david.coudert@inria.fr>
                     NUSSER André <anusser@mpi-inf.mpg.de>
                     VIENNOT Laurent <laurent.viennot@inria.fr>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "graph.h"

#include <algorithm>

void Graph::init(ParserData data)
{
	initOffsets(data);

	PRINT("Graph info:");
	PRINT("===========");
	printInfo();
}

void Graph::printInfo() const
{
#ifndef NVERBOSE
	uint active_nodes = 0;
	double avg_deg = 0;
	std::vector<uint> deg;

	for (NodeID node_id = 0; node_id < number_of_nodes; ++node_id) {
		if (degree(node_id) != 0) {
			deg.push_back(degree(node_id));
			avg_deg += deg.back();
			++active_nodes;
		}
	}

	PRINT("#nodes: " << number_of_nodes << ", #edges: " << number_of_edges << "\n");
	auto mm_deg = std::minmax_element(deg.begin(), deg.end());
	avg_deg /= active_nodes;

	PRINT("min/max/avg degree: "
		<< *mm_deg.first     << " / " << *mm_deg.second     << " / " << avg_deg);
#endif
}


std::ostream& operator<<(std::ostream& os, Graph& G)
{
    uint active_nodes = 0;
    double avg_deg = 0;
    std::vector<uint> deg;

    for (NodeID node_id = 0; node_id < G.number_of_nodes; ++node_id) {
        if (G.degree(node_id) != 0) {
            deg.push_back(G.degree(node_id));
            avg_deg += deg.back();
            ++active_nodes;
        }
    }

    os << "#nodes: " << G.number_of_nodes << ", #edges: " << G.number_of_edges << "\n";
    auto mm_deg = std::minmax_element(deg.begin(), deg.end());
    avg_deg /= active_nodes;
    os << "min/max/avg degree: " << *mm_deg.first     << " / " << *mm_deg.second     << " / " << avg_deg;
    return os;
}




void Graph::initOffsets(ParserData& data)
{
	number_of_nodes = data.number_of_nodes;
	auto& edges = data.edges;

	if (number_of_nodes == 0) { return; }

	// add reverse edges and then deduplicate
	auto original_size = edges.size();
	for (std::size_t i = 0; i < original_size; ++i) {
		auto& edge = edges[i];
		edges.push_back({edge.target, edge.source});
	}
	std::sort(edges.begin(), edges.end());
	auto new_end = std::unique(edges.begin(), edges.end());
	edges.erase(new_end, edges.end());

	assert(edges.size()%2 == 0);
	number_of_edges = edges.size()/2;

	// init neighbors, offsets, degree, and is_active
	neighbors.reserve(edges.size());
	std::vector<std::size_t> degree(number_of_nodes, 0);
	offsets.reserve(number_of_nodes+1);
	for (auto const& edge: edges) {
		neighbors.push_back(edge.target);
		++degree[edge.source];
	}

	offsets.push_back(0);
	std::size_t sum = 0;
	for (NodeID node_id = 0; node_id < number_of_nodes; ++node_id) {
		sum += degree[node_id];
		offsets.push_back(sum);
	}
	assert(offsets[number_of_nodes] == 2*number_of_edges);
}

uint Graph::degree(NodeID node_id) const
{
	return offsets[node_id+1] - offsets[node_id];
}

ConstNeighbors Graph::neighborsOf(NodeID node_id) const
{
	auto begin = neighbors.begin() + offsets[node_id];
	auto end = neighbors.begin() + offsets[node_id+1];
	return ConstNeighbors(begin, end);
}

NodeIDRange Graph::vertices() const
{
    NodeID begin = 0;
    NodeID end = number_of_nodes;
    return NodeIDRange(begin, end);
}
