#include "hyperbolicity_dom.h"
#include "algorithms.h"
#include "graph.h"
#include "far_apart_iterator.h"
#include "measurement_tool.h"

#include <random>
#include <numeric>
#include <cmath> // std::floor
#include <unordered_map>
#include <unordered_set>
#include <queue>

#include <iomanip>

/*
 Initialisation / preprocessing phase:
 1) Compute all dominating sets
    For each dominating set, we store:
    - For each vertex of the dominating set: the list of dominated vertices and the True radius (largest distance to a dominated vertex)
    - For each u, its dominator and the distance to the dominator
 
    Actually, we want a hierarchical dominating set ensuring that is if u is dominated by v in a dominating set at distance d, and that v is dominated by w in a dominating set at distance d' > d, then w also dominates u in the dominating set at distance d'.
  
 2) Compute 2-hop labeling
 
 
 Main Phase:
 1) Ensuring that the dominating set with largest distance is small enough
    - Compute distance matrix and sort pairs by decreasing distance for the higher level
    - Explore 4-tuples and maintain current best value h_lb and current range (according dominating distances)
    - Given a 4-tuple, let h be be it's value and [hmin, h, hmax] be it's range.
    - If hmax > h_lb (= the 4-tuple can help improving h_lb), we call recursive method
 
 2) Recursive method:
    - Input: a 4-tuple and a domination level
    - For each dominated 4-tuples at lower domination level
      if the 4-tuple can help improving, recurse
 
 */


// =====================================================================
// Hierarchical Dominating Set
// =====================================================================

// Assume we want k dominating sets from distance 0, 1, 2, ..., d/ratio, d
// We denote dd[i] the domination distance for the i-th dominating set
// We start for the layer k with distance dd[k]
// 1) We first compute a distance dd[k] dominating set
//  - for each dominator, we record the vertices it dominates and the largest distance
// 2) for i from k-1 down to 1
//  - for each vertex u in the distance dd[i+1] dominating set
//  - compute a distance dd[i] dominating set of the vertices dominated by u in the distance dd[i+1] dominating set
//  - record the vertices dominated by each vertex and the largest distance
// 3) the dd[0] dominating set is graph.vertices()

HierarchicalDominatingSet::HierarchicalDominatingSet(Graph const& graph, Distance d, float ratio,
                             Distance known_h_lb, uint version)
: graph(graph), version(version), max_value(0)
{
    assert((ratio > 1) or (d == 1));

    version_priority  = version % 10;
    int high          = version / 10;
    version_use_rec   = (high == 0);
    version_connected = (high == 2) or (high == 4);
    bool closest      = (high == 4) or (high == 5);

    // To select default d:
    if (d == DIST_INF) {
        d = 1;
        Distance d_gap = d;
        while ( 8 * (d_gap + Distance(ratio*d)) - 2 < known_h_lb ) {
            d = Distance(ratio * d);
            d_gap += d;
        }
    }

    // Determine all domination distances: d, d/ratio, d/ratio^2,..., 0
    Distance d2 = d;
    domination_distances.push_back(d);
    while (d2 > 0) {
        d2 = Distance(std::floor(float(d2)/ratio));
        domination_distances.push_back(d2);
    }
    std::reverse(domination_distances.begin(), domination_distances.end());
    nb_layers = domination_distances.size();

    // Prepare data needed to order vertices
    if ((version_priority == 1) or (version_priority == 2)) {  // by degree
        for (auto v: graph.vertices()) {
            values.push_back(graph.degree(v));
        }
    } else if ((version_priority == 3) or (version_priority == 4)) {  // by eccentricity
        for (auto e: alg::computeAllEccentricitiesDHV(graph)) {
            values.push_back(e);
        }
    }
    if (values.size() > 0) {
        max_value = *std::max_element(values.begin(), values.end());
    }
    // std::cout << "max value = " << max_value<<std::endl;

    // for statistics
    stat_init();

    dominating_sets_vec.resize(nb_layers);
    if (version_use_rec) {
        NodeIDs all_vertices;
        for (auto v : graph.vertices()) { all_vertices.push_back(v); }
        NodeIDs dom = compute_dominating_set_rec(nb_layers - 1, all_vertices);
    } else {
        compute_dominating_set_iter(version_connected, closest);
    }
    stat_end();
}

// Version of the constructor enabling to give as input the vector of eccentricities
HierarchicalDominatingSet::HierarchicalDominatingSet(Graph const& graph, Distances const& eccentricities,
                                                     Distance d, float ratio,
                                                     Distance known_h_lb, uint version)
: graph(graph), version(version), max_value(0)
{
    assert((ratio > 1) or (d == 1));

    version_priority  = version % 10;
    int high          = version / 10;
    version_use_rec   = (high == 0);
    version_connected = (high == 2) or (high == 4);
    bool closest      = (high == 4) or (high == 5);

    // To select default d:
    if (d == DIST_INF) {
        d = 1;
        Distance d_gap = d;
        while ( 8 * (d_gap + Distance(ratio*d)) - 2 < known_h_lb ) {
            d = Distance(ratio * d);
            d_gap += d;
        }
    }

    // Determine all domination distances: d, d/ratio, d/ratio^2,..., 0
    Distance d2 = d;
    domination_distances.push_back(d);
    while (d2 > 0) {
        d2 = Distance(std::floor(float(d2)/ratio));
        domination_distances.push_back(d2);
    }
    std::reverse(domination_distances.begin(), domination_distances.end());
    nb_layers = domination_distances.size();

    // Prepare data needed to order vertices
    if ((version_priority == 1) or (version_priority == 2)) {  // by degree
        for (auto v: graph.vertices()) {
            values.push_back(graph.degree(v));
        }
    } else if ((version_priority == 3) or (version_priority == 4)) {  // by eccentricity
        if (eccentricities.size() == graph.numberOfNodes()) {
            for (auto e: eccentricities) {
                values.push_back(e);
            }
        } else { // We compute eccentricities
            for (auto e: alg::computeAllEccentricitiesDHV(graph)) {
                values.push_back(e);
            }
        }
    }
    if (values.size() > 0) {
        max_value = *std::max_element(values.begin(), values.end());
    }
    // std::cout << "max value = " << max_value<<std::endl;

    // for statistics
    stat_init();
    
    dominating_sets_vec.resize(nb_layers);
    if (version_use_rec) {
        NodeIDs all_vertices;
        for (auto v : graph.vertices()) { all_vertices.push_back(v); }
        NodeIDs dom = compute_dominating_set_rec(nb_layers - 1, all_vertices);
    } else {
        compute_dominating_set_iter(version_connected, closest);
    }

    stat_end();
}


/*
 This method takes as input the index of a layer and a list of vertices to dominate.
 It computes a dominating set of this set and store it in D.
 Furthermore, for each vertex v of this dominating set, dominating a set Dv, it
 computes (using a recursive call) a dominating set of Dv and store in the data for
 this layer the dominator data (v, radius, dominating set of Dv).
 */
NodeIDs HierarchicalDominatingSet::compute_dominating_set_rec(uint layer, NodeIDs & vertices)
{
    NodeIDs D;
    if (layer == 0) {
        for (auto v: vertices) {
            D.push_back(v);
            dominating_sets_vec[layer][v] = {v, 0, {v}};
        }
    } else {
        BFSInfo bfs_info;
        prioritize_vertices(vertices);
        std::unordered_set<NodeID> active(vertices.begin(), vertices.end());
        
        for (auto v: vertices) {
            if (active.find(v) != active.end()) {
                // We add v to the dominating set
                D.push_back(v);
                // and set active vertices at distance <= k as dominated
                alg::runBFSk(bfs_info, graph, v, domination_distances[layer]);
                Distance radius = 0;
                NodeIDs dominated_vertices;
                for (auto u: bfs_info.visitSet) {
                    if (active.find(u) != active.end()) {
                        active.erase(u);
                        dominated_vertices.push_back(u);
                        radius = std::max(radius, bfs_info.distance(u));
                    }
                }
                stat_add_radius(layer, radius);
                stat_add_ball_size(layer, dominated_vertices.size());

                // Now that we know the vertices dominated by v, we search for a dominating set of these vertices
                NodeIDs Dv = compute_dominating_set_rec(layer - 1, dominated_vertices);
                // and we store the data for v
                dominating_sets_vec[layer][v] = DominatorData(v, radius, Dv);
                
                stat_add_dom_size(layer, Dv.size());
            }
        }
    }
    return D;
}

/*
 This method computes a hierarchical dominating set iteratively.
 The order of operations can ensure that the balls are connected.
 */
void HierarchicalDominatingSet::compute_dominating_set_iter(bool connected, bool closest)
{
    auto n = graph.numberOfNodes();
    NodeIDs vertices(n);
    std::iota(vertices.begin(), vertices.end(), 0); // Fill with 0, 1, ..., n-1.
    prioritize_vertices(vertices);

    NodeIDs ball(n, 0);  // Use to limit the propagation of a BFS to a ball, initially unique
    BFSInfo bfs_info;  // Used when connected is false
    for (uint layer = top_layer_index(); layer > 0; layer--) {
        bool good_layer = (layer < top_layer_index());

        Distance k = domination_distances[layer];
        NodeIDs dominator(n);  // store the vertex that dominates v
        std::vector<bool> is_dominator(n, false);  // whether a vertex is a dominator
        Distances dist(n, DIST_INF);  // store distance from dominator
        std::vector<bool> active(n, true);  // whether a vertex has already been seen or not
        NodeIDs dom; // the dominating set of this layer
        
        for (auto source: vertices) {
            if (active[source]) {
                dist[source] = 0;
                dominator[source] = source;
                is_dominator[source] = true;
                dom.push_back(source);
                active[source] = false;

                if (connected) {
                    // Search for a connected set of vertices dominated by source
                    std::queue<NodeID> queue;
                    queue.push(source);
                    while (not queue.empty()) {
                        auto current = queue.front();
                        auto dc = dist[current];
                        if (dc == k) {
                            // we are done
                            break;
                        }
                        queue.pop();
                        for (auto u: graph.neighborsOf(current)) {
                            if ((not is_dominator[u]) and (ball[source] == ball[u])) {
                                // source can dominate u
                                if (active[u] or (closest and (dc + 1 < dist[u]))) {
                                    dist[u] = dc + 1;
                                    dominator[u] = source;
                                    queue.push(u);
                                    active[u] = false;
                                }
                            }
                        }
                    }
                } else {
                    // Search for all vertices at distance up to k that can be dominated by source
                    alg::runBFSk(bfs_info, graph, source, k);
                    for (auto u: bfs_info.visitSet) {
                        if ((not is_dominator[u]) and (ball[source] == ball[u])) {
                            // source can dominate u
                            if ((active[u]) or (closest and (bfs_info.distance(u) < dist[u]))) {
                                // first time we see u or source is a closer dominator
                                active[u] = false;
                                dominator[u] = source;
                                dist[u] = bfs_info.distance(u);
                            }
                        }
                    }
                }
            }
        }
        
        // We now extract the dominating sets, the domination radius, etc.
        NodeIDs next_vertices;  // store the vertices in correct order for next round
        Distances radius(n, 0);  // store domination radius
        std::vector<NodeIDs> D(n);  // dominating sets
        
        for (auto u: vertices) {
            auto source = dominator[u];
            radius[source] = std::max(radius[source], dist[u]);
            D[source].push_back(u);
        }
        
        for (auto source: dom) {
            dominating_sets_vec[layer][source] = DominatorData(source, radius[source], {});
            // add source in the dominating set of dominators of upper layer
            if (good_layer) {
                dominating_sets_vec[layer + 1][ball[source]].vertices.push_back(source);
            }
            // order vertices for next layer
            prioritize_vertices(D[source]);
            next_vertices.insert(next_vertices.end(), D[source].begin(), D[source].end());
            // for statistics
            stat_add_radius(layer, radius[source]);
            stat_add_ball_size(layer, D[source].size());
        }

        // we update the domains and the order of vertices for next layer
        std::swap(ball, dominator);
        std::swap(vertices, next_vertices);
    }
    for (auto v: vertices) {
        dominating_sets_vec[0][v] = {v, 0, {v}};
    }
    if (top_layer_index() >= 1) {
        for (auto v: vertices) {
            dominating_sets_vec[1][ball[v]].vertices.push_back(v);
        }
    }
    
    // some more statistics
    for (uint layer=1; layer < nb_layers; layer++) {
        for (auto const& data: dominating_sets_vec[layer]) {
            stat_add_dom_size(layer, data.second.vertices.size());
        }
    }
}




/*
    This method takes as input a list of vertices and reorder them according
    the version of the algorithm.
    0. do nothing
    1. by increasing degree
    2. by decreasing degree
    3. by increasing eccentricity
    4. by decreasing eccentricity
    5. random order
 */
void HierarchicalDominatingSet::prioritize_vertices(NodeIDs & vertices)
{
    if (vertices.size() < 2) {
        return;
    } else if ((1 <= version_priority) and (version_priority <= 4)) {
        // sort vertices by increasing values using counting sort
        if (max_value < vertices.size()) {
            // use inplace counting sort
            std::vector<std::size_t> offset(max_value + 1, 0);
            for (auto v: vertices) {
                offset[values[v]]++;
            }
            std::size_t tot = std::accumulate(offset.begin(), offset.end(), 0);
            assert( tot == vertices.size());
            for (int i=max_value; i >= 0; i--) {
                tot -= offset[i];
                offset[i] = tot;
                //std::cout << i << " " << offset[i] << " " << tot << std::endl;
            }
            NodeIDs tmp(vertices.size());
            for (auto v: vertices) {
                auto val = values[v];
                auto i = offset[val];
                tmp[i] = v;
                offset[val]++;
            }
            vertices.swap(tmp);
            if ((version_priority == 2) or (version_priority == 4)) {
                std::reverse(vertices.begin(), vertices.end());
            }
        } else {
            // use standard sort
            std::vector<std::pair<int, NodeID> > tmp;
            for (auto v: vertices) {
                tmp.emplace_back(values[v], v);
            }
            if ((version_priority == 2) or (version_priority == 4)) {
                std::sort(tmp.rbegin(), tmp.rend());
            } else {
                std::sort(tmp.begin(), tmp.end());
            }
            vertices.clear();
            for (auto e: tmp) {
                vertices.push_back(e.second);
            }
        }
    } else if (version_priority == 5) {
        // Random order
        std::random_device rd;
        std::mt19937 g(rd());
        std::shuffle(vertices.begin(), vertices.end(), g);
    }
}



uint HierarchicalDominatingSet::dominating_set_size(uint layer)
{
    assert( layer < nb_layers );
    return dominating_sets_vec[layer].size();
}

Distance HierarchicalDominatingSet::domination_distance(uint layer) const
{
    assert( layer < nb_layers );
    return domination_distances[layer];
}

NodeIDs HierarchicalDominatingSet::dominating_set(uint layer)
{
    assert( layer < nb_layers );
    NodeIDs dom;
    for (auto z: dominating_sets_vec[layer]) {
        dom.push_back(z.first);
    }
    return dom;
}

// Return the vertices of the dominating set at layer-1 that are dominated by node u at layer layer
ConstNodeIDCRange HierarchicalDominatingSet::dominated_vertices(uint layer, NodeID const u) const
{
    if ( layer < nb_layers ) {
        auto it_u = dominating_sets_vec[layer].find(u);
        if (it_u != dominating_sets_vec[layer].end()) {
            return ConstNodeIDCRange(it_u->second.vertices.begin(), it_u->second.vertices.end());
        }
    }

    // FIXME: this is a bad hack that only works because we won't even access an element,
    // but because 's' is local it will be deleted and thus the iterators are invalid.

    // Otherwise, we return empty range
    NodeIDs s;
    return ConstNodeIDCRange(s.begin(), s.end());
}

// Return the number of vertices of the dominating set at layer-1 that are dominated by node u at layer layer
std::size_t HierarchicalDominatingSet::dominated_vertices_size(uint layer, NodeID const u) const
{
    if ( layer < nb_layers ) {
        auto it_u = dominating_sets_vec[layer].find(u);
        if (it_u != dominating_sets_vec[layer].end()) {
            return it_u->second.vertices.size();
        }
    }
    // Otherwise, we return 0
    return 0;
}

Distance HierarchicalDominatingSet::domination_radius(uint layer, NodeID const u) const
{
    if ( layer < nb_layers ) {
        auto it_u = dominating_sets_vec[layer].find(u);
        if (it_u != dominating_sets_vec[layer].end()) {
            return it_u->second.radius;
        }
    }
    return DIST_INF;
}

void HierarchicalDominatingSet::stat_init()
{
    // for statistics
    auto d = domination_distances[top_layer_index()];
    stat_radius_min.assign(nb_layers, d);
    stat_radius_max.assign(nb_layers, 0);
    stat_radius_mean.assign(nb_layers, 0);
    stat_ball_min.assign(nb_layers, graph.numberOfNodes());
    stat_ball_max.assign(nb_layers, 0);
    stat_ball_mean.assign(nb_layers, 0);
    stat_dom_min.assign(nb_layers, graph.numberOfNodes());
    stat_dom_max.assign(nb_layers, 0);
    stat_dom_mean.assign(nb_layers, 0);
}

void HierarchicalDominatingSet::stat_add_radius(uint layer, Distance radius)
{
    if (radius < stat_radius_min[layer]) { stat_radius_min[layer] = radius; }
    if (radius > stat_radius_max[layer]) { stat_radius_max[layer] = radius; }
    stat_radius_mean[layer] += radius;
}

void HierarchicalDominatingSet::stat_add_ball_size(uint layer, uint64_t ball_size)
{
    if (ball_size < stat_ball_min[layer]) { stat_ball_min[layer] = ball_size; }
    if (ball_size > stat_ball_max[layer]) { stat_ball_max[layer] = ball_size; }
    stat_ball_mean[layer] += ball_size;
}

void HierarchicalDominatingSet::stat_add_dom_size(uint layer, uint64_t dom_size)
{
    if (dom_size < stat_dom_min[layer]) { stat_dom_min[layer] = dom_size; }
    if (dom_size > stat_dom_max[layer]) { stat_dom_max[layer] = dom_size; }
    stat_dom_mean[layer] += dom_size;
}


void HierarchicalDominatingSet::stat_end()
{
    stat_radius_min[0] = 0;
    stat_ball_min[0] = stat_ball_max[0] = 1;
    stat_ball_mean[0] = graph.numberOfNodes();
    stat_dom_min[0] = 0;
    for (int l = nb_layers - 1; l >= 0; l--) {
        stat_radius_mean[l] /= dominating_set_size(l);
        stat_ball_mean[l] /= dominating_set_size(l);
        stat_dom_mean[l] /= dominating_set_size(l);
    }
}


std::ostream& operator<<(std::ostream& os, HierarchicalDominatingSet& H)
{
    os << "=== Hierarchical dominating set ===";
    os << "\n- version = " << H.version;
    os << "\n- # layers = "<< H.number_of_layers();
    os << "\n- distances :";
    for (int i = H.top_layer_index(); i >= 0; i--) {
        os << " " << H.domination_distances[i];
    }
    os << "\n- top layer size = " << H.dominating_set_size(H.top_layer_index());
    os << "\n- Statistics:";
    os << "\n  k\tmin r\tavg r\tmax r\tmin B\tavg B\tmax B\tmin b\tavg b\tmax b\t|D|";
    os << std::fixed << std::setprecision(1);
    for (int l = H.nb_layers - 1; l >= 0; l--) {
        os << "\n  " << H.domination_distances[l];
        os << "\t" << H.stat_radius_min[l];
        os << "\t" << H.stat_radius_mean[l];
        os << "\t" << H.stat_radius_max[l];
        os << "\t" << H.stat_ball_min[l];
        os << "\t" << H.stat_ball_mean[l];
        os << "\t" << H.stat_ball_max[l];
        os << "\t" << H.stat_dom_min[l];
        os << "\t" << H.stat_dom_mean[l];
        os << "\t" << H.stat_dom_max[l];
        os << "\t" << H.dominating_set_size(l);
    }
    os << "\n===================================\n";
    return os;
}






// =====================================================================
// Hyperbolicity using Hierarchical Dominating Set
// =====================================================================

#define _tau_(_S1_, _S2_, _S3_) (_S2_ >= _S3_ ? (_S1_ > _S2_ ? _S1_ - _S2_ : 0) : (_S1_ >= _S3_ ? _S1_ - _S3_ : 0))
#define _delta12_(_S1_, _S2_, _S3_) (_S2_ >= _S3_ ? _S1_ - _S2_ : (_S1_ >= _S3_ ? _S1_ - _S3_ : _S3_ - _S1_))
#define _delta_(_S1_, _S2_, _S3_) (_S1_ >= _S2_ ? _delta12_(_S1_, _S2_, _S3_) : _delta12_(_S2_, _S1_, _S3_))



HyperbolicityDom::HyperbolicityDom(Graph const& graph)
    : graph(graph) {}


// Version using ideas from Borassi et al. + real domination radius
float HyperbolicityDom::compute_DOM_DFS(int version, Distance d, float ratio, uint dom_version,
                                           uint max_side, uint max_cache_size, Distance known_h_lb)
{
    assert(max_cache_size >= 7);
    mytimer.reset();
    std::cout << "max cache size = " << max_cache_size << std::endl;

    h_lb = known_h_lb;
    max_matrix_side = max_side;

    // 1. Compute eccentricities
    std::cout << "Compute eccentricities... " << std::flush;
    ecc = alg::computeAllEccentricitiesDHV(graph);
    std::cout << "done\t" << mytimer << std::endl;
    
    // 2. Choose central vertex and compute distances from this vertex
    std::cout << "Selected central vertex and compute distances from it... " << std::flush;
    Distance radius = DIST_INF;
    Distance diameter = 0;
    for (auto v: graph.vertices()) {
        if (ecc[v] < radius) {
            radius = ecc[v];
            central_vertex = v;
        } else if (ecc[v] > diameter) {
            diameter = ecc[v];
        }
    }
    dist_central.resize(graph.numberOfNodes());
    alg::runBFS(dist_central, graph, central_vertex);
    std::cout << "done\t" << mytimer << std::endl;
    auto mean_eccs = std::accumulate(ecc.begin(), ecc.end(), 0.0)/graph.numberOfNodes();
    std::cout << "radius = " << radius
              << "\nmean eccentricity = " << mean_eccs
              << "\ndiameter = " << diameter << std::endl;
    
    // 3. Compute hierarchical dominating set
    std::cout << "Compute hierarchical dominating set... " << std::endl;
    HierarchicalDominatingSet HDS(graph, ecc, d, ratio, h_lb, dom_version);
    auto top_layer_index = HDS.top_layer_index();
    assert( HDS.dominating_set_size(top_layer_index) >= 4 );  // might be removed...
    auto d_gap = HDS.domination_distance(top_layer_index);
    auto two_d_gap = 2 * d_gap;
    std::cout << HDS << mytimer << std::endl;
    
    
    // 4. Build and order the list of pairs of top dominating set
    std::cout << "Compute distance matrix between vertices of top dominating set... " << std::endl;
    auto dom = HDS.dominating_set(top_layer_index);
    auto n_dom = dom.size();
    auto dist = alg::computeDistanceMatrix(graph, dom);
    FarApartPairs pairs;
    for (uint i = 0; i < n_dom; i++) {
        for (uint j = i+1; j < n_dom; j++) {
            pairs.emplace_back(i, j, dist(i, j));
        }
    }
    std::sort(pairs.rbegin(), pairs.rend());
    // We turn the algorithm to an exact algorithm
    for (uint i = 0; i < n_dom; i++) {
        pairs.emplace_back(i, i, 0);
    }
    Distances dom_radius(n_dom);
    for (uint i = 0; i < n_dom; i++) {
        dom_radius[i] = HDS.domination_radius(top_layer_index, dom[i]);
    }
    std::cout << "Top dominating set: " << pairs.size() << " pairs\t" << mytimer << std::endl;
    
    // Vector to record best upper bound obtained for each pair xy during first round.
    // During second round, we can avoid considering pairs xy for which this bound is less than h_lb.
    Distances best_bound(pairs.size(), 0);

    // 5. Apply modified BCCM
    for (std::size_t round = 0; round < 2; round ++) {
        if (round == 0) {
            std::cout << "=== round 1: compute lower bound using top dominating set ===" << std::endl;
        } else {
            if ((version == 1) or (version == 2) or (version == 3)) {
                // Compute hub labeling - needed to explore quadruples
                std::cout << "=====================================\n";
                if (version == 1) {
                    std::cout << "Compute hub labeling using Akiba... " << std::endl;
                    pll.ConstructIndex(graph);
                    pll.PrintStatistics();
                    hublab = nullptr;
                } else {
                    std::cout << "Compute hub labeling using Viennot... " << std::endl;
                    hublab = alg::computeHubLabeling_ptr(graph);
                }
                std::cout << "done\t" << mytimer << "\nInitialize cache of distance matrices..." << std::flush;
                // initialize arrays and matrices used to save calls to hub labeling
                cache_of_matrices.clear();
                for (uint layer = 0; layer < HDS.number_of_layers(); layer++) {
                    // the number of matrices of a layer can not exceed the number of pairs
                    uint64_t square = HDS.dominating_set_size(layer)*HDS.dominating_set_size(layer);
                    auto max_size = (max_cache_size < square ? max_cache_size : square);
                    if (version == 1) {
                        cache_of_matrices.emplace_back(HDS, pll, layer, max_size);
                    } else {
                        cache_of_matrices.emplace_back(HDS, hublab, layer, max_size);
                    }
                }
                storage_capacity = 2 * HDS.number_of_layers() + 1;
                storage_of_distances.resize(storage_capacity);
                for (std::size_t i=0; i < storage_capacity; i++) {
                    storage_of_distances[i].resize(graph.numberOfNodes());
                }
                std::cout << " done\t" << mytimer << std::endl;
                std::cout << "=== round 2: explore quadruples ===" << std::endl;
            } else {
                // We stop with an approximation...
                break;
            }
        }

        h_ub = DIST_INF;
        std::vector<uint> is_acceptable_int(n_dom, 0);
        uint acceptable_count = 0;
        NodeIDs valuable;
        NodeIDs active;
        std::vector<bool> is_active(n_dom, false);
        std::vector<NodeIDs> mates;
        mates.resize(n_dom);
        uint64_t quad_done = 0, log = 2;
        uint64_t use_best_bound = 0;
        evaluated_quadruples.assign(HDS.number_of_layers() + 1, 0);
        checked_quadruples.assign(HDS.number_of_layers() + 1, 0);

        for (uint i = 0; i < pairs.size(); i++) {
            auto pi = pairs[i];
            auto x = pi.source;
            auto y = pi.target;
            auto const dxy = pi.distance;

            if (dxy < h_ub) {
                h_ub = dxy;
                std::cout << "h_lb = " << h_lb << "\tpair_dist = " << h_ub << "\t" << mytimer << std::endl;
            }
            if (h_ub + two_d_gap <= h_lb) { break; }
            if (round == 1) {
                if (best_bound[i] <= h_lb) {
                    use_best_bound++;
                    // We know we cannot improve with this pair
                    // We update mates and go to the next pair.
                    if (is_active[x] == false) {
                        is_active[x] = true;
                        active.push_back(x);
                    }
                    if (is_active[y] == false) {
                        is_active[y] = true;
                        active.push_back(y);
                    }
                    mates[x].push_back(y);
                    mates[y].push_back(x);
                    continue;
                }
            }
            if (h_ub + dom_radius[x] + dom_radius[y] <= h_lb) { continue; }

            // Compute Acceptable and Valuable
            acceptable_count++;
            valuable.clear();
            for (auto u: active) {
                Distance dxu = dist(x, u);
                Distance dyu = dist(y, u);
                if ((2 * (dxu + dom_radius[x] + dom_radius[u]) > h_lb) and (2 * (dyu + dom_radius[y] + dom_radius[u]) > h_lb)) {
                    Distance detour = dxu + dyu - dxy;
                    Distance sum_radius = 2 * (2 * dom_radius[u] + dom_radius[x] + dom_radius[y]);
                    Distance value = 2 * ecc[dom[u]] + sum_radius;
                    if (value > 2 * h_lb + 2 + detour) {
                        auto mymax = 2 * std::max((dxu > dom_radius[x] ? dxu - dom_radius[x] : 0),
                                                  (dyu > dom_radius[y] ? dyu - dom_radius[y] : 0));
                        if (value + 2 * dxy >  mymax + 3 * h_lb + 3) {
                            is_acceptable_int[u] = acceptable_count;
                            if (2 * dist_central[dom[u]] + sum_radius > h_lb + detour) {
                                valuable.push_back(u);
                            }
                        }
                    }
                }
            }
            
            // explore quadruples with u valuable and v acceptable
            Distance best_delta_and_radius = 0;
            for (auto u: valuable) {
                for (auto v: mates[u]) {
                    if (is_acceptable_int[v] == acceptable_count) {
                        evaluated_quadruples[top_layer_index]++;
                        // We consider the quadruple only when S1 is the largest sum
                        Distance S1 = dxy + dist(u, v);
                        Distance S2 = dist(x, u) + dist(y, v);
                        if (S1 < S2) {
                            continue;
                        }
                        Distance S3 = dist(x, v) + dist(y, u);
                        if (S1 < S3) {
                            continue;
                        }
                        //Distance delta = _tau_(S1, S2, S3);
                        Distance delta = S1 - (S2 > S3 ? S2 : S3);
                        Distance delta_and_radius = delta + 2*(dom_radius[u] + dom_radius[v] + dom_radius[x] + dom_radius[y]);
                        best_delta_and_radius = std::max(best_delta_and_radius, delta_and_radius);

                        if (delta > h_lb) {
                            h_lb = delta;
                            std::cout << "h_lb = " << h_lb
                                      << "\tpair_dist = " << h_ub <<" for "
                                      << dom[x] <<" "<< dom[y] <<" "<< dom[u] <<" "<< dom[v]
                                      << "\t" << mytimer << std::endl;
                        }
                        if ((round == 1) and (delta_and_radius > h_lb)) {
                            compute_DOM_DFS_rec(HDS, top_layer_index, dom[u], dom[v], dom[x], dom[y]);
                            ++quad_done;
                            checked_quadruples[top_layer_index]++;
                            if (quad_done >= log) {
                                std::cout << "checked quadruples: " << quad_done << " / " << evaluated_quadruples[top_layer_index]
                                          << " -- h_lb = " << h_lb << "\t" << mytimer << std::endl;
                                log *= 2;
                            }
                        }
                    }
                }
            }
            best_bound[i] = best_delta_and_radius;
            
            if (is_active[x] == false) {
                is_active[x] = true;
                active.push_back(x);
            }
            if (is_active[y] == false) {
                is_active[y] = true;
                active.push_back(y);
            }
            mates[x].push_back(y);
            mates[y].push_back(x);
        }

        // Finally
        if (round == 0) {
            std::cout << "evaluated quadruples: " << evaluated_quadruples[top_layer_index] << std::endl;
            evaluated_quadruples[top_layer_index] = 0;  // reset to get good count for next round
        } else if (round == 1) {
            std::cout << "cuts with best bound: " << use_best_bound << std::endl;
            std::cout << "checked quadruples: " << quad_done << " / " << evaluated_quadruples[top_layer_index] << std::endl;
            std::cout << "=====================================" << std::endl;
            uint64_t tot_checked = 0;
            for (uint64_t tt: checked_quadruples) { tot_checked += tt; }
            uint64_t tot_evaluated = 0;
            for (uint64_t tt: evaluated_quadruples) { tot_evaluated += tt; }
            std::cout << "Total checked quadruples: " << tot_checked << " / " << tot_evaluated << std::endl;
            std::cout << "ki\tchecked\tevaluated\n";
            for (int i = top_layer_index; i >= 0; i--) {
                std::cout << HDS.domination_distance(i) << "\t" << checked_quadruples[i]
                          << "\t" << evaluated_quadruples[i] << std::endl;
            }
            std::cout << "=====================================" << std::endl;
            std::cout << mytimer << std::endl;
        }
    }

    // Finally return the result
    return h_lb / 2.0;
}

/*
 This method takes as input 4 vertices and a layer index.
 It considers the 4-tuples formed by the vertices that are dominated at this layer by the 4 vertices.
 If a considered 4-tuple is promising, it perform on recursive call for this 4-tuple on lower layer.
 */
void HyperbolicityDom::compute_DOM_DFS_rec(HierarchicalDominatingSet const& HDS,
                                              uint layer, NodeID up, NodeID vp, NodeID xp, NodeID yp)
{
    assert( layer > 0 );

    // We store the radius of the balls dominated by x, y, u, v
    Distances & radius = storage_of_distances[2 * layer];
    for (auto zp: {xp, yp, up, vp}) {
        for (auto z: HDS.dominated_vertices(layer, zp)) {
            radius[z] = HDS.domination_radius(layer - 1, z);
        }
    }

    // Count the number of dominated vertices to decide which implementation to use
    std::size_t nn = 0;
    for (auto zp: {xp, yp, up, vp}) {
        nn = std::max(nn, HDS.dominated_vertices_size(layer, zp));
    }

    if (nn < max_matrix_side) {
        // Here, we use matrices to cache distances between the vertices dominated
        // by each pair ap,bp in {up,vp,xp,yp}. We use 1 matrix per pair.
        // For each zp in {up,vp,xp,yp} we use a mapping from the vertices dominated
        // by zp to 0,1,...,n-1  where n is the number of dominated vertices.
        std::unordered_map<NodeID, NodeIDs> int_to_vertex;
        for (auto zp: {xp, yp, up, vp}) {
            int_to_vertex[zp].clear();
            for (auto z: HDS.dominated_vertices(layer, zp)) {
                int_to_vertex[zp].push_back(z);
            }
        }

        // We now consider the quadruples (u, v, x, y)
        DistanceMatrix const& dist_xy = cache_of_matrices[layer].get(xp, yp, false);
        for (std::size_t xi = 0; xi < int_to_vertex[xp].size(); xi++) {
            NodeID x = int_to_vertex[xp][xi];

            for (std::size_t yi = 0; yi < int_to_vertex[yp].size(); yi++) {
                NodeID y = int_to_vertex[yp][yi];
                Distance dxy = dist_xy(xi, yi);

                if (dxy + radius[x] + radius[y] <= h_lb) { continue; }

                // Check acceptable and valuable vertices in up and vp
                std::unordered_map<NodeID, NodeIDs> acceptable;
                std::unordered_map<NodeID, NodeIDs> valuable;
                for (auto zp: {up, vp}) {
                    DistanceMatrix const& dist_xu = cache_of_matrices[layer].get(xp, zp);
                    DistanceMatrix const& dist_yu = cache_of_matrices[layer].get(yp, zp);
                    for (std::size_t ui = 0; ui < int_to_vertex[zp].size(); ui++) {
                        NodeID u = int_to_vertex[zp][ui];
                        Distance dxu = dist_xu(xi, ui);
                        Distance dyu = dist_yu(yi, ui);
                        if ((2 * (dxu + radius[x] + radius[u]) > h_lb) and (2 * (dyu + radius[y] + radius[u]) > h_lb)) {
                            Distance detour = dxu + dyu - dxy;
                            Distance ecc_u = ecc[u];
                            Distance sum_radius = 2 * (2 * radius[u] + radius[x] + radius[y]);
                            Distance value = 2 * ecc_u + sum_radius;
                            if (value > 2 * h_lb + 2 + detour) {
                                auto mymax = 2 * std::max((dxu > radius[x] ? dxu - radius[x] : 0),
                                                          (dyu > radius[y] ? dyu - radius[y] : 0));
                                if (value + 2 * dxy >  mymax + 3 * h_lb + 3) {
                                    acceptable[zp].push_back(ui);
                                    if (2 * dist_central[u] + sum_radius > h_lb + detour) {
                                        valuable[zp].push_back(ui);
                                    }
                                }
                            }
                        }
                    }
                }

                // Explore quadruples with u valuable and v acceptable, and then with v valuable and u acceptable
                // We must prevent considering a pair twice
                std::unordered_set<NodePair> visited_pairs;
                std::vector<std::tuple<NodeID, NodeID>> order = {{up, vp}, {vp, up}};
                for (auto [ap, bp]: order) {
                    if ((valuable[ap].size() == 0) or (acceptable[bp].size() == 0)) {
                        continue;  // nothing to do
                    }
                    DistanceMatrix const& dist_xu = cache_of_matrices[layer].get(xp, ap, false);
                    DistanceMatrix const& dist_yu = cache_of_matrices[layer].get(yp, ap, false);
                    DistanceMatrix const& dist_xv = cache_of_matrices[layer].get(xp, bp, false);
                    DistanceMatrix const& dist_yv = cache_of_matrices[layer].get(yp, bp, false);
                    DistanceMatrix const& dist_uv = cache_of_matrices[layer].get(ap, bp, true);
                    for (auto ui: valuable[ap]) {
                        NodeID u = int_to_vertex[ap][ui];
                        Distance dxu = dist_xu(xi, ui);
                        Distance dyu = dist_yu(yi, ui);
                        for (auto vi: acceptable[bp]) {
                            NodeID v = int_to_vertex[bp][vi];
                            NodePair p = (u < v ? std::make_pair(u, v) : std::make_pair(v, u));
                            if (visited_pairs.find(p) != visited_pairs.end()) {
                                continue;  // the pair has already been considered
                                // TODO: find a faster way to do this test ?
                            } else {
                                visited_pairs.insert(p);
                            }
                            evaluated_quadruples[layer - 1]++;
                            Distance S1 = dxy + dist_uv(ui, vi);
                            Distance S2 = dxu + dist_yv(yi, vi);
                            // We don't want to consider negative values of delta
                            // So we do successive tests
                            if (S1 < S2) {
                                continue;
                            }
                            Distance S3 = dist_xv(xi, vi) + dyu;
                            if (S1 < S3) {
                                continue;
                            }
                            Distance delta = S1 - (S2 > S3 ? S2 : S3);

                            if (delta > h_lb) {
                                h_lb = delta;
                                std::cout << "h_lb = " << h_lb <<" for "
                                          << u <<" "<< v <<" "<< x <<" "<< y
                                          << "\t" << mytimer << std::endl;
                            }
                            if ((layer > 1) and (delta + 2*(radius[u] + radius[v] + radius[x] + radius[y]) > h_lb)) {
                                compute_DOM_DFS_rec(HDS, layer - 1, u, v, x, y);
                                checked_quadruples[layer - 1]++;
                            }
                        }
                    }
                }
            }
        }
    } else {
        // Here, we use maps and vectors to cache distances
        //     from x to y,u,v; from y to u,v; from u to v
        // This part is used only when the side of the matrices that can be stored in the cache is too small...
        std::unordered_map<std::pair<NodeID, NodeID>, Distance> dist;
        for (auto y: HDS.dominated_vertices(layer, yp)) {
            // Store distances from y to vertices dominated by up and vp
            if (hublab == nullptr) {
                for (auto u: HDS.dominated_vertices(layer, up)) { dist[{y, u}] = pll.QueryDistance(y, u); }
                for (auto v: HDS.dominated_vertices(layer, vp)) { dist[{y, v}] = pll.QueryDistance(y, v); }
            } else {
                for (auto u: HDS.dominated_vertices(layer, up)) { dist[{y, u}] = hublab->distance(y, u); }
                for (auto v: HDS.dominated_vertices(layer, vp)) { dist[{y, v}] = hublab->distance(y, v); }
            }
        }
        for (auto u: HDS.dominated_vertices(layer, up)) {
            if (hublab == nullptr) {
                for (auto v: HDS.dominated_vertices(layer, vp)) {
                    dist[{u, v}] = pll.QueryDistance(u, v);
                    dist[{v, u}] = dist[{u, v}];  // We sometimes exchange the role of u and v
                }
            } else {
                for (auto v: HDS.dominated_vertices(layer, vp)) {
                    dist[{u, v}] = hublab->distance(u, v);
                    dist[{v, u}] = dist[{u, v}];  // We sometimes exchange the role of u and v
                }
            }
        }

        Distances & dist_x = storage_of_distances[2 * layer + 1];
        for (auto x: HDS.dominated_vertices(layer, xp)) {
            // Store distances from x to vertices dominated by yp, up and vp
            if (hublab == nullptr) {
                for (auto y: HDS.dominated_vertices(layer, yp)) { dist_x[y] = pll.QueryDistance(x, y); }
                for (auto u: HDS.dominated_vertices(layer, up)) { dist_x[u] = pll.QueryDistance(x, u); }
                for (auto v: HDS.dominated_vertices(layer, vp)) { dist_x[v] = pll.QueryDistance(x, v); }
            } else {
                for (auto y: HDS.dominated_vertices(layer, yp)) { dist_x[y] = hublab->distance(x, y); }
                for (auto u: HDS.dominated_vertices(layer, up)) { dist_x[u] = hublab->distance(x, u); }
                for (auto v: HDS.dominated_vertices(layer, vp)) { dist_x[v] = hublab->distance(x, v); }
            }

            for (auto y: HDS.dominated_vertices(layer, yp)) {
                // Store distances from y to vertices dominated by up and vp
                auto dxy = dist_x[y];
                
                if (dxy + radius[x] + radius[y] <= h_lb) { continue; }
                
                // Check acceptable and valuable vertices in up and vp
                std::unordered_map<NodeID, NodeIDs> acceptable;
                std::unordered_map<NodeID, NodeIDs> valuable;
                for (auto zp: {up, vp}) {
                    for (auto u: HDS.dominated_vertices(layer, zp)) {
                        Distance dxu = dist_x[u];
                        Distance dyu = dist[{y, u}];
                        if ((2 * (dxu + radius[x] + radius[u]) > h_lb) and (2 * (dyu + radius[y] + radius[u]) > h_lb)) {
                            Distance detour = dxu + dyu - dxy;
                            Distance ecc_u = ecc[u];
                            Distance sum_radius = 2 * (2 * radius[u] + radius[x] + radius[y]);
                            Distance value = 2 * ecc_u + sum_radius;
                            if (value > 2 * h_lb + 2 + detour) {
                                auto mymax = 2 * std::max((dxu > radius[x] ? dxu - radius[x] : 0),
                                                          (dyu > radius[y] ? dyu - radius[y] : 0));
                                if (value + 2 * dxy >  mymax + 3 * h_lb + 3) {
                                    acceptable[zp].push_back(u);
                                    if (2 * dist_central[u] + sum_radius > h_lb + detour) {
                                        valuable[zp].push_back(u);
                                    }
                                }
                            }
                        }
                    }
                }

                // Explore quadruples with u valuable and v acceptable, and then with v valuable and u acceptable
                // We must prevent considering a pair twice
                std::unordered_set<NodePair> visited_pairs;
                std::vector<std::tuple<NodeID, NodeID>> order = {{up, vp}, {vp, up}};
                for (auto [ap, bp]: order) {
                    if ((valuable[ap].size() == 0) or (acceptable[bp].size() == 0)) {
                        continue;  // nothing to do
                    }
                    for (auto u: valuable[ap]) {
                        Distance dxu = dist_x[u];
                        Distance dyu = dist[{y, u}];
                        for (auto v: acceptable[bp]) {
                            NodePair p = (u < v ? std::make_pair(u, v) : std::make_pair(v, u));
                            if (visited_pairs.find(p) != visited_pairs.end()) {
                                continue;  // the pair has already been considered
                            } else {
                                visited_pairs.insert(p);
                            }
                            evaluated_quadruples[layer - 1]++;
                            Distance S1 = dxy + dist[{u, v}];
                            Distance S2 = dxu + dist[{y, v}];
                            if (S1 < S2) {
                                continue;
                            }
                            Distance S3 = dist_x[v] + dyu;
                            if (S1 < S3) {
                                continue;
                            }
                            //Distance delta = _tau_(S1, S2, S3);
                            Distance delta = S1 - (S2 > S3 ? S2 : S3);

                            if (delta > h_lb) {
                                h_lb = delta;
                                std::cout << "h_lb = " << h_lb <<" for "
                                          << u <<" "<< v <<" "<< x <<" "<< y
                                          << "\t" << mytimer << std::endl;
                            }
                            if ((layer > 1) and (delta + 2*(radius[u] + radius[v] + radius[x] + radius[y]) > h_lb)) {
                                compute_DOM_DFS_rec(HDS, layer - 1, u, v, x, y);
                                checked_quadruples[layer - 1]++;
                            }
                        }
                    }
                }
            }
        }
    }
}


// =====================================================================
// Cache of distance matrices
// =====================================================================



CacheOfDistanceMatrix::CacheOfDistanceMatrix(HierarchicalDominatingSet & HDS,
                                             PrunedLandmarkLabeling<> & pll,
                                             uint layer, int cache_size)
: HDS(HDS), pll(&pll), hublab(nullptr), use_pll(true), layer(layer), cache_of_matrices(cache_size)
{
    // We need a cache of size at least 12 (may be at least 7...)
    assert(cache_size >= 12);
    //std::cout << "cache size = " << cache_size << std::endl;
}

CacheOfDistanceMatrix::CacheOfDistanceMatrix(HierarchicalDominatingSet & HDS,
                                             HubLabeling * hublab,
                                             uint layer, int cache_size)
: HDS(HDS), pll(nullptr), hublab(hublab), use_pll(false), layer(layer), cache_of_matrices(cache_size)
{
    // We need a cache of size at least 12 (may be at least 7...)
    assert(cache_size >= 12);
    //std::cout << "cache size = " << cache_size << std::endl;
}

DistanceMatrix& CacheOfDistanceMatrix::get(NodeID ap, NodeID bp, bool both)
{
    if (cache_of_matrices.contains({ap, bp})) {
        return cache_of_matrices.get({ap, bp});
    } else {
        // We compute and return the requested distance matrix
        return make_matrices(ap, bp, both);
    }
}

DistanceMatrix& CacheOfDistanceMatrix::make_matrices(NodeID ap, NodeID bp, bool both)
{
    DistanceMatrix & A = cache_of_matrices.getNew({ap, bp});
    auto na = HDS.dominated_vertices_size(layer, ap);
    auto nb = HDS.dominated_vertices_size(layer, bp);
    A.resize(na, nb);
    std::size_t ai = 0;
    if (use_pll) {
        for (auto a: HDS.dominated_vertices(layer, ap)) {
            std::size_t bi = 0;
            for (auto b: HDS.dominated_vertices(layer, bp)) {
                A(ai, bi) = pll->QueryDistance(a, b);
                bi++;
            }
            ai++;
        }
    } else {
        for (auto a: HDS.dominated_vertices(layer, ap)) {
            std::size_t bi = 0;
            for (auto b: HDS.dominated_vertices(layer, bp)) {
                A(ai, bi) = hublab->distance(a, b);
                bi++;
            }
            ai++;
        }
    }

    // We also compute the matrix bp ap
    if (both) {
        DistanceMatrix & B = cache_of_matrices.getNew({bp, ap});
        B.resize(nb, na);
        
        for (std::size_t ai = 0; ai < HDS.dominated_vertices_size(layer, ap); ai ++) {
            for (std::size_t bi = 0; bi < HDS.dominated_vertices_size(layer, bp); bi++) {
                B(bi, ai) = A(ai, bi);
            }
        }
    }
    
    return A;
}

void CacheOfDistanceMatrix::lock(NodeID ap, NodeID bp, bool both)
{
    cache_of_matrices.lock({ap, bp});
    if (both) {
        cache_of_matrices.lock({bp, ap});
    }
}

void CacheOfDistanceMatrix::unlock(NodeID ap, NodeID bp, bool both)
{
    cache_of_matrices.unlock({ap, bp});
    if (both) {
        cache_of_matrices.unlock({bp, ap});
    }
}


std::ostream& operator<<(std::ostream& os, MyTimer& MT)
{
    auto current_time = std::chrono::high_resolution_clock::now();
    auto since_start = std::chrono::duration_cast<std::chrono::milliseconds>(current_time - MT.last_time);
    auto since_last = std::chrono::duration_cast<std::chrono::milliseconds>(current_time - MT.start_time);
    MT.last_time = current_time;
    //os << "[Time since last = " << since_start.count() << " | since start = " << since_last.count() << "]";
    os << "[Time: " << since_start.count() << " | " << since_last.count() << "]";
    return os;
}
