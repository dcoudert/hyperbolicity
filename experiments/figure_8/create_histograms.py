import glob
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

experiments_directory = "../output/"

hyp = {"BG-MV-Physical": 4.5, "BG-S-Affinity\_Capture-MS": 0, "BG-S-Affinity\_Capture-RNA": 2.0, "BG-S-Affinity\_Capture-Western": 4.0, "BG-S-Biochemical\_Activity": 3.0, "BG-S-Dosage\_Rescue": 4.0, "BG-S-Synthetic\_Growth\_Defect": 2.0, "BG-S-Synthetic\_Lethality": 2.0, "dip20170205": 4.5, "CAIDA\_as\_20000102": 2.5, "CAIDA\_as\_20040105": 2.5, "CAIDA\_as\_20050905": 3.0, "CAIDA\_as\_20110116": 2.0, "CAIDA\_as\_20120101": 0, "CAIDA\_as\_20130101": 2.5, "CAIDA\_as\_20131101": 2.5, "DIMES\_201012": 2.0, "DIMES\_201204": 2.0, "p2p-Gnutella09": 3.0, "gnutella31-d": 3.5, "notreDame-d": 8.0, "ca-CondMat": 3.5, "ca-HepPh": 3.0, "ca-HepTh": 4.0, "com-dblp.ungraph": 5.0, "dblp-2010": 5.5, "email-Enron": 2.5, "epinions1-d": 2.5, "facebook\_combined": 1.5, "loc-brightkite": 3.0, "loc-gowalla\_edges": 3.5, "slashdot0902-d": 2.5, "oregon2\_010331": 2.0, "t.FLA-w": 0, "buddha-w": 0, "froz-w": 0, "grid300-10": 280.0, "grid500-10": 463.0, "z-alue7065": 138.0}

def read_files_to_histograms(directory, suffix):
    histograms = []
    for filename in sorted(glob.glob(directory + "*" + suffix)):
        with open(filename) as file:
            graph_name = filename.split("/")[-1].split(".bcc")[0]
            graph_name = graph_name.replace("BIOGRID", "BG").replace("SYSTEM", "S").replace("_", "\_").replace("-3.4.145", "")
            if graph_name not in hyp:
                continue
            histogram = [graph_name, [], []]

            for line in file:
                if "status 124" in line:
                    continue
                elif "nb_pairs =" in line:
                    parts = line.split()
                    assert(len(parts) == 7 or len(parts) == 6)
                    if len(parts) == 7:
                        number_of_far_nodes = int(line.split()[2])
                        distance = int(line.split()[6])
                    else:
                        number_of_far_nodes = int(line.split()[1][1:])
                        distance = int(line.split()[5])
                    histogram[1].append(distance)
                    histogram[2].append(number_of_far_nodes)
                elif "appart pairs =" in line:
                    histograms.append(histogram)

    return histograms

def change_fonts():
    plt.rc('text', usetex=True)
    plt.rc('xtick', labelsize='18')
    plt.rc('ytick', labelsize='18')
    plt.rc('axes', labelsize='18')
    plt.rc('legend', fontsize='15')

def create_plot(histogram):
    plt.style.use('seaborn')
    change_fonts()
    fig, ax = plt.subplots()
    #  plt.title(histogram[0])
    plt.yscale('log')
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.bar(histogram[1], histogram[2])
    ax.set_ylim([.5, 2*max(histogram[2])])
    ax.fill_betweenx([.5, 2*max(histogram[2])], 2*hyp[histogram[0]] - .5, max(histogram[1])+.5, alpha=.3)
    ax.set_xlim([0, max(histogram[1])+.5])
    plt.tight_layout()
    plt.savefig("histograms/" + histogram[0].replace("\\", "") + ".pdf")
    plt.close()

def create_plots(histograms):
    for histogram in histograms:
        create_plot(histogram)

histograms = read_files_to_histograms(experiments_directory, "7.out")
create_plots(histograms)
