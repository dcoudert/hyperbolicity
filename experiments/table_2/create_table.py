import glob

experiments_directory = "../output/"

graph_order = ["BG-MV-Physical", "BG-S-Affinity\_Capture-MS", "BG-S-Affinity\_Capture-RNA", "BG-S-Affinity\_Capture-Western", "BG-S-Biochemical\_Activity", "BG-S-Dosage\_Rescue", "BG-S-Synthetic\_Growth\_Defect", "BG-S-Synthetic\_Lethality", "dip20170205", "CAIDA\_as\_20000102", "CAIDA\_as\_20040105", "CAIDA\_as\_20050905", "CAIDA\_as\_20110116", "CAIDA\_as\_20120101", "CAIDA\_as\_20130101", "CAIDA\_as\_20131101", "DIMES\_201012", "DIMES\_201204", "p2p-Gnutella09", "gnutella31-d", "notreDame-d", "ca-CondMat", "ca-HepPh", "ca-HepTh", "com-dblp.ungraph", "dblp-2010", "email-Enron", "epinions1-d", "facebook\_combined", "loc-brightkite", "loc-gowalla\_edges", "slashdot0902-d", "oregon2\_010331", "t.FLA-w", "buddha-w", "froz-w", "grid300-10", "grid500-10", "z-alue7065"]

# number_of_nodes = [ 9851, 17793, 3339, 9971, 2944, 1521, 3013, 2258, 13969, 4009, 4009, 10424, 12957, 23214, 25614, 27454, 29432, 18764, 16907, 5606, 33812, 134958, 17234, 9025, 5898, 211409, 140610, 20416, 36111, 3698, 33187, 137519, 51528, 7602, 691175, 543652, 749520, 90211, 250041, 34040 ]

hline_positions = ["dip20170205", "DIMES\_201204", "notreDame-d", "slashdot0902-d", "t.FLA-w"]

def sort_and_reduce(columns, graph_order):
    new_columns = []
    for graph in graph_order:
        for i in range(len(columns)):
            entry = columns[i]
            if entry[0] == graph:
                new_columns.append(entry)

    return new_columns

def to_size(size, decimal_places=2):
    if isinstance(size, str):
        return size
    for unit in ['KB', 'MB', 'GB', 'TB', 'PB']:
        if size < 1000.0 or unit == 'PiB':
            break
        size /= 1000.0
    return f"{size:.{decimal_places}f} {unit}"

def to_seconds(time_string):
    dot_split = time_string.split(".")
    assert(len(dot_split) == 1 or len(dot_split) == 2)

    seconds = sum(x * int(t) for x, t in zip([1, 60, 3600], reversed(dot_split[0].split(":"))))
    d_seconds = 0
    if len(dot_split) == 2:
        d_seconds = 0.01*int(dot_split[1])

    return seconds + d_seconds

def good_format(number, decimal_places=1):
    if isinstance(number, str):
        return number
    return f"{number:,.{decimal_places}f}".replace(",","\\,")

def get_graph_name(filename):
    graph_name = filename.split("/")[-1].split(".bcc")[0]
    graph_name = graph_name.replace("BIOGRID", "BG").replace("SYSTEM", "S").replace("_", "\_").replace("-3.4.145", "")
    return graph_name

def read_cache_usage_from_file(filename):
    with open(filename) as file:
        for line in file:
            if "Maximum resident" in line:
                return float(line.split()[-1])

def read_cache_usage(experiments_directory):
    cache_dict = {}

    for filename in sorted(glob.glob(experiments_directory + "*" + "bcc.3.prune.heur.c2.out")):
        cache_dict[get_graph_name(filename)] = read_cache_usage_from_file(filename)

    for filename in sorted(glob.glob(experiments_directory + "*" + ".bcc.3.out")):
        graph_name = get_graph_name(filename)
        if graph_name in cache_dict:
            # c1000_cache - c2_cache
            cache_dict[graph_name] *= -1
            cache_dict[graph_name] += read_cache_usage_from_file(filename)

    return cache_dict

def read_files_to_columns(directory, suffix):
    columns = []
    for filename in sorted(glob.glob(directory + "*" + suffix)):
        with open(filename) as file:
            entry = [get_graph_name(filename)]

            lb = "--"
            ub = "--"
            for line in file:
                if "#nodes" in line:
                    number_of_nodes = float(line.split()[2][:-1])
                elif "signal 11" in line or "alloc" in line:
                    entry.append("--")
                    entry.append("$\skull$")
                    break
                elif "status 124" in line:
                    entry.append("$\skull$")
                    entry.append("--")
                    break
                elif "Maximum resident" in line:
                    entry.append(float(line.split()[-1]))
                    #  entry.append(line.split()[-1])
                elif "Elapsed" in line:
                    seconds = round(to_seconds(line.split()[-1]),2)
                    entry.append(seconds)
                elif "h_lb" in line and "h_ub" in line:
                    sl = line.split()
                    lb = str(round(float(sl[sl.index("h_lb")+2])/2.,1))
                    ub = str(round(float(sl[sl.index("h_ub")+2])/2.,1))
                elif "hyperbolicity of the graph" in line:
                    lb = str(round(float(line.split()[-1]),1))
                    ub = lb
                elif "Error" in line:
                    entry = []
                    break

            assert(len(entry) == 0 or len(entry) == 3)
            if entry:
                ub_lb_for_both = True
                if ub_lb_for_both or suffix == ".bcc.3.out":
                    entry.append(lb)
                    entry.append(ub)
                columns.append(entry)

    return sort_and_reduce(columns, graph_order)

def better_than(x, y):
    S = "$\\skull$"
    D = "--"

    if x == S or x == D or y == D:
        return False
    if y == S:
        return True
    if x < y:
        return True
    else:
        return False

def print_table(columns_1, columns_3, cache_dict):
    # merge lower and upper bound into one cell
    for columns in [columns_1, columns_3]:
        for column in columns:
            if column[3] == column[4]:
                new_element = str(column[3])
                column.pop()
                column.pop()
                column.append(new_element)
            else:
                new_element = str("[" + str(column[3]) + ", " + str(column[4]) + "]")
                column.pop()
                column.pop()
                column.append(new_element)

    for column_1, column_3 in zip(columns_1, columns_3):
        assert(column_1[0] == column_3[0])
        graph_name = column_1[0]
        for i in range(len(column_1)):
            if i == 1:
                if better_than(column_1[i], column_3[i]):
                    print("\\textbf{" + good_format(column_1[i]) + "}", end=" & ")
                else:
                    print(good_format(column_1[i]), end=" & ")
            elif i == 2:
                if better_than(column_1[i], column_3[i]):
                    print("\\textbf{" + to_size(column_1[i]) + "}", end=" & ")
                else:
                    print(to_size(column_1[i]), end=" & ")
            else:
                print(to_size(column_1[i]), end=" & ")

        for i in range(1,len(column_3)):
            if i == 1:
                if better_than(column_3[i], column_1[i]):
                    print("\\textbf{" + good_format(column_3[i]) + "}", end=" & ")
                else:
                    print(good_format(column_3[i]), end=" & ")
            elif i == 2:
                if better_than(column_3[i], column_1[i]):
                    print("\\textbf{" + to_size(column_3[i]) + "}", end=" & ")
                else:
                    print(to_size(column_3[i]), end=" & ")
            else:
                print(to_size(column_3[i]), end=" & ")


        if isinstance(column_3[-2], str) and column_3[-2][0:2] == "--":
            print("--" + " \\\\")
        else:
            print(to_size(cache_dict[graph_name]) + " \\\\")

        if column_1[0] in hline_positions:
            print("\\hline")

columns_1 = read_files_to_columns(experiments_directory, ".bcc.1.out")
columns_3 = read_files_to_columns(experiments_directory, ".bcc.3.out")
cache_dict = read_cache_usage(experiments_directory)

print_table(columns_1, columns_3, cache_dict)
