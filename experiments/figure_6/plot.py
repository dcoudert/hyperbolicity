import matplotlib.pyplot as plt

experiments_directory = "../output/cache/"

graphs = ["loc-brightkite", "notreDame-d", "CAIDA_as_20130101", "dblp-2010"]
cache_sizes = [2, 10, 50, 100, 500, 1000, 5000, 10000]

def to_seconds(time_string):
    dot_split = time_string.split(".")
    assert(len(dot_split) == 1 or len(dot_split) == 2)

    seconds = sum(x * int(t) for x, t in zip([1, 60, 3600], reversed(dot_split[0].split(":"))))
    d_seconds = 0
    if len(dot_split) == 2:
        d_seconds = 0.01*int(dot_split[1])

    return seconds + d_seconds

def read_all_times():
    all_times = []

    for graph in graphs:
        graph_times = [graph]

        for cache_size in cache_sizes:
            filename = experiments_directory + graph + ".bcc.3.prune.c" + str(cache_size) + ".out"
            read_times(filename, graph_times)

        all_times.append(graph_times)

    return all_times

def read_times(filename, graph_times):
    with open(filename) as file:
        for line in file:
            if "Elapsed" in line:
                seconds = round(to_seconds(line.split()[-1]),2)
                graph_times.append(seconds)
                return

def change_fonts():
    plt.rc('text', usetex=True)
    plt.rc('xtick', labelsize='15')
    plt.rc('ytick', labelsize='15')
    plt.rc('axes', labelsize='18')
    plt.rc('legend', fontsize='15')

def plot(all_times):
    plt.style.use('seaborn')
    change_fonts()
    fig, ax = plt.subplots()

    symbols = ["o", "v", "s", "D"]
    for times, symbol in zip(all_times, symbols):
        graph_name = times[0]
        add(cache_sizes, times[1:], graph_name, graph_name + ".pdf", ax, symbol)

    plt.legend()
    plt.tight_layout()
    plt.savefig("time_vs_cache.pdf")

def add(X, Y, title, filename, ax, symbol):
    ax.plot(X, Y, "-" + symbol, alpha=0.9, label=title.replace("_", "\_"))
    ax.grid(True)
    plt.xscale('log')
    plt.xlabel('Cache Size')
    plt.ylabel('Time (s)')
    ax.set_ylim([0, 1.35*max(Y)])

all_times = read_all_times()
plot(all_times)
