import glob

graph_order = ["BG-MV-Physical", "BG-S-Affinity\_Capture-MS", "BG-S-Affinity\_Capture-RNA", "BG-S-Affinity\_Capture-Western", "BG-S-Biochemical\_Activity", "BG-S-Dosage\_Rescue", "BG-S-Synthetic\_Growth\_Defect", "BG-S-Synthetic\_Lethality", "dip20170205", "CAIDA\_as\_20000102", "CAIDA\_as\_20040105", "CAIDA\_as\_20050905", "CAIDA\_as\_20110116", "CAIDA\_as\_20120101", "CAIDA\_as\_20130101", "CAIDA\_as\_20131101", "DIMES\_201012", "DIMES\_201204", "p2p-Gnutella09", "gnutella31-d", "notreDame-d", "ca-CondMat", "ca-HepPh", "ca-HepTh", "com-dblp.ungraph", "dblp-2010", "email-Enron", "epinions1-d", "facebook\_combined", "loc-brightkite", "loc-gowalla\_edges", "slashdot0902-d", "oregon2\_010331", "t.FLA-w", "buddha-w", "froz-w", "grid300-10", "grid500-10", "z-alue7065"]

hline_positions = ["dip20170205", "DIMES\_201204", "notreDame-d", "slashdot0902-d", "t.FLA-w"]

def sort_and_reduce(columns, graph_order):
    new_columns = []
    for graph in graph_order:
        for i in range(len(columns)):
            entry = columns[i]
            if entry[0] == graph:
                new_columns.append(entry)

    return new_columns

def to_seconds(time_string):
    dot_split = time_string.split(".")
    assert(len(dot_split) == 1 or len(dot_split) == 2)

    seconds = sum(x * int(t) for x, t in zip([1, 60, 3600], reversed(dot_split[0].split(":"))))
    d_seconds = 0
    if len(dot_split) == 2:
        d_seconds = 0.01*int(dot_split[1])

    return seconds + d_seconds

def read_files_to_columns(directory, suffix):
    columns = []
    for filename in sorted(glob.glob(directory + "*" + suffix)):
        with open(filename) as file:
            graph_name = filename.split("/")[-1].split(".bcc")[0]
            graph_name = graph_name.replace("BIOGRID", "BG").replace("SYSTEM", "S").replace("_", "\_").replace("-3.4.145", "")
            entry = [graph_name]

            for line in file:
                #if "signal 11" in line or "alloc" in line:
                #    entry.append("$\skull$")
                #    break
                #elif "status 124" in line:
                #    entry.append("$\skull$")
                #    break
                #  elif "Maximum resident" in line:
                #      entry.append(to_size(float(line.split()[-1])))
                #      #  entry.append(line.split()[-1])
                if "Maximum resident set size" in line:
                    kbytes = int(line.split()[-1])
                    entry.append(kbytes)
                #  elif "h_lb" in line and "h_ub" in line:
                #      sl = line.split()
                #      lb = str(round(float(sl[sl.index("h_lb")+2])/2.,1))
                #      ub = str(round(float(sl[sl.index("h_ub")+2])/2.,1))
                #  elif "hyperbolicity of the graph" in line:
                #      lb = str(round(float(line.split()[-1]),1))
                #      ub = lb
                elif "Graph: #nodes" in line:
                    n = int(line.split()[2].replace(",", ""))
                    m = int(line.split()[-1])
                    entry.append(n)
                    entry.append(m)
                elif "Error" in line:
                    entry = []
                    break

            assert(len(entry) == 0 or len(entry) == 4)
            if entry:
                columns.append(entry)

    return sort_and_reduce(columns, graph_order)

def print_table(columns_3_0_0, columns_3_0_1, columns_3_1_0, columns_3_1_1_2c, columns_3_1_1):
    for c1, c2, c3, c4, c5 in zip(columns_3_0_0, columns_3_0_1, columns_3_1_0, columns_3_1_1_2c, columns_3_1_1):
        assert(c1[0] == c2[0] == c3[0] == c4[0])
        assert(c1[1] == c2[1] == c3[1] == c4[1])
        assert(c1[2] == c2[2] == c3[2] == c4[2])
        print(c1[0], end=" & ")
        for i in range(3,len(c1)):
            print(c1[i], end=" & ")

        for i in range(3,len(c2)):
            print(c2[i], end=" & ")

        for i in range(3,len(c3)):
            print(c3[i], end=" & ")

        for i in range(3,len(c4)):
            print(c4[i], end=" & ")

        for i in range(3,len(c5)):
            print(c5[i], end=" & ")

        for i in range(1,3):
            print(c1[i], end=" & ")
        n = c1[1]; m = c1[2]
        print(int((4*n*n)/1024), end=" & ") # the distance matrix is symmetric, but let it be
        print(str(int(4*(n+m)/1024)) + " \\\\")

        if c1[0] in hline_positions:
            print("\\hline")


columns_3_0_0 = read_files_to_columns("../output/", ".bcc.3.noprune.noheur.out")
columns_3_0_1 = read_files_to_columns("../output/", ".bcc.3.noprune.out")
columns_3_1_0 = read_files_to_columns("../output/", ".bcc.3.prune.noheur.out")
columns_3_1_1_2c = read_files_to_columns("../output/", ".bcc.3.prune.heur.c2.out")
columns_3_1_1 = read_files_to_columns("../output/", ".bcc.3.out")

print_table(columns_3_0_0, columns_3_0_1, columns_3_1_0, columns_3_1_1_2c, columns_3_1_1)
