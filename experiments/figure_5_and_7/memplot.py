import sys
import math
import numpy as np
import matplotlib.pyplot as plt

def change_fonts():
    plt.rc('text', usetex=True)
    plt.rc('xtick', labelsize='15')
    plt.rc('ytick', labelsize='15')
    plt.rc('axes', labelsize='18')


#data=np.loadtxt('table3.mat', dtype=float, comments='#', delimiter=' ')

def plot_match(data, i, iname, j, jname, multfact) :
    x = data[:,i] * multfact
    y = data[:,j]
    vmax = max(max(x), max(y))
    fg, ax = plt.subplots()
    ax.scatter(x, y, c='tab:green', alpha=0.9, zorder=2)
    ax.plot([0., 10*max(y)], [0., 10*max(y)], "k--", alpha=.5, zorder=1)
    #ax.plot([0., vmax], [0., 2.*vmax], "k--", alpha=.2, zorder=1)
    #ax.plot([0., vmax], [0., vmax/2.], "k--", alpha=.2, zorder=1)
    #ax.plot([0., max(x)], [0., max(x)/10.], "k--", alpha=.2, zorder=1)
    ax.set_xlabel(iname)
    ax.set_ylabel(jname)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim([min(y)/2, 2*max(x)])
    #ax.set_ylim([min(y)/2, 2*max(y)])
    ax.set_aspect('equal')
    #ticks = [0, 5000, 10000, 15000, 21600]
    #plt.axes().set_xticks(ticks)
    #plt.axes().set_yticks(ticks)

#plot_match(data, 0, "none", 1, "prune"); plt.show()

data=np.loadtxt(sys.argv[1], dtype=float, comments='#', delimiter=' ')

i, j = int(sys.argv[2]), int(sys.argv[3])
name = ["cache", "cache+heur", "cache+prune", "prune+heur", "cache+prune+heur", "n", "m", "$n^2$", "graph size * 100"]
name = ["cache", "cache+heur", "cache+prune", "our memory usage in KB (cache size $c=2$)", "cache+prune+heur", "n", "m", "memory usage of distance matrix in KB", "graph size * 100"]
factor= [1.,     1.,            1.,           1.,           1.,                  1.,  1., 1.,         100]

labelfig = sys.argv[4]

plt.style.use('seaborn')
#plt.title("Time Comparison")
change_fonts()

plot_match(data, i, labelfig+" "+name[i], j, name[j], factor[i])

plt.tight_layout()
plt.savefig(sys.stdout.buffer, format='pdf',bbox_inches='tight')
plt.close()

# heur : no sensible gain, but no sensible cost
# cache : often marginal gain, rarely marginal loss in time, but significant loss in space !
# prune : important gain in several cases

# keep 21600 points for skulls ?

