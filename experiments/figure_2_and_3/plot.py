import math
import numpy as np
import matplotlib.pyplot as plt

def change_fonts():
    plt.rc('text', usetex=True)
    plt.rc('xtick', labelsize='15')
    plt.rc('ytick', labelsize='15')
    plt.rc('axes', labelsize='18')

def plot(X, Y, title, filename, min_val = .1, line_regression = False):
    plt.style.use('seaborn')
    plt.title(title)
    change_fonts()
    fig, ax = plt.subplots()

    # sort X and Y according to X
    X, Y = (list(t) for t in zip(*sorted(zip(X,Y))))
    ax.scatter(X, Y, c='tab:green', alpha=0.9)

    ax.grid(True)
    #  plt.axis('sqaure')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel('Borassi et al.')
    plt.ylabel('Our Algorithm')
    ax.set_aspect('equal')
    ax.set_xlim([min_val, 2*max(X)])
    ax.set_ylim([min_val, 2*max(X)])
    max_value = 2*max(max(X), max(Y))
    ax.plot([0., max_value], [0., max_value] , "k--", alpha=.5)

    if line_regression:
        # regression of points in log space
        X_log, Y_log = np.log2(X), np.log2(Y)
        A = np.polyfit(X_log, Y_log, 1)
        R = [-100, math.log2(max_value)]
        ax.plot(2**np.array(R), [2**(A[1] + x*A[0]) for x in R], "-", c='tab:green', alpha=0.8)
        print("Linear regression result: 2^{" + str(A[1]) + " + " + str(A[0]) + " * x}")

    plt.tight_layout()
    plt.savefig(filename)

def read_table(filename):
    X1, Y1, X2, Y2 = [], [], [], []
    with open(filename) as file:
        for line in file:
            if any(s in line for s in ["skull", "hline"]):
                continue
            _, x1, x2, y1, y2 = line.split("&")
            X1.append(float(x1.strip()))
            X2.append(int(x2.strip()))
            Y1.append(float(y1.strip()))
            Y2.append(int(y2.strip().split()[0]))

    return X1, Y1, X2, Y2

Alg1_time, Alg3_time, Alg1_mem, Alg3_mem = read_table("table.txt")
plot(Alg1_time, Alg3_time, "Time Comparison", "time_scatter.pdf", .1)
plot(Alg1_mem, Alg3_mem, "Memory Comparison", "mem_scatter.pdf", 10**4, True)
