import glob

experiments_directory = "../output/"

graph_order = ["BG-MV-Physical", "BG-S-Affinity\_Capture-MS", "BG-S-Affinity\_Capture-RNA", "BG-S-Affinity\_Capture-Western", "BG-S-Biochemical\_Activity", "BG-S-Dosage\_Rescue", "BG-S-Synthetic\_Growth\_Defect", "BG-S-Synthetic\_Lethality", "dip20170205", "CAIDA\_as\_20000102", "CAIDA\_as\_20040105", "CAIDA\_as\_20050905", "CAIDA\_as\_20110116", "CAIDA\_as\_20120101", "CAIDA\_as\_20130101", "CAIDA\_as\_20131101", "DIMES\_201012", "DIMES\_201204", "p2p-Gnutella09", "gnutella31-d", "notreDame-d", "ca-CondMat", "ca-HepPh", "ca-HepTh", "com-dblp.ungraph", "dblp-2010", "email-Enron", "epinions1-d", "facebook\_combined", "loc-brightkite", "loc-gowalla\_edges", "slashdot0902-d", "oregon2\_010331", "t.FLA-w", "buddha-w", "froz-w", "grid300-10", "grid500-10", "z-alue7065"]
hyp = {"BG-MV-Physical": 4.5, "BG-S-Affinity\_Capture-MS": 0, "BG-S-Affinity\_Capture-RNA": 2.0, "BG-S-Affinity\_Capture-Western": 4.0, "BG-S-Biochemical\_Activity": 3.0, "BG-S-Dosage\_Rescue": 4.0, "BG-S-Synthetic\_Growth\_Defect": 2.0, "BG-S-Synthetic\_Lethality": 2.0, "dip20170205": 4.5, "CAIDA\_as\_20000102": 2.5, "CAIDA\_as\_20040105": 2.5, "CAIDA\_as\_20050905": 3.0, "CAIDA\_as\_20110116": 2.0, "CAIDA\_as\_20120101": 0, "CAIDA\_as\_20130101": 2.5, "CAIDA\_as\_20131101": 2.5, "DIMES\_201012": 2.0, "DIMES\_201204": 2.0, "p2p-Gnutella09": 3.0, "gnutella31-d": 3.5, "notreDame-d": 8.0, "ca-CondMat": 3.5, "ca-HepPh": 3.0, "ca-HepTh": 4.0, "com-dblp.ungraph": 5.0, "dblp-2010": 5.5, "email-Enron": 2.5, "epinions1-d": 2.5, "facebook\_combined": 1.5, "loc-brightkite": 3.0, "loc-gowalla\_edges": 3.5, "slashdot0902-d": 2.5, "oregon2\_010331": 2.0, "t.FLA-w": 0, "buddha-w": 0, "froz-w": 0, "grid300-10": 280.0, "grid500-10": 463.0, "z-alue7065": 138.0}

hline_positions = ["dip20170205", "DIMES\_201204", "notreDame-d", "slashdot0902-d", "t.FLA-w"]

def sort_and_reduce(columns, graph_order):
    new_columns = []
    for graph in graph_order:
        for i in range(len(columns)):
            entry = columns[i]
            if entry[0] == graph:
                new_columns.append(entry)

    return new_columns

def to_size(size, decimal_places=2):
    for unit in ['KB', 'MB', 'GB', 'TB', 'PB']:
        if size < 1000.0 or unit == 'PiB':
            break
        size /= 1000.0
    return f"{size:.{decimal_places}f} {unit}"

def to_seconds(time_string):
    dot_split = time_string.split(".")
    assert(len(dot_split) == 1 or len(dot_split) == 2)

    seconds = sum(x * int(t) for x, t in zip([1, 60, 3600], reversed(dot_split[0].split(":"))))
    d_seconds = 0
    if len(dot_split) == 2:
        d_seconds = 0.01*int(dot_split[1])

    return seconds + d_seconds

def good_format(number, decimal_places=1):
    return f"{number:,.{decimal_places}f}".replace(",","\\,")

def read_file_to_columns(directory, suffix):
    columns = []
    for filename in sorted(glob.glob(directory + "*" + suffix)):
        with open(filename) as file:
            graph_name = filename.split("/")[-1].split(".bcc")[0]
            graph_name = graph_name.replace("BIOGRID", "BG").replace("SYSTEM", "S").replace("_", "\_").replace("-3.4.145", "")
            if graph_name not in graph_order:
                continue
            entry = [graph_name]
            pairs_to_consider = 0

            for line in file:
                if "#nodes" in line:
                    number_of_nodes = float(line.split()[2][:-1])
                elif "signal 11" in line or "alloc" in line:
                    entry.append("--")
                    entry.append("--")
                    entry.append("--")
                    entry.append("$\skull$")
                    break
                elif "status 124" in line:
                    entry.append("--")
                    entry.append("--")
                    entry.append("$\skull$")
                    entry.append("--")
                    break
                elif "nb_pairs =" in line:
                    parts = line.split()
                    assert(len(parts) == 7 or len(parts) == 6)
                    if len(parts) == 7:
                        number_of_far_nodes = int(line.split()[2])
                        distance = int(line.split()[6])
                    else:
                        number_of_far_nodes = int(line.split()[1][1:])
                        distance = int(line.split()[5])
                    if distance >= 2*hyp[graph_name]:
                        pairs_to_consider += number_of_far_nodes
                elif "Maximum resident" in line:
                    entry.append(to_size(float(line.split()[-1])))
                elif "Elapsed" in line:
                    seconds = round(to_seconds(line.split()[-1]),2)
                    entry.append(good_format(seconds))
                elif "appart pairs =" in line:
                    entry.append(round(100*2*float(line.split()[-1])/(number_of_nodes*(number_of_nodes-1)), 2))
                    if hyp[graph_name] != 0:
                        entry.append(good_format(round(100*2*pairs_to_consider/(number_of_nodes*(number_of_nodes-1)), 3), 3))
                    else:
                        entry.append("--")
                elif "Error" in line:
                    entry = []
                    break

            assert(len(entry) == 0 or len(entry) == 5)
            if entry:
                columns.append(entry)

    return sort_and_reduce(columns, graph_order)

def print_table(columns):
    for column in columns:
        column = column[0], column[3], column[4], column[1], column[2]
        for i in range(len(column)-1):
            print(column[i], end=" & ")
        print(str(column[-1]) + " \\\\")

        if column[0] in hline_positions:
            print("\\hline")

columns = read_file_to_columns(experiments_directory, "7.out")
print_table(columns)
