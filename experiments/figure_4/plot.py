import glob
import math
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

experiments_directory = "../output/"

graph_order = ["BG-MV-Physical", "BG-S-Affinity\_Capture-MS", "BG-S-Affinity\_Capture-RNA", "BG-S-Affinity\_Capture-Western", "BG-S-Biochemical\_Activity", "BG-S-Dosage\_Rescue", "BG-S-Synthetic\_Growth\_Defect", "BG-S-Synthetic\_Lethality", "dip20170205", "CAIDA\_as\_20000102", "CAIDA\_as\_20040105", "CAIDA\_as\_20050905", "CAIDA\_as\_20110116", "CAIDA\_as\_20120101", "CAIDA\_as\_20130101", "CAIDA\_as\_20131101", "DIMES\_201012", "DIMES\_201204", "p2p-Gnutella09", "gnutella31-d", "notreDame-d", "ca-CondMat", "ca-HepPh", "ca-HepTh", "com-dblp.ungraph", "dblp-2010", "email-Enron", "epinions1-d", "facebook\_combined", "loc-brightkite", "loc-gowalla\_edges", "slashdot0902-d", "oregon2\_010331", "t.FLA-w", "buddha-w", "froz-w", "grid300-10", "grid500-10", "z-alue7065"]

graphs = []

def change_fonts():
    plt.rc('text', usetex=True)
    plt.rc('xtick', labelsize='15')
    plt.rc('ytick', labelsize='15')
    plt.rc('axes', labelsize='18')

def sort_and_reduce(columns, graph_order):
    new_columns = []
    for graph in graph_order:
        for i in range(len(columns)):
            entry = columns[i]
            if entry[0] == graph:
                graphs.append(entry[0])
                new_columns.append(np.array(entry[1:]))

    M = np.column_stack(new_columns).T
    S = M.sum(axis=1)
    return 100*M/S[:, np.newaxis]

# read matrix
def read_times(directory):
    columns = []
    for filename in sorted(glob.glob(directory + "*.bcc.3.out")):
        with open(filename) as file:
            graph_name = filename.split("/")[-1].split(".bcc")[0]
            graph_name = graph_name.replace("BIOGRID", "BG").replace("SYSTEM", "S").replace("_", "\_").replace("-3.4.145", "")
            entry = [graph_name]

            for line in file:
                if "sum = " in line and "COMPLETE" not in line:
                    entry.append(float(line.split()[-6]))

            assert(len(entry) == 1 or len(entry) == 6)
            if len(entry) == 6:
                columns.append(entry)

    return sort_and_reduce(columns, graph_order)

def consistent_rounding(arr,digits=1):
    rounded_arr = np.floor(arr*pow(10,digits))/pow(10,digits)
    hundred_threshold = 100.-1./pow(10,digits) + 5./pow(10,digits+1)
    while sum(rounded_arr) < hundred_threshold:
        rounded_arr[np.argmax(arr-rounded_arr)] += 1./pow(10,digits)

    return rounded_arr

def plot(times):
    change_fonts()
    
    fig, ax = plt.subplots(figsize=(9,9))
    im = ax.imshow(times, aspect = 'auto', vmin=0., vmax=100., cmap = "YlOrRd")

    # division lines
    y_division = [7.5,15.5,18.5,29.5,30.5]
    for y in y_division:
        plt.axhline(y=y, color='black', lw=1)

    # parts of the algorithm measured
    parts = ["initialize", "next pair", "$(d_x, d_y)$-BFS", "computeAccVal", "update $\delta_L$"]
    
    # Show all ticks and label them with the respective list entries
    #ax.set_xticks(np.arange(len(parts)), labels=parts)
    #ax.set_yticks(np.arange(len(graphs)), labels=graphs)
    ax.set_xticks(np.arange(len(parts)))
    ax.set_xticklabels(parts)
    ax.set_yticks(np.arange(len(graphs)))
    ax.set_yticklabels(graphs)
    
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
    
    # labels of cells
    for i in range(len(graphs)):
        times[i,:] = consistent_rounding(times[i,:])
        for j in range(len(parts)):
            percent = f"{times[i, j]:,.{1}f}\\%"
            text = ax.text(j, i, percent, ha="center", va="center", color="black")

    # legend
    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel("running time (\\%)", rotation=-90, va="bottom")
    
    #  ax.set_title("timing of parts of algorithm")
    fig.tight_layout()
    # plt.show()
    plt.savefig("time_parts.pdf", bbox_inches='tight')

times = read_times(experiments_directory)
plot(times)
