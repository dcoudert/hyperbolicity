import glob

experiments_directory = "../output/"

graph_order = ["BG-MV-Physical", "BG-S-Affinity\_Capture-MS", "BG-S-Affinity\_Capture-RNA", "BG-S-Affinity\_Capture-Western", "BG-S-Biochemical\_Activity", "BG-S-Dosage\_Rescue", "BG-S-Synthetic\_Growth\_Defect", "BG-S-Synthetic\_Lethality", "dip20170205", "CAIDA\_as\_20000102", "CAIDA\_as\_20040105", "CAIDA\_as\_20050905", "CAIDA\_as\_20110116", "CAIDA\_as\_20120101", "CAIDA\_as\_20130101", "CAIDA\_as\_20131101", "DIMES\_201012", "DIMES\_201204", "p2p-Gnutella09", "gnutella31-d", "notreDame-d", "ca-CondMat", "ca-HepPh", "ca-HepTh", "com-dblp.ungraph", "dblp-2010", "email-Enron", "epinions1-d", "facebook\_combined", "loc-brightkite", "loc-gowalla\_edges", "slashdot0902-d", "oregon2\_010331", "t.FLA-w", "buddha-w", "froz-w", "grid300-10", "grid500-10", "z-alue7065"]

hline_positions = ["dip20170205", "DIMES\_201204", "notreDame-d", "slashdot0902-d", "t.FLA-w"]

def sort_and_reduce(columns, graph_order):
    new_columns = []
    for graph in graph_order:
        for i in range(len(columns)):
            entry = columns[i]
            if entry[0] == graph:
                new_columns.append(entry)

    return new_columns

def read_files_to_columns(directory, suffix):
    columns = []
    for filename in sorted(glob.glob(directory + "*" + suffix)):
        with open(filename) as file:
            graph_name = filename.split("/")[-1].split(".bcc")[0]
            graph_name = graph_name.replace("BIOGRID", "BG").replace("SYSTEM", "S").replace("_", "\_").replace("-3.4.145", "")
            entry = [graph_name]

            for line in file:
                if "#nodes" in line:
                    number_of_nodes = line.split()[2][:-1]
                    number_of_edges = line.split()[4]
                    entry.append(number_of_nodes)
                    entry.append(number_of_edges)
                elif "radius" in line or "diameter" in line:
                    entry.append(line.split()[-1])
                elif "eccentricity" in line:
                    value = float(line.split()[-1])
                    entry.append(f"{value:.{2}f}")
                elif "Error" in line:
                    entry = []
                    break

            assert(len(entry) == 6)
            columns.append(entry)

    return sort_and_reduce(columns, graph_order)

def print_table(columns):
    for column in columns:
        column[4], column[5] = column[5], column[4]
        for i in range(0,len(column)-1):
            print(column[i], end=" & ")
        print(column[-1] + " \\\\")

        if column[0] in hline_positions:
            print("\\hline")

columns = read_files_to_columns(experiments_directory, ".bcc.1.out")

print_table(columns)
