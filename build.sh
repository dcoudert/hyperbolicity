#!/bin/sh
mkdir -p build
cd build
ln -sf ../graphs
ln -sf ../unit_test_data
cmake ..
make -j
