cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
project(hyperbolicity CXX)

enable_testing()

include(CMakeDetermineCXXCompiler)


message(STATUS "  Build type ${CMAKE_BUILD_TYPE}")
message(STATUS "  Compiler ID : ${CMAKE_CXX_COMPILER_ID}")


# default to RelWithDebInfo
if("${CMAKE_BUILD_TYPE}" STREQUAL "")
	set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose the type of build, options are: None(CMAKE_CXX_FLAGS or CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel." FORCE)
endif()

if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
	set(EXTRA_CXX_FLAGS "-Wall -Wextra -Wno-unused-parameter -pedantic" CACHE STRING "Extra flags used by the compiler during all build types.")
	set(EXTRA_EXE_LINKER_FLAGS "" CACHE STRING "Extra flags used by the linker.")
	set(EXTRA_EXE_LINKER_FLAGS_RELEASE "-flto" CACHE STRING "Extra flags used by the linker for the Release build type.")
	set(EXTRA_EXE_LINKER_FLAGS_RELWITHDEBINFO "-flto" CACHE STRING "Extra flags used by the linker for the RelWithDebInfo build type.")
else()
	set(EXTRA_CXX_FLAGS "" CACHE STRING "Extra flags used by the compiler during all build types.")
	set(EXTRA_EXE_LINKER_FLAGS "" CACHE STRING "Extra flags used by the linker.")
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${EXTRA_CXX_FLAGS} -std=c++17 -fpermissive -pthread")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${EXTRA_EXE_LINKER_FLAGS} -pthread")
set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} ${EXTRA_EXE_LINKER_FLAGS_RELEASE}")
set(CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO "${CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO} ${EXTRA_EXE_LINKER_FLAGS_RELWITHDEBINFO}")

option(VERBOSE "Verbose logging" OFF)
if(NOT VERBOSE)
	add_definitions(-DNVERBOSE)
endif()

# 0: BFSCacheBase
# 1: BFSCacheReset
# 2: BFSCacheCounter
# 3: BFSCacheCounterStruct
add_definitions(-DBFS_INFO_TYPE=0)

include_directories(SYSTEM contraction-hierarchies/src/)
include_directories(SYSTEM hub-labeling/src/)

add_library(common OBJECT
	src/algorithms.cpp
	src/basic_types.cpp
        src/bfs_info.cpp
	src/graph.cpp
	src/hyperbolicity.cpp
	src/hyperbolicity_dom.cpp
	src/leanness.cpp
	src/io.cpp
	src/naive_iterator.cpp
	src/far_apart_iterator.cpp
)

add_executable(main
	src/main.cpp
	$<TARGET_OBJECTS:common>
)

add_executable(leanness
	src/main_leanness.cpp
	$<TARGET_OBJECTS:common>
)

add_executable(run_benchmarks
	src/run_benchmarks.cpp
	$<TARGET_OBJECTS:common>
)

add_executable(run_tests
	src/run_tests.cpp
	src/unit_tests.cpp
	$<TARGET_OBJECTS:common>
)

add_test(NAME unit-test
	WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/src"
	COMMAND $<TARGET_FILE:run_tests>
)
