"""
Methods to extract data from outut files and analyse these data
"""
import re
import glob

import os
from collections import Counter
import matplotlib.pyplot as plt


def to_size(size, decimal_places=1):
    if isinstance(size, str):
        return size
    for unit in ['KB', 'MB', 'GB', 'TB', 'PB']:
        if size < 1000.0 or unit == 'PiB':
            break
        size /= 1000.0
    return f"{size:.{decimal_places}f} {unit}"


def to_seconds(time_string):
    dot_split = time_string.split(".")
    assert(len(dot_split) == 1 or len(dot_split) == 2)

    seconds = sum(x * int(t) for x, t in zip([1, 60, 3600], reversed(dot_split[0].split(":"))))
    d_seconds = 0
    if len(dot_split) == 2:
        d_seconds = 0.01*int(dot_split[1])

    return seconds + d_seconds



def parse_outfile(filename):
    r"""
    """
    res = {'filename': filename}
    with open(filename, 'r') as f:
        for line in f:
            line = line.strip()

            if (line.startswith('===') or line.startswith('Compute')
                    or line.startswith('index') or line.startswith('bit')
                    or line.startswith('done')):
                continue
            elif line.startswith('average'):
                res['avg_label_size'] = RR(re.findall(r'\d+.\d+', line)[0])
            elif line.startswith('k'):
                res = {algo: {} for algo in ['LMB', 'LMI', 'LMD', 'LHB', 'LHI', 'LHD']}
            elif 'LMB' not in res:
                raise ValueError('this should not happen')

            else:  # should a line of entries
                k, i, LMb, LMi, LMD, LHb, LHi, LHD = [int(x) for x in re.findall(r'\d+', line)]
                for algo, val in zip(['LMB', 'LMI', 'LMD', 'LHB', 'LHI', 'LHD'], [LMb, LMi, LMD, LHb, LHi, LHD]):
                    if k in res[algo]:
                        res[algo][k].append(val)
                    else:
                        res[algo][k] = [val]

    return res


def get_data(filename):
    from numpy import mean, median

    res = parse_outfile(filename)
    data = {}
    for key, val in res.items():
        if key.startswith('file') or key.startswith('avg'):
            data[key] = val
        else:
            k_values = sorted(val.keys())
            data[key] = {'k': k_values,
                         'min': [min(val[k])/1000 for k in k_values],
                         'mean': [mean(val[k])/1000 for k in k_values],
                         'median': [median(val[k])/1000 for k in k_values],
                         'max': [max(val[k])/1000 for k in k_values]}

    return data


def change_fonts():
    plt.rc('text', usetex=True)
    plt.rc('xtick', labelsize='16')
    plt.rc('ytick', labelsize='16')
    plt.rc('axes', labelsize='20') # 18
    plt.rc('legend', fontsize='16') # 14


algo_symbol = {'LMB': 'o',
               'LMI': 's',
               'LMD': 'd',
               'LHB': 'D',
               'LHI': '*',
               'LHD': 'P'}

algo_color = {'LMB': 'tab:blue',
              'LMI': 'tab:red',
              'LMD': 'tab:orange',
              'LHB': 'tab:green',
              'LHI': 'tab:brown',
              'LHD': 'tab:purple'}

algo_name = {'LMB': 'M-basic',
              'LMI': 'M-iFUB',
              'LMD': 'M-DHV',
              'LHB': 'H-basic',
              'LHI': 'H-iFUB',
              'LHD': 'H-DHV'}


def my_plot(source_file, dest_file):
    r"""
    """
    data = get_data(source_file)

    # plt.style.use('seaborn')  # deprecated
    plt.clf()
    fig, ax = plt.subplots(figsize=(12,6))
    ax.grid(True)
    plt.xlabel('size of slices')
    plt.ylabel('running time ($\mu s$)')
    plt.yscale('log')
    ymax = 0
    for algo in algo_name:
        ax.plot(data[algo]['k'], data[algo]['mean'], '-' + algo_symbol[algo], c=algo_color[algo],
                alpha=0.9, label=algo_name[algo])
        ymax = max(ymax, max(data[algo]['mean']))

    ax.set_ylim([0, 1.1*ymax])
    ax.set_xlim([0, max(data['LMB']['k'])])
    plt.legend()
    change_fonts()
    plt.tight_layout()
    plt.savefig(dest_file)



def make_plots(source_dir, dest_dir, fig_type='pdf'):

    suffix = '.diam.out'
    for filename in sorted(glob.glob(source_dir + "*" + suffix)):
        name = filename.split('/')[-1]
        dest_file = dest_dir + '/' + name.replace(suffix, f".{fig_type}")
        print(f"plot '{name}': '{filename}' -> '{dest_file}'")
        my_plot(filename, dest_file)

