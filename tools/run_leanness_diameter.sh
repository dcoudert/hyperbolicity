#!/bin/bash

# running time and memory limits
time_limit="10h"

if [[ $(uname -p) == 'arm' ]]; then
    # Apple M1
    TIME_OPTIONS="-l"
    memory_limit=""
else
    # Intel
    TIME_OPTIONS="-v timeout $time_limit"    
    memory_limit="62000000"
fi

LINE='=========================='

TOOLS_DIRECTORY="."
LEANNESS="$TOOLS_DIRECTORY/../build/leanness"
GRAPHS_DIRECTORY="$TOOLS_DIRECTORY/../graphs"
# Create output directories if not already done
OUTDIR="$TOOLS_DIRECTORY/../experiments/leanness/diameter"
TMPDIR="$TOOLS_DIRECTORY/../experiments/leanness/diameter/tmp"
mkdir -p "$TMPDIR"

# Run algorithm on
# file $1
# with arguments $2
# and save in file with extension $3
function run_on_file {
    BASENAME=$(basename "$1" .edgelist)
    DESTFILE=$(basename "$BASENAME" .txt).$3
    if [ -f $OUTDIR/$DESTFILE ]; then
	echo "File $DESTFILE exists. Remove to rerun experiments."
    else
	TMPFILE=$(mktemp $OUTDIR/tmp/blop.XXXX)
	echo "run algorithm with arguments $2 on $1 -> $TMPFILE -> $OUTDIR/$DESTFILE"
	(ulimit -Sv $memory_limit && echo $LINE && /usr/bin/time $TIME_OPTIONS $LEANNESS $GRAPHS_DIRECTORY/$1 $2) > $TMPFILE
	mv $TMPFILE $OUTDIR/$DESTFILE
    fi
}


# list of graphs

SMALL_GRAPHS="
BIOGRID-MV-Physical-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Affinity_Capture-MS-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Affinity_Capture-RNA-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Affinity_Capture-Western-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Biochemical_Activity-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Dosage_Rescue-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Synthetic_Growth_Defect-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Synthetic_Lethality-3.4.145.bcc.edgelist
CAIDA_as_20000102.bcc.edgelist
CAIDA_as_20040105.bcc.edgelist
CAIDA_as_20050905.bcc.edgelist
CAIDA_as_20110116.bcc.edgelist
CAIDA_as_20120101.bcc.edgelist
CAIDA_as_20130101.bcc.edgelist
CAIDA_as_20131101.bcc.edgelist
DIMES_201012.bcc.edgelist
DIMES_201204.bcc.edgelist
ca-CondMat.bcc.edgelist
ca-HepPh.bcc.edgelist
ca-HepTh.bcc.edgelist
dip20170205.bcc.edgelist
loc-brightkite.bcc.edgelist
p2p-Gnutella09.bcc.edgelist
"

SNAP_GRAPHS="
as20000102.bcc.txt
email-Enron.bcc.txt
facebook_combined.bcc.txt
oregon2_010331.bcc.txt
z-alue7065.bcc.txt
"
# loc-gowalla_edges.bcc.txt

LARGE_GRAPHS="
epinions1-d.bcc.txt
gnutella31-d.bcc.txt
slashdot0902-d.bcc.txt
"
# grid300-10.bcc.txt
# grid500-10.bcc.txt
# dblp-2010.bcc.txt
# notreDame-d.bcc.txt


MASSIVE_GRAPHS="
y-BerkStan-d.bcc.txt
buddha-w.bcc.txt
froz-w.bcc.txt
t.FLA-w.bcc.txt
"


# Run experiment on diameter on all graphs
for file in $SMALL_GRAPHS $SNAP_GRAPHS $LARGE_GRAPHS; do
    echo "=========================="
    # Algorithm 1 (MDG)
    run_on_file "$file" "-diameter" "diam.out"
done



