#!/bin/bash

# running time and memory limits
time_limit="6h"
memory_limit="192000000"

TOOLS_DIRECTORY="."
GRAPHS_DIRECTORY="$TOOLS_DIRECTORY/../graphs"
# Create output directories if not already done
OUTDIR="$TOOLS_DIRECTORY/../experiments/output"
TMPDIR="$TOOLS_DIRECTORY/../experiments/output/tmp"
mkdir -p "$TMPDIR"

# Run algorithm on
# file $1
# with arguments $2
# and same in file with extension $3
function run_on_file {
    TMPFILE=$(mktemp $OUTDIR/tmp/blop.XXXX)
    BASENAME=$(basename "$1" .edgelist)
    DESTFILE=$(basename "$BASENAME" .txt).$3
    echo "run algorithm with arguments $2 on $1 -> $TMPFILE -> $OUTDIR/$DESTFILE"
    (ulimit -Sv $memory_limit && lscpu && echo '==========================' && lsmem && echo '==========================' && /usr/bin/time -v timeout $time_limit $TOOLS_DIRECTORY/../build/main $GRAPHS_DIRECTORY/$1 $2) > $TMPFILE 2>&1
    mv $TMPFILE $OUTDIR/$DESTFILE
}


# list of graphs

SMALL_GRAPHS="
BIOGRID-MV-Physical-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Affinity_Capture-MS-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Affinity_Capture-RNA-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Affinity_Capture-Western-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Biochemical_Activity-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Dosage_Rescue-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Synthetic_Growth_Defect-3.4.145.bcc.edgelist
BIOGRID-SYSTEM-Synthetic_Lethality-3.4.145.bcc.edgelist
CAIDA_as_20000102.bcc.edgelist
CAIDA_as_20040105.bcc.edgelist
CAIDA_as_20050905.bcc.edgelist
CAIDA_as_20110116.bcc.edgelist
CAIDA_as_20120101.bcc.edgelist
CAIDA_as_20130101.bcc.edgelist
CAIDA_as_20131101.bcc.edgelist
DIMES_201012.bcc.edgelist
DIMES_201204.bcc.edgelist
ca-CondMat.bcc.edgelist
ca-HepPh.bcc.edgelist
ca-HepTh.bcc.edgelist
dip20170205.bcc.edgelist
loc-brightkite.bcc.edgelist
p2p-Gnutella09.bcc.edgelist
"

SNAP_GRAPHS="
as-skitter.bcc.txt
as20000102.bcc.txt
email-Enron.bcc.txt
facebook_combined.bcc.txt
loc-gowalla_edges.bcc.txt
oregon1_010331.bcc.txt
oregon2_010331.bcc.txt
web-Google.bcc.txt
web-Google.txt
web-Stanford.bcc.txt
roadNet-CA.bcc.txt
roadNet-PA.bcc.txt
roadNet-TX.bcc.txt
"

MASSIVE_GRAPHS="
buddha-w.bcc.txt
dblp-2010.bcc.txt
epinions1-d.bcc.txt
froz-w.bcc.txt
gnutella31-d.bcc.txt
grid300-10.bcc.txt
grid500-10.bcc.txt
notreDame-d.bcc.txt
slashdot0902-d.bcc.txt
t.FLA-w.bcc.txt
y-BerkStan-d.bcc.txt
z-alue7065.bcc.txt
"


# Run algorithms 1, 3 and 7 on all graphs
for file in $SMALL_GRAPHS $SNAP_GRAPHS $MASSIVE_GRAPHS; do
    echo "=========================="
    # Algorithm 1 (Borassi et al.)
    run_on_file "$file" "-a 1" "1.out"
    # Algorithm 3 + pruning + heuristic + cache size 1000
    run_on_file "$file"  "-a 3 -c 1000" "3.out"
    # Algorithm 3 + heuristic without pruning + cache size 1000
    run_on_file "$file" "-a 3 -noprune -c 1000" "3.noprune.out"
    # Algorithm 3 + pruning but without heuristic + cache size 1000
    run_on_file "$file" "-a 3 -noheur -k 0 -c 1000" "3.prune.noheur.out"
    # Algorithm 3 + no pruning no heuristic + cache size 1000
    run_on_file "$file" "-a 3 -noprune -noheur -k 0 -c 1000" "3.noprune.noheur.out"
    # Algorithm 3 + no pruning + no heuristic + cache size 2
    run_on_file "$file" "-a 3 -noprune -noheur -k 0 -c 2" "3.noprune.noheur.out"
    # Algorithm 7 + cache size 1000
    run_on_file "$file" "-a 7 -c 1000" "7.out"
done


# Experiments for the impact of cache size on performances
GRAPHS="dblp-2010.bcc.edgelist CAIDA_as_20130101.bcc.edgelist notreDame-d.bcc.txt loc-brightkite.bcc.edgelist"
CACHE_SIZES="2 10 50 100 500 1000 5000 10000"

for file in $GRAPHS; do
    for size in $CACHE_SIZES; do
	echo "run algorithm 3 on $file with cache size $size"
	run_on_file "$file" "-a 3 -c $size" "3.prune.c$size.out"
    done
done


